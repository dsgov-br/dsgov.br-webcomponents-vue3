#!/bin/bash

set -e

pnpm install
pnpm dlx nx release version $1
pnpm dlx nx build webcomponents --verbose
pnpm dlx nx build angular --verbose
pnpm dlx nx build react --verbose
pnpm dlx nx build vue --verbose

# Verificar se o parâmetro passado é "next", "alpha", "beta" ou "rc"
if [[ ! "$1" =~ (next|alpha|beta|rc) ]]; then
  pnpm dlx nx docs:version site $1 --w=site
else
  echo "Versão $1 é uma versão de pré-lançamento (next, alpha, beta, rc). Não será gerada uma versão da documentação no site."
fi
