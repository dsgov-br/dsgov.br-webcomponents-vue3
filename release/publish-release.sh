#!/bin/bash

set -e

if [ -z "$1" ]; then
  versioningChannel="latest"
else
  versioningChannel="$1"
fi

pnpm publish --recursive --tag $versioningChannel --no-git-checks --access public
