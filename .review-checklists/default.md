<!-- markdownlint-disable MD041 -->

## Checklists de Revisão

### Merge Request

- [ ] Descrição do MR contém detalhes suficientes para review.
- [ ] As issues que são resolvidas pelo MR estão relacionadas com o MR.

### Padrões de Commits

- [ ] [Commits](https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/git-gitlab/guias/commit/) seguem os padrões definidos pelo projeto e descrevem claramente as mudanças realizadas;
- [ ] Os escopos 'no-release, site ou monorepo' foram utilizados para que o commit seja ignorado pelo `semantic-release` quando necessário.
- [ ] [`BREAKING CHANGES`](https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/git-gitlab/guias/breaking-changes/) estão sendo devidamente marcadas no `commit` e/ou `merge request`.
