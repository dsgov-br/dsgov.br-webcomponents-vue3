<!-- markdownlint-disable MD041 -->

## CI

- [ ] `gitlab-ci.yml` foi testado no `pipeline editor` do gitlab.com para encontrar erros?
- [ ] Estágios estão devidamente documentados, quando necessário?
- [ ] Regras de execução (agendadas, branchs, tags, etc) estão devidamente definidas?
- [ ] Artefatos estão sendo incluídos?
- [ ] Artefatos e logs estão sendo incluídos no `Danger`, quando necessário?
- [ ] Documentação do monorepo (`readme.md`) foi atualizada de acordo?

### Otimização

- [ ] Os pipelines estão otimizados para reduzir o tempo de execução.
- [ ] Os artefatos são gerados corretamente e estão disponíveis para download.
