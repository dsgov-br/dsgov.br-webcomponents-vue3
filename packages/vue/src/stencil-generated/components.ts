/* eslint-disable */
/* tslint:disable */
/* auto-generated vue proxies */
import { defineContainer, defineStencilSSRComponent } from '@stencil/vue-output-target/runtime';

import type { JSX } from '@govbr-ds/webcomponents/dist/components';

import { defineCustomElement as defineBrAvatar } from '@govbr-ds/webcomponents/dist/components/br-avatar.js';
import { defineCustomElement as defineBrButton } from '@govbr-ds/webcomponents/dist/components/br-button.js';
import { defineCustomElement as defineBrCheckbox } from '@govbr-ds/webcomponents/dist/components/br-checkbox.js';
import { defineCustomElement as defineBrCheckgroup } from '@govbr-ds/webcomponents/dist/components/br-checkgroup.js';
import { defineCustomElement as defineBrDropdown } from '@govbr-ds/webcomponents/dist/components/br-dropdown.js';
import { defineCustomElement as defineBrIcon } from '@govbr-ds/webcomponents/dist/components/br-icon.js';
import { defineCustomElement as defineBrInput } from '@govbr-ds/webcomponents/dist/components/br-input.js';
import { defineCustomElement as defineBrItem } from '@govbr-ds/webcomponents/dist/components/br-item.js';
import { defineCustomElement as defineBrList } from '@govbr-ds/webcomponents/dist/components/br-list.js';
import { defineCustomElement as defineBrLoading } from '@govbr-ds/webcomponents/dist/components/br-loading.js';
import { defineCustomElement as defineBrMessage } from '@govbr-ds/webcomponents/dist/components/br-message.js';
import { defineCustomElement as defineBrRadio } from '@govbr-ds/webcomponents/dist/components/br-radio.js';
import { defineCustomElement as defineBrSelect } from '@govbr-ds/webcomponents/dist/components/br-select.js';
import { defineCustomElement as defineBrSwitch } from '@govbr-ds/webcomponents/dist/components/br-switch.js';
import { defineCustomElement as defineBrTag } from '@govbr-ds/webcomponents/dist/components/br-tag.js';
import { defineCustomElement as defineBrTextarea } from '@govbr-ds/webcomponents/dist/components/br-textarea.js';
import { defineCustomElement as defineBrUpload } from '@govbr-ds/webcomponents/dist/components/br-upload.js';


export const BrAvatar = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrAvatar>('br-avatar', defineBrAvatar, [
  'density',
  'customId',
  'src',
  'alt',
  'isIconic',
  'text',
  'iconWidth',
  'iconHeight',
  'iconMarginTop',
  'disabled'
]) : defineStencilSSRComponent({
  tagName: 'br-avatar',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'density': [String, "density"],
    'customId': [String, "custom-id"],
    'src': [String, "src"],
    'alt': [String, "alt"],
    'isIconic': [Boolean, "is-iconic"],
    'text': [String, "text"],
    'iconWidth': [String, "icon-width"],
    'iconHeight': [String, "icon-height"],
    'iconMarginTop': [String, "icon-margin-top"],
    'disabled': [Boolean, "disabled"]
  }
});


export const BrButton = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrButton>('br-button', defineBrButton, [
  'colorMode',
  'customId',
  'density',
  'disabled',
  'emphasis',
  'isActive',
  'isLoading',
  'shape',
  'type',
  'value'
]) : defineStencilSSRComponent({
  tagName: 'br-button',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'colorMode': [String, "color-mode"],
    'customId': [String, "custom-id"],
    'density': [String, "density"],
    'disabled': [Boolean, "disabled"],
    'emphasis': [String, "emphasis"],
    'isActive': [Boolean, "is-active"],
    'isLoading': [Boolean, "is-loading"],
    'shape': [String, "shape"],
    'type': [String, "type"],
    'value': [String, "value"]
  }
});


export const BrCheckbox = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrCheckbox, JSX.BrCheckbox["checked"]>('br-checkbox', defineBrCheckbox, [
  'indeterminate',
  'checked',
  'customId',
  'disabled',
  'hasHiddenLabel',
  'label',
  'name',
  'state',
  'value',
  'checkedChange',
  'indeterminateChange'
], [
  'checkedChange',
  'indeterminateChange'
],
'checked', 'checkedChange') : defineStencilSSRComponent({
  tagName: 'br-checkbox',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'indeterminate': [Boolean, "indeterminate"],
    'checked': [Boolean, "checked"],
    'customId': [String, "custom-id"],
    'disabled': [Boolean, "disabled"],
    'hasHiddenLabel': [Boolean, "has-hidden-label"],
    'label': [String, "label"],
    'name': [String, "name"],
    'state': [String, "state"],
    'value': [String, "value"],
    'onCheckedChange': [Function],
    'onIndeterminateChange': [Function]
  }
});


export const BrCheckgroup = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrCheckgroup>('br-checkgroup', defineBrCheckgroup, [
  'label',
  'labelSelecionado',
  'labelDesselecionado',
  'indeterminate',
  'customId'
]) : defineStencilSSRComponent({
  tagName: 'br-checkgroup',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'label': [String, "label"],
    'labelSelecionado': [String, "label-selecionado"],
    'labelDesselecionado': [String, "label-desselecionado"],
    'indeterminate': [Boolean, "indeterminate"],
    'customId': [String, "custom-id"]
  }
});


export const BrDropdown = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrDropdown>('br-dropdown', defineBrDropdown, [
  'isOpen',
  'customId',
  'brDropdownChange'
], [
  'brDropdownChange'
]) : defineStencilSSRComponent({
  tagName: 'br-dropdown',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'isOpen': [Boolean, "is-open"],
    'customId': [String, "custom-id"],
    'onBrDropdownChange': [Function]
  }
});


export const BrIcon = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrIcon>('br-icon', defineBrIcon, [
  'iconName',
  'height',
  'customId',
  'width',
  'cssClasses',
  'isInline',
  'rotate',
  'flip'
]) : defineStencilSSRComponent({
  tagName: 'br-icon',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'iconName': [String, "icon-name"],
    'height': [String, "height"],
    'customId': [String, "custom-id"],
    'width': [String, "width"],
    'cssClasses': [String, "css-classes"],
    'isInline': [Boolean, "is-inline"],
    'rotate': [String, "rotate"],
    'flip': [String, "flip"]
  }
});


export const BrInput = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrInput, JSX.BrInput["value"]>('br-input', defineBrInput, [
  'type',
  'autocomplete',
  'density',
  'disabled',
  'isInline',
  'isHighlight',
  'state',
  'label',
  'customId',
  'name',
  'placeholder',
  'readonly',
  'required',
  'value',
  'helpText',
  'autocorrect',
  'min',
  'max',
  'minlength',
  'maxlength',
  'multiple',
  'pattern',
  'step',
  'valueChange'
], [
  'valueChange'
],
'value', 'valueChange') : defineStencilSSRComponent({
  tagName: 'br-input',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'type': [String, "type"],
    'autocomplete': [String, "autocomplete"],
    'density': [String, "density"],
    'disabled': [Boolean, "disabled"],
    'isInline': [Boolean, "is-inline"],
    'isHighlight': [Boolean, "is-highlight"],
    'state': [String, "state"],
    'label': [String, "label"],
    'customId': [String, "custom-id"],
    'name': [String, "name"],
    'placeholder': [String, "placeholder"],
    'readonly': [Boolean, "readonly"],
    'required': [Boolean, "required"],
    'value': [String, "value"],
    'helpText': [String, "help-text"],
    'autocorrect': [String, "autocorrect"],
    'min': [Number, "min"],
    'max': [Number, "max"],
    'minlength': [Number, "minlength"],
    'maxlength': [Number, "maxlength"],
    'multiple': [Boolean, "multiple"],
    'pattern': [String, "pattern"],
    'step': [Number, "step"],
    'onValueChange': [Function]
  }
});


export const BrItem = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrItem>('br-item', defineBrItem, [
  'disabled',
  'customId',
  'isActive',
  'isSelected',
  'isInteractive',
  'href',
  'target',
  'isButton',
  'type',
  'value',
  'density',
  'brDidClick',
  'brDidSelect'
], [
  'brDidClick',
  'brDidSelect'
]) : defineStencilSSRComponent({
  tagName: 'br-item',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'disabled': [Boolean, "disabled"],
    'customId': [String, "custom-id"],
    'isActive': [Boolean, "is-active"],
    'isSelected': [Boolean, "is-selected"],
    'isInteractive': [Boolean, "is-interactive"],
    'href': [String, "href"],
    'target': [String, "target"],
    'isButton': [Boolean, "is-button"],
    'type': [String, "type"],
    'value': [String, "value"],
    'density': [String, "density"],
    'onBrDidClick': [Function],
    'onBrDidSelect': [Function]
  }
});


export const BrList = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrList>('br-list', defineBrList, [
  'header',
  'isHorizontal',
  'customId',
  'hideHeaderDivider'
]) : defineStencilSSRComponent({
  tagName: 'br-list',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'header': [String, "header"],
    'isHorizontal': [Boolean, "is-horizontal"],
    'customId': [String, "custom-id"],
    'hideHeaderDivider': [Boolean, "hide-header-divider"]
  }
});


export const BrLoading = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrLoading>('br-loading', defineBrLoading, [
  'isProgress',
  'customId',
  'progressPercent',
  'isMedium',
  'label'
]) : defineStencilSSRComponent({
  tagName: 'br-loading',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'isProgress': [Boolean, "is-progress"],
    'customId': [String, "custom-id"],
    'progressPercent': [Number, "progress-percent"],
    'isMedium': [Boolean, "is-medium"],
    'label': [String, "label"]
  }
});


export const BrMessage = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrMessage>('br-message', defineBrMessage, [
  'messageTitle',
  'customId',
  'message',
  'isInline',
  'isClosable',
  'autoRemove',
  'showIcon',
  'isFeedback',
  'state',
  'brDidClose'
], [
  'brDidClose'
]) : defineStencilSSRComponent({
  tagName: 'br-message',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'messageTitle': [String, "message-title"],
    'customId': [String, "custom-id"],
    'message': [String, "message"],
    'isInline': [Boolean, "is-inline"],
    'isClosable': [Boolean, "is-closable"],
    'autoRemove': [Boolean, "auto-remove"],
    'showIcon': [Boolean, "show-icon"],
    'isFeedback': [Boolean, "is-feedback"],
    'state': [String, "state"],
    'onBrDidClose': [Function]
  }
});


export const BrRadio = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrRadio, JSX.BrRadio["checked"]>('br-radio', defineBrRadio, [
  'checked',
  'disabled',
  'state',
  'hasHiddenLabel',
  'customId',
  'name',
  'label',
  'value',
  'checkedChange'
], [
  'checkedChange'
],
'checked', 'checkedChange') : defineStencilSSRComponent({
  tagName: 'br-radio',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'checked': [Boolean, "checked"],
    'disabled': [Boolean, "disabled"],
    'state': [String, "state"],
    'hasHiddenLabel': [Boolean, "has-hidden-label"],
    'customId': [String, "custom-id"],
    'name': [String, "name"],
    'label': [String, "label"],
    'value': [String, "value"],
    'onCheckedChange': [Function]
  }
});


export const BrSelect = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrSelect>('br-select', defineBrSelect, [
  'label',
  'placeholder',
  'isMultiple',
  'options',
  'selectAllLabel',
  'unselectAllLabel',
  'showSearchIcon',
  'isOpen',
  'customId',
  'brDidSelectChange'
], [
  'brDidSelectChange'
]) : defineStencilSSRComponent({
  tagName: 'br-select',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'label': [String, "label"],
    'placeholder': [String, "placeholder"],
    'isMultiple': [Boolean, "is-multiple"],
    'options': [String, "options"],
    'selectAllLabel': [String, "select-all-label"],
    'unselectAllLabel': [String, "unselect-all-label"],
    'showSearchIcon': [Boolean, "show-search-icon"],
    'isOpen': [Boolean, "is-open"],
    'customId': [String, "custom-id"],
    'onBrDidSelectChange': [Function]
  }
});


export const BrSwitch = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrSwitch, JSX.BrSwitch["checked"]>('br-switch', defineBrSwitch, [
  'disabled',
  'checked',
  'customId',
  'label',
  'labelPosition',
  'labelOn',
  'labelOff',
  'hasIcon',
  'name',
  'value',
  'density',
  'checkedChange'
], [
  'checkedChange'
],
'checked', 'checkedChange') : defineStencilSSRComponent({
  tagName: 'br-switch',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'disabled': [Boolean, "disabled"],
    'checked': [Boolean, "checked"],
    'customId': [String, "custom-id"],
    'label': [String, "label"],
    'labelPosition': [String, "label-position"],
    'labelOn': [String, "label-on"],
    'labelOff': [String, "label-off"],
    'hasIcon': [Boolean, "has-icon"],
    'name': [String, "name"],
    'value': [String, "value"],
    'density': [String, "density"],
    'onCheckedChange': [Function]
  }
});


export const BrTag = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrTag>('br-tag', defineBrTag, [
  'label',
  'iconName',
  'name',
  'multiple',
  'selected',
  'customId',
  'color',
  'density',
  'shape',
  'status',
  'interaction',
  'interactionSelect',
  'disabled',
  'radioSelected'
], [
  'radioSelected'
]) : defineStencilSSRComponent({
  tagName: 'br-tag',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'label': [String, "label"],
    'iconName': [String, "icon-name"],
    'name': [String, "name"],
    'multiple': [Boolean, "multiple"],
    'selected': [Boolean, "selected"],
    'customId': [String, "custom-id"],
    'color': [String, "color"],
    'density': [String, "density"],
    'shape': [String, "shape"],
    'status': [Boolean, "status"],
    'interaction': [Boolean, "interaction"],
    'interactionSelect': [Boolean, "interaction-select"],
    'disabled': [Boolean, "disabled"],
    'onRadioSelected': [Function]
  }
});


export const BrTextarea = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrTextarea, JSX.BrTextarea["value"]>('br-textarea', defineBrTextarea, [
  'value',
  'label',
  'placeholder',
  'customId',
  'disabled',
  'maxlength',
  'showCounter',
  'state',
  'density',
  'isInline',
  'valueChange'
], [
  'valueChange'
],
'value', 'valueChange') : defineStencilSSRComponent({
  tagName: 'br-textarea',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'value': [String, "value"],
    'label': [String, "label"],
    'placeholder': [String, "placeholder"],
    'customId': [String, "custom-id"],
    'disabled': [Boolean, "disabled"],
    'maxlength': [Number, "maxlength"],
    'showCounter': [Boolean, "show-counter"],
    'state': [String, "state"],
    'density': [String, "density"],
    'isInline': [Boolean, "is-inline"],
    'onValueChange': [Function]
  }
});


export const BrUpload = /*@__PURE__*/ globalThis.window ? defineContainer<JSX.BrUpload>('br-upload', defineBrUpload, [
  'accept',
  'label',
  'disabled',
  'state',
  'multiple'
]) : defineStencilSSRComponent({
  tagName: 'br-upload',
  hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate'),
  props: {
    'accept': [String, "accept"],
    'label': [String, "label"],
    'disabled': [Boolean, "disabled"],
    'state': [String, "state"],
    'multiple': [Boolean, "multiple"]
  }
});

