# Wrapper React para @govbr-ds/webcomponents

Um wrapper React que encapsula os [Web Components GovBR-DS](https://gov.br/ds/webcomponents), permitindo seu uso como componentes nativos do React.

## Por que usar este wrapper? 🤔

Usar o wrapper React para os Web Components GovBR-DS oferece várias vantagens:

- **Automatização**: Automatiza a geração de wrappers de componentes React para componentes Stencil, economizando tempo e esforço no desenvolvimento.

- **Bindings JSX**: Gera wrappers de componentes funcionais React com bindings JSX para eventos e propriedades personalizadas, facilitando a integração e o uso dos componentes.

- **Tipagens e Auto-completação**: Fornece tipagens e auto-completação para componentes React no seu IDE, melhorando a experiência de desenvolvimento e reduzindo erros.

Para mais detalhes, consulte a [documentação oficial do Stencil](https://stenciljs.com/docs/react).

React e custom elements enfrentam algumas limitações quando usados juntos. O [Custom Elements Everywhere](https://custom-elements-everywhere.com/#react) descreve bem essas dificuldades:

### Manipulação de dados

React transfere dados para custom elements como atributos HTML. Isso funciona para valores primitivos, mas apresenta problemas para dados complexos, como objetos ou arrays. Esses valores acabam sendo convertidos para strings, como `some-attr="[object Object]"`, tornando-os inutilizáveis.

### Manipulação de eventos

Como o React utiliza seu próprio sistema de eventos sintéticos, ele não pode capturar eventos DOM disparados por custom elements sem recorrer a soluções alternativas. Isso exige que os desenvolvedores utilizem referências (`refs`) para anexar ouvintes de eventos manualmente com `addEventListener`, complicando o uso de custom elements.

Este wrapper soluciona essas questões, expondo um componente React que mapeia propriedades e eventos diretamente para o elemento personalizado correspondente.

## Instalação ⚙️

Instale os pacotes necessários:

```bash
npm install --save-dev @govbr-ds/{core,webcomponents,webcomponents-react}
```

## Configuração 🛠️

### Incluindo os tokens do `@govbr-ds/core`

Os estilos de `govbr-ds/core` podem ser importados no arquivo de estilo principal do seu aplicativo:

```css
@import '~@govbr-ds/core/dist/core-tokens.min.css';
```

## Uso 📚

Exemplo de utilização:

```jsx
import React from 'react';
import { BrButton } from '@govbr-ds/webcomponents-react';

function App() {
  const handleButtonClick = (ev: CustomEvent) => {
    console.log(ev.detail);
  };

  return (
    <BrButton emphasis="primary" onBqClick={handleButtonClick}>
      Clique aqui
    </BrButton>
  );
}

export default App;
```

## Estrutura de pastas e arquivos 📁

```plaintext
- src/
  - stencil-generated/
  - index.ts
- ssr/
  - stencil-generated/
  - index.ts
```

- A pasta `src` contém os arquivos da biblioteca React padrão.

- A pasta `ssr` contém os arquivos para aplicações com renderização no servidor (SSR).

**Importante:** Ao gerar um novo build da biblioteca de Web Components, as pastas `src/stencil-generated` e `ssr/stencil-generated` são recriadas, sobrescrevendo quaisquer alterações feitas nesses arquivos.

Os arquivos `index.ts` atuam como pontos de entrada da biblioteca.

## Build 📦

Antes de compilar a biblioteca React, é necessário gerar o build dos webcomponents:

```bash
nx build webcomponents
```

Depois, para gerar o build da biblioteca React, execute:

```bash
nx build react
```

## Documentações Complementares 📚

Consulte a seção sobre Web Componente na nossa [Wiki](https://gov.br/ds/wiki/desenvolvimento/web-components) para obter mais informações sobre esse projeto.

Para saber mais detalhes sobre a especificação Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Contribuindo 🤝

Antes de abrir um Merge Request, por favor, leve em consideração as seguintes informações:

- Este é um projeto open-source e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, use um título claro e explicativo para o MR, e siga os padrões estabelecidos em nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir? Consulte o nosso guia [como contribuir](https://gov.br/ds/wiki/comunidade/contribuindo-com-o-ds/ 'Como contribuir?').

## Reportar Bugs/Problemas ou Sugestões 🐛

Para reportar problemas ou sugerir melhorias, abra uma [issue](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/issues/new). Utilize o modelo adequado e forneça o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Commits 📝

Este projeto segue um padrão para branches e commits. Consulte a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender mais sobre esses padrões.

## Precisa de ajuda? 🆘

Por favor, **não** crie issues para perguntas gerais.

Use os canais abaixo para tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)
- Web Components [https://gov.br/ds/webcomponents](https://gov.br/ds/webcomponents)
- Canal no Discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Créditos 🎉

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') foram desenvolvidos pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') em colaboração com a comunidade.
