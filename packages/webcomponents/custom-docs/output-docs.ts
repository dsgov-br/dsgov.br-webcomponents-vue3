import fs, { promises as fsPromises } from 'fs'
import * as path from 'path'

import { JsonDocs, JsonDocsComponent } from '@stencil/core/internal'

import { stylesToMarkdown } from './markdown-css-props'
import { depsToMarkdown } from './markdown-dependencies'
import { eventsToMarkdown } from './markdown-events'
import { allEventsToMarkdown } from './markdown-events-all'
import { examplesToMarkdown } from './markdown-examples'
import { methodsToMarkdown } from './markdown-methods'
import { allMethodsToMarkdown } from './markdown-methods-all'
import { overviewToMarkdown } from './markdown-overview'
import { partsToMarkdown } from './markdown-parts'
import { propsToMarkdown } from './markdown-props'
import { allPropsToMarkdown } from './markdown-props-all'
import { sectionsToMarkdown } from './markdown-sections'
import { slotsToMarkdown } from './markdown-slots'
import { allSlotsToMarkdown } from './markdown-slots-all'
import { sortTablesInMarkdown } from './sort-markdown-tables'

// Função principal para gerar o arquivo Markdown a partir dos dados do componente
export const generateMarkdown = async (cmps: JsonDocs) => {
  const basePath = path.resolve(__dirname, '../../../')

  // Deleta a pasta com as documentações geradas automaticamente dentro do docusaurus
  if (fs.existsSync(path.resolve(basePath, `apps/site/docs/stencil-generated-docs`))) {
    try {
      fs.rmSync(path.resolve(basePath, `apps/site/docs/stencil-generated-docs`), { recursive: true })
    } catch (err) {
      console.error(`Erro ao deletar apps/site/docs/stencil-generated-docs`, err)
    }
  }

  for (const cmp of cmps.components) {
    // Obtém o nome do componente a partir do caminho do diretório
    const componentName = getComponentName(cmp.dirPath ?? '')

    // Monta as seções dos arquivos markdown
    const dependencies = depsToMarkdown(cmp, cmps.components).join(`\n`)
    const deprecation = getDocsDeprecation(cmp).join(`\n`)
    const events = eventsToMarkdown(cmp.events).join(`\n`)
    const examples = examplesToMarkdown(cmp.tag, basePath)
    const methods = methodsToMarkdown(cmp.methods).join(`\n`)
    const overview = overviewToMarkdown(cmp.overview).join(`\n`)
    const parts = partsToMarkdown(cmp.parts).join(`\n`)
    const props = propsToMarkdown(cmp.props).join(`\n`)
    const sections = sectionsToMarkdown(cmp.tag, basePath)
    const slots = slotsToMarkdown(cmp.slots).join(`\n`)
    const styles = stylesToMarkdown(cmp.styles).join(`\n`)

    const htmlExamples = Object.keys(examples)
      .filter((nomeArquivo) => nomeArquivo.endsWith('.html'))
      .reduce((acc, nomeArquivo) => {
        acc[nomeArquivo] = `\`\`\`html\n${examples[nomeArquivo]}\`\`\``
        return acc
      }, {})

    // Monta o conteúdo do `readme.md` a partir de várias seções
    const mainReadmeFile = [
      deprecation,
      overview,
      props,
      events,
      methods,
      slots,
      parts,
      styles,
      `## Exemplos`,
      ...Object.values(htmlExamples),
      dependencies,
      `---`,
      ...Object.values(sections),
      '',
    ].join('\n')

    // Escreve o conteúdo Markdown em um arquivo `readme.md` dentro do diretório do componente
    fsPromises
      .writeFile(
        path.resolve(basePath, `packages/webcomponents/src/components/${componentName}/readme.md`),
        sortTablesInMarkdown(mainReadmeFile.trim()),
        'utf8'
      )
      .catch((err) => console.error(`Erro ao criar o ${componentName}/readme.md`, err))

    const siteGenDocsBasePath = path.resolve(basePath, 'apps/site/docs/stencil-generated-docs')
    const siteGenComponentDocsBasePath = path.resolve(basePath, 'apps/site/docs/stencil-generated-docs', componentName)

    // Cria a pasta base e todas as subpastas necessárias no docusaurus
    try {
      fs.mkdirSync(siteGenComponentDocsBasePath, { recursive: true })
    } catch (err) {
      console.error(`Erro ao criar ${siteGenComponentDocsBasePath}`, err)
    }

    // Criar arquivos apenas se tiver conteúdo
    const createFileIfNotEmpty = (filePath: string, content: string) => {
      const trimmedContent = content.trim()
      if (trimmedContent.length > 0) return fsPromises.writeFile(filePath, trimmedContent, 'utf8')
      return Promise.resolve() // Retorna uma promessa resolvida se o conteúdo for vazio
    }

    // Array de promessas de escrita de arquivos
    const writeOperations = [
      createFileIfNotEmpty(
        path.join(siteGenComponentDocsBasePath, 'deprecation.md'),
        sortTablesInMarkdown(deprecation)
      ),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'overview.md'), sortTablesInMarkdown(overview)),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'props.md'), sortTablesInMarkdown(props)),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'events.md'), sortTablesInMarkdown(events)),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'methods.md'), sortTablesInMarkdown(methods)),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'slots.md'), sortTablesInMarkdown(slots)),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'parts.md'), sortTablesInMarkdown(parts)),
      createFileIfNotEmpty(path.join(siteGenComponentDocsBasePath, 'styles.md'), sortTablesInMarkdown(styles)),
      createFileIfNotEmpty(
        path.join(siteGenComponentDocsBasePath, 'dependencies.md'),
        sortTablesInMarkdown(dependencies)
      ),
    ]

    // Array de promessas de escrita de arquivos com base no objeto `sections`
    const writeSections = Object.entries(sections).map(([key, value]) => {
      const filePath = path.join(siteGenComponentDocsBasePath, `sections/${key}.md`)

      try {
        fs.mkdirSync(`${siteGenComponentDocsBasePath}/sections`, { recursive: true })
      } catch (err) {
        console.error(`Erro ao criar ${siteGenComponentDocsBasePath}/sections`, err)
      }

      return createFileIfNotEmpty(filePath, sortTablesInMarkdown(value))
    })

    // Array de promessas de escrita de arquivos com base no objeto `examples`
    const writeExamples = Object.entries(examples).map(([key, value]) => {
      const filePath = path.join(siteGenComponentDocsBasePath, `examples/${key}`)

      try {
        fs.mkdirSync(`${siteGenComponentDocsBasePath}/examples`, { recursive: true })
      } catch (err) {
        console.error(`Erro ao criar ${siteGenComponentDocsBasePath}/examples`, err)
      }

      return createFileIfNotEmpty(filePath, sortTablesInMarkdown(value))
    })

    // Array com todas as promises de escrita
    const allPromisses = [...writeOperations, ...writeSections, ...writeExamples]

    // Executa as operações de escrita e trata o resultado
    Promise.all(allPromisses).catch((err) => {
      console.error('Erro ao criar arquivos.', err)
    })

    fsPromises.writeFile(
      path.join(siteGenDocsBasePath, 'all-component-props.md'),
      sortTablesInMarkdown(allPropsToMarkdown(cmps).join(`\n`)),
      'utf8'
    )

    fsPromises.writeFile(
      path.join(siteGenDocsBasePath, 'all-component-methods.md'),
      sortTablesInMarkdown(allMethodsToMarkdown(cmps).join(`\n`)),
      'utf8'
    )

    fsPromises.writeFile(
      path.join(siteGenDocsBasePath, 'all-component-events.md'),
      sortTablesInMarkdown(allEventsToMarkdown(cmps).join(`\n`)),
      'utf8'
    )

    fsPromises.writeFile(
      path.join(siteGenDocsBasePath, 'all-component-slots.md'),
      sortTablesInMarkdown(allSlotsToMarkdown(cmps).join(`\n`)),
      'utf8'
    )
  }
}

// Função para obter a seção de descontinuação, se existir
const getDocsDeprecation = (cmp: JsonDocsComponent) => {
  if (cmp.deprecation !== undefined) {
    // Verifica se há uma mensagem de descontinuação
    return [`> **[Descontinuado/Obsoleto]** ${cmp.deprecation}`, ''] // Retorna a mensagem formatada
  }
  return [] // Retorna um array vazio se não houver descontinuação
}

// Função para extrair o nome do componente a partir do caminho do diretório
function getComponentName(dirPath: string): string {
  const parts = dirPath.split('/') // Divide o caminho do diretório em partes
  return parts[parts.length - 1] // Retorna a última parte, que é o nome do componente
}
