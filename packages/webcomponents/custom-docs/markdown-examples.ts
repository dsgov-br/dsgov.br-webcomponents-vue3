import * as fs from 'fs'
import * as path from 'path'

import { JsonDocsUsage } from '@stencil/core/internal'

// Função para ler os arquivos markdown de uma pasta
export const examplesToMarkdown = (componentTag: string, basePath: string): JsonDocsUsage => {
  const usageData: JsonDocsUsage = {}

  const directoryPath = path.resolve(
    basePath,
    `packages/webcomponents/src/pages/components/${componentTag.replace('br-', '')}`
  )

  // Verifica se a pasta existe antes de tentar ler os arquivos
  if (!fs.existsSync(directoryPath)) return usageData // Retorna um objeto vazio se a pasta não existir

  // Lê o conteúdo da pasta especificada
  const files = fs.readdirSync(directoryPath)

  files.forEach((file) => {
    const filePath = path.join(directoryPath, file)
    const fileContent = fs.readFileSync(filePath, 'utf8')
    usageData[file] = fileContent
  })

  return usageData
}
