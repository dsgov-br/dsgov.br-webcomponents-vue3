import { JsonDocs, JsonDocsMethod } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

export const allMethodsToMarkdown = (cmps: JsonDocs) => {
  const content: string[] = []

  const table = new MarkdownTable() // Cria uma nova tabela Markdown

  // Adiciona o cabeçalho da tabela
  table.addHeader(['Assinatura', 'Componente', 'Descrição'])

  // Itera sobre os metadados das propriedades
  for (const cmp of cmps.components) {
    for (const method of cmp.methods) {
      table.addRow([`\`${method.signature}\``, cmp.tag, getDocsField(method)])
    }
  }

  // Adiciona a tabela convertida ao conteúdo
  content.push(...table.toMarkdown())
  content.push(``)
  return content // Retorna o conteúdo gerado
}

const getDocsField = (prop: JsonDocsMethod) => {
  // Se o método está deprecated, adiciona uma mensagem de depreciação
  return `${
    prop.deprecation !== undefined
      ? `<span style="color:red">**[Descontinuado/Obsoleto]**</span> ${prop.deprecation}<br/><br/>`
      : ''
  }${prop.docs}` // Retorna a documentação do método
}
