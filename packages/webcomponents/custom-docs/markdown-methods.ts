import { JsonDocsMethod } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

// Função para gerar a documentação em Markdown dos métodos de um componente
export const methodsToMarkdown = (methods: JsonDocsMethod[]) => {
  const content: string[] = []

  // Se não houver métodos, retorna um array vazio
  if (methods.length === 0) return content

  // Adiciona o título da seção de métodos
  content.push(`## Métodos`)
  content.push(``)

  // Itera sobre cada método para gerar a documentação
  methods.forEach((method) => {
    content.push(`### \`${method.signature}\``) // Adiciona a assinatura do método como um título
    content.push(``)
    content.push(getDocsField(method)) // Adiciona a documentação do método
    content.push(``)

    // Se o método possui parâmetros, cria uma tabela para eles
    if (method.parameters.length > 0) {
      const parmsTable = new MarkdownTable() // Cria uma nova tabela Markdown

      // Adiciona o cabeçalho da tabela
      parmsTable.addHeader(['Nome', 'Tipo', 'Descrição'])

      // Adiciona cada parâmetro à tabela
      method.parameters.forEach(({ name, type, docs }) => {
        parmsTable.addRow(['`' + name + '`', '`' + type + '`', docs])
      })

      // Adiciona a seção de parâmetros à documentação
      content.push(`#### Parâmetros`)
      content.push(``)
      content.push(...parmsTable.toMarkdown()) // Adiciona o conteúdo da tabela
      content.push(``)
    }

    // Se o método retorna um valor, adiciona essa informação à documentação
    if (method.returns !== undefined) {
      content.push(`#### Retorna`)
      content.push(``)
      content.push(`Tipo: \`${method.returns.type}\``) // Tipo de retorno
      content.push(``)
      content.push(method.returns.docs) // Documentação do retorno
      content.push(``)
    }
  })

  content.push(``) // Linha em branco para formatação

  return content // Retorna o conteúdo gerado em formato de Markdown
}

// Função auxiliar para obter a documentação do campo do método
const getDocsField = (prop: JsonDocsMethod) => {
  // Se o método está deprecated, adiciona uma mensagem de depreciação
  return `${
    prop.deprecation !== undefined
      ? `<span style="color:red">**[Descontinuado/Obsoleto]**</span> ${prop.deprecation}<br/><br/>`
      : ''
  }${prop.docs}` // Retorna a documentação do método
}
