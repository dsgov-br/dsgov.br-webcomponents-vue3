import { JsonDocs, JsonDocsEvent } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

export const allEventsToMarkdown = (cmps: JsonDocs) => {
  const content: string[] = []

  const table = new MarkdownTable() // Cria uma nova tabela Markdown

  // Adiciona o cabeçalho da tabela
  table.addHeader(['Evento', 'Componente', 'Tipo', 'Descrição'])

  // Itera sobre os metadados das propriedades
  for (const cmp of cmps.components) {
    for (const event of cmp.events) {
      table.addRow([`\`${event.event}\``, cmp.tag, `\`CustomEvent<${event.detail}>\``, getDocsField(event)])
    }
  }

  // Adiciona a tabela convertida ao conteúdo
  content.push(...table.toMarkdown())
  content.push(``)
  return content // Retorna o conteúdo gerado
}

// Função auxiliar para obter a documentação do campo de evento
const getDocsField = (prop: JsonDocsEvent) => {
  // Se o evento está `deprecated`, adiciona uma mensagem de depreciação
  return `${
    prop.deprecation !== undefined
      ? `<span style="color:red">**[Descontinuado/Obsoleto]**</span> ${prop.deprecation}<br/><br/>`
      : ''
  }${prop.docs}`
}
