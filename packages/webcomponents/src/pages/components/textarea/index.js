if (!window.elementsToListen && !window.eventsToListen) {
  const elementsToListen = document.querySelectorAll('br-textarea')

  const eventsToListen = ['click']

  elementsToListen.forEach((domElement) => {
    eventsToListen.forEach((event) => {
      domElement.addEventListener(event, function (event) {
        console.log({
          Elemento: domElement,
          Evento: event,
          Tipo: event.type,
          'Detail(Custom Events)': event.detail,
          'Data(Native Events)': event.data,
        })
      })
    })
  })
}
