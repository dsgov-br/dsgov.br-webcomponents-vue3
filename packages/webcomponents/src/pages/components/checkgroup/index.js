if (!window.elementsToListen && !window.eventsToListen) {
  const elementsToListen = document.querySelectorAll('br-checkgroup')

  const eventsToListen = ['click', 'change', 'brDidAllCheckboxesSelected']

  elementsToListen.forEach((domElement) => {
    eventsToListen.forEach((event) => {
      console.log('Event:', event)
      domElement.addEventListener(event, function (event) {
        console.log({
          Elemento: domElement,
          Evento: event,
          Tipo: event.type,
          'Data(Native Events)': event.data,
          'Detail(Custom Events)': event.detail,
        })
      })
    })
  })
}
