if (!window.elementsToListen && !window.eventsToListen) {
  const elementsToListen = document.querySelectorAll('br-input')
  const eventsToListen = ['click', 'valueChange']
  window.elementsToListen = true

  elementsToListen.forEach((domElement) => {
    eventsToListen.forEach((event) => {
      domElement.addEventListener(event, function (event) {
        console.log({
          Evento: event.type,
          Elemento: domElement.outerHTML,
          ...(event.detail && { Detail: event.detail }),
          ...(event.data && { Data: event.data }),
        })
      })
    })
  })

  function validateForm() {
    const input = document.querySelector('#pattern')
    if (!input.validity.valid) {
      alert('Por favor, insira um código de país válido com três letras.')
      return false // Impede o envio do formulário
    }
    return true // Permite o envio do formulário
  }
}
