## Propriedades

| Propriedade         | Atributo             | Descrição                                                                                                                                                                                                          | Tipo                                 | Valor padrão            |
| ------------------- | -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------ | ----------------------- |
| `color`             | `color`              | A propriedade 'color' permite definir a cor do componente. Aceita valores como 'red', 'blue', entre outros, e é refletida no DOM.                                                                                  | `string`                             | `''`                    |
| `customId`          | `custom-id`          | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                             | `string`                             | ```br-tag-${tagId++}``` |
| `density`           | `density`            | A propriedade 'density' permite definir a densidade do componente. Aceita valores como 'small', 'medium' e 'large', e é refletida no DOM.                                                                          | `"large" \| "medium" \| "small"`     | `DENSITY_SMALL`         |
| `disabled`          | `disabled`           | Tag deve estar desabilitado.                                                                                                                                                                                       | `boolean`                            | `false`                 |
| `iconName`          | `icon-name`          | A propriedade 'iconName' define o nome do ícone que será exibido ao lado do texto. O valor padrão é 'fa-solid:car', que corresponde a um ícone de carro da biblioteca Font Awesome.                                | `string`                             | `''`                    |
| `interaction`       | `interaction`        | Interação deve ser habilitada.                                                                                                                                                                                     | `boolean`                            | `false`                 |
| `interactionSelect` | `interaction-select` | Interação de seleção deve ser habilitada.                                                                                                                                                                          | `boolean`                            | `false`                 |
| `label`             | `label`              | A propriedade 'label' é uma string que representa o texto a ser exibido no componente. Ela é refletida no DOM, permitindo que alterações no valor sejam refletidas no elemento HTML correspondente.                | `string`                             | `''`                    |
| `multiple`          | `multiple`           | A tag permite seleção múltipla.                                                                                                                                                                                    | `boolean`                            | `false`                 |
| `name`              | `name`               | A propriedade 'name' é uma string que representa o nome do grupo da tag para seleção. Ela é refletida no DOM, permitindo que alterações no valor sejam refletidas no elemento HTML correspondente.                 | `string`                             | `''`                    |
| `selected`          | `selected`           | A tag está selecionada.                                                                                                                                                                                            | `boolean`                            | `false`                 |
| `shape`             | `shape`              | A propriedade 'shape' define o formato do componente. Valores possíveis: - 'circle': Componente com formato circular. - 'rounded': Componente com bordas arredondadas. - 'default': Componente com formato padrão. | `"circle" \| "default" \| "rounded"` | `SHAPE_DEFAULT`         |
| `status`            | `status`             | Status deve ser exibido.                                                                                                                                                                                           | `boolean`                            | `false`                 |

## Eventos

| Evento          | Descrição                                  | Tipo                  |
| --------------- | ------------------------------------------ | --------------------- |
| `radioSelected` | Evento emitido quando a tag é selecionada. | `CustomEvent<string>` |

## Exemplos

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-tag label="1" color="bg-danger" shape="rounded"></br-tag>
  <br-tag label="10" color="bg-danger" shape="rounded"></br-tag>
  <br-tag label="130" color="bg-danger" shape="rounded"></br-tag>
  <br-tag label="999+" color="bg-danger" shape="rounded"></br-tag>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-tag label="Alta" color="bg-orange-vivid-50" density="small"></br-tag>
  <br-tag label="Média" color="bg-green-warm-vivid-50" density="medium"></br-tag>
  <br-tag label="Baixa" color="bg-indigo-warm-vivid-50" density="large"></br-tag>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-tag color="bg-orange-vivid-50" icon-name="fa-solid:car" shape="circle"></br-tag>
  <br-tag color="bg-green-warm-vivid-50" icon-name="fa-solid:search" shape="circle"></br-tag>
  <br-tag color="bg-indigo-warm-vivid-50" icon-name="fa-solid:user" shape="circle"></br-tag>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-tag label="Carro 1" icon-name="fa-solid:car" interaction></br-tag>
  <br-tag label="Carro 2" icon-name="fa-solid:car" interaction></br-tag>
  <br-tag label="Carro 3" icon-name="fa-solid:car" interaction></br-tag>
</div>
```

```html
<div class="row">
  <div class="col-md">
    <p class="h4">Seleção única</p>
    <div class="d-flex align-items-center">
      <br-tag label="Carro" name="vehicle" icon-name="fa-solid:car" interaction-select></br-tag>
      <br-tag label="Barco" name="vehicle" icon-name="fa-solid:ship" interaction-select selected></br-tag>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md">
    <p class="h4">Seleção múltipla</p>
    <div class="d-flex align-items-center">
      <br-tag multiple label="Bicicleta" name="multipla" icon-name="fa-solid:bicycle" interaction-select></br-tag>
      <br-tag multiple label="Barco" name="multipla" icon-name="fa-solid:ship" interaction-select selected></br-tag>
      <br-tag
        multiple
        label="Carro"
        name="multipla"
        icon-name="fa-solid:car"
        interaction-select
        selected
        disabled
      ></br-tag>
    </div>
  </div>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-tag color="bg-danger" shape="rounded" density="large"></br-tag>
  <br-tag color="bg-warning" shape="rounded" density="medium"></br-tag>
  <br-tag color="bg-success " shape="rounded" density="small"></br-tag>
  <br-tag label="Offline" color="bg-danger" shape="rounded" density="large" status></br-tag>
  <br-tag label="Ausente" color="bg-warning" shape="rounded" density="medium" status></br-tag>
  <br-tag label="Online" color="bg-success " shape="rounded" density="small" status></br-tag>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-tag label="Texto e Ícone" color="bg-orange-vivid-50" icon-name="fa-solid:car" density="small"></br-tag>
  <br-tag label="Texto" color="bg-green-warm-vivid-50" density="large"></br-tag>
  <br-tag label="Jhon Doe" color="bg-indigo-warm-vivid-50" density="large"></br-tag>
</div>
```

## Dependências

### Depende de

- [br-icon](../icon)
- [br-button](../button)

### Gráfico

```mermaid
graph TD;
  br-tag --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-tag --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-tag mainComponent
```

---

## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

Não se aplica a este componente, pois ele não estava presente na versão anterior.
