import { AttachInternals, Component, Element, h, Host, Listen, Prop, State } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/utilitarios/js/checkgroup?tab=desenvolvedor).
 *
 * @slot default - Slot para adicionar os checkboxes que serão controlados pelo checkgroup.
 */
@Component({
  tag: 'br-checkgroup',
  styleUrl: 'checkgroup.scss',
  shadow: true,
  formAssociated: true,
})
export class Checkgroup {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrCheckgroupElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Texto descritivo do grupo.
   */
  @Prop() label: string

  /**
   * Define o texto do label quando o checkbox está marcado.
   */
  @Prop({ reflect: true }) labelSelecionado: string = 'Desselecionar tudo'

  /**
   * Define o texto do label quando o checkbox está desmarcado.
   */
  @Prop({ reflect: true }) labelDesselecionado: string = 'Selecionar tudo'

  /**
   * Define o estado intermediário do checkbox.
   * Se definido como verdadeiro, o checkbox exibirá um estado intermediário, que é um estado visual que indica que a opção está parcialmente selecionada.
   * Este estado é útil quando o checkbox faz parte de um grupo com seleção parcial.
   */
  @Prop({ reflect: true }) indeterminate: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-checkgroup-${checkgroupId++}`

  /**
   * Define o estado de seleção do checkbox.
   * Se definido como verdadeiro, o checkbox estará marcado. Caso contrário, estará desmarcado.
   */
  @State() checked: boolean = false

  /**
   * Indica se o grupo de checkboxes está indeterminado
   */
  @State() isIndeterminate: boolean = false

  componentWillLoad() {
    this.isIndeterminate = this.el.hasAttribute('indeterminate')

    this.updateGroupState()
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-checkgroup': true,
    }
  }

  @Listen('checkedChange', {
    target: 'document',
  })
  handleChange(event: Event) {
    const target = event.target as HTMLBrCheckboxElement

    if (this.isParentCheckbox(target)) {
      this.checked = !this.checked
      this.handleAllCheckboxChange()
    } else {
      this.updateGroupState()
    }
  }

  private isParentCheckbox(target: HTMLBrCheckboxElement): boolean {
    return target === this.el.shadowRoot?.querySelector('br-checkbox')
  }

  private readonly handleAllCheckboxChange = () => {
    const checkListAll = Array.from(this.el.querySelectorAll('br-checkbox'))

    checkListAll.forEach((item) => {
      item.checked = this.checked
      item.indeterminate = false
    })

    const parentCheckbox = this.el.shadowRoot?.querySelector('br-checkbox')
    if (parentCheckbox !== null) {
      parentCheckbox.indeterminate = false
    }
  }

  private updateGroupState() {
    const checkListAll = Array.from(this.el.querySelectorAll('br-checkbox'))
    const allChecked = checkListAll.every((item) => item.checked)
    const noneChecked = checkListAll.every((item) => !item.checked)

    if (allChecked) {
      this.checked = true
      this.isIndeterminate = false
    } else if (noneChecked) {
      this.checked = false
      this.isIndeterminate = false
    } else {
      this.checked = false
      this.isIndeterminate = true
    }
  }

  render() {
    return (
      <Host>
        <div class={this.getCssClassMap()} style={{ width: '100%' }}>
          {this.label && <label>{this.label}</label>}
          <div class="mb-1">
            <div class="checkgroup-example">
              <br-checkbox
                name={`checkgroup-${this.customId}`}
                checked={this.checked}
                indeterminate={this.isIndeterminate}
                label={this.checked ? this.labelDesselecionado : this.labelSelecionado}
                aria-label={this.checked ? this.labelDesselecionado : this.labelSelecionado}
                onCheckedChange={this.handleChange.bind(this)}
              ></br-checkbox>
            </div>
          </div>
          <div class="mb-1 ml-2">
            <slot></slot>
          </div>
        </div>
      </Host>
    )
  }
}

let checkgroupId = 0
