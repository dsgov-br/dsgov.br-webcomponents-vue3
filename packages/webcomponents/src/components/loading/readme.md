## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/loading?tab=designer).

## Propriedades

| Propriedade       | Atributo           | Descrição                                                                                                                                                                                    | Tipo      | Valor padrão                    |
| ----------------- | ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------------------------------- |
| `customId`        | `custom-id`        | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                       | `string`  | ```br-loading-${loadingId++}``` |
| `isMedium`        | `is-medium`        | Se verdadeiro, ajusta o tamanho do indicador de carregamento para o tamanho médio.                                                                                                           | `boolean` | `false`                         |
| `isProgress`      | `is-progress`      | Determina o tipo de indicador a ser exibido: - Se verdadeiro, exibirá uma barra de progresso com a porcentagem de progresso. - Se falso, exibirá um indicador de carregamento indeterminado. | `boolean` | `false`                         |
| `label`           | `label`            | Texto a ser exibido abaixo do indicador de carregamento. Pode ser usado para fornecer informações adicionais ou descritivas sobre o estado do carregamento.                                  | `string`  | ---                             |
| `progressPercent` | `progress-percent` | Define a porcentagem de progresso a ser exibida na barra de progresso. Deve ser um valor numérico entre 0 e 100. Ignorado se `isProgress` estiver definido como falso.                       | `number`  | ---                             |






## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-loading label="Carregando..." is-medium></br-loading>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-loading is-progress progress-percent="100"></br-loading>
</div>
```

---
## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### br-loading - Guia de Migração de Web Components de Vue para StencilJS

Este guia detalha as mudanças necessárias para migrar o componente `br-loading` de Vue para StencilJS, incluindo propriedades, eventos e estrutura.

#### Propriedades

| Propriedade Vue | Propriedade Stencil | Descrição                                          | Tipo      | Padrão  |
| :-------------- | :------------------ | :------------------------------------------------- | :-------- | :------ |
| -               | -                   | Atributos ARIA herdados do elemento host.          | `object`  | `{}`    |
| -               | `label`             | Texto exibido abaixo do indicador de carregamento. | `string`  | `''`    |
| -               | `medium`            | Define o tamanho médio para o componente.          | `boolean` | `false` |
| -               | `percent`           | Porcentagem de progresso (se `progress` for true). | `number`  | `null`  |
| -               | `progress`          | Indica se a barra de progresso está visível.       | `boolean` | `false` |

#### Eventos

| Evento                | Descrição                                                                                       |
| :-------------------- | :---------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`  | Emitido após o componente ser carregado.                                                        |
| `brComponentWillLoad` | Emitido antes do componente ser carregado. É aqui que os atributos ARIA herdados são coletados. |
| `brConnectedCallback` | Emitido quando o componente é conectado ao DOM.                                                 |

#### Estrutura

- O componente renderiza um elemento `div` com a classe `br-loading` e atributos ARIA para acessibilidade.
- Se `progress` for `true`, uma barra de progresso com duas máscaras é exibida.
- Se `progress` for `false`, um indicador de carregamento indeterminado e um rótulo (`label`) são exibidos.
- A classe `medium` pode ser adicionada para alterar o tamanho do componente.

#### Exemplo de Uso

```html
<br-loading progress percent="50" label="Carregando..."></br-loading>
```

#### Considerações Finais

- Para eventos do ciclo de vida do StencilJS (`brComponentWillLoad`, etc.), consulte a documentação oficial em [https://stenciljs.com/docs/component-lifecycle](https://stenciljs.com/docs/component-lifecycle).
- Certifique-se de que os estilos do componente estejam corretamente definidos no arquivo `loading.scss`.
- Para suporte ou dúvidas adicionais, consulte a equipe do Padrão Digital de Governo em [https://discord.gg/NkaVZERAT7](https://discord.gg/NkaVZERAT7).