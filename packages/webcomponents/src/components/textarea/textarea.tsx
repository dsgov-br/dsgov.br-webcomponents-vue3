import {
  AttachInternals,
  Component,
  Element,
  Event,
  EventEmitter,
  h,
  Host,
  Method,
  Prop,
  State,
  Watch,
} from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/textarea?tab=designer).
 *
 * @slot default - Espaço de conteúdo do textarea. Geralmente usado para incluir texto adicional ou instruções para o usuário.
 */
@Component({
  tag: 'br-textarea',
  styleUrl: 'textarea.scss',
  shadow: true,
  formAssociated: true,
})
export class Textarea {
  @Element() el: HTMLBrTextareaElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Valor exibido no textarea.
   * Pode ser alterado pelo usuário se a propriedade `readonly` não estiver ativa.
   */
  @Prop({ mutable: true, reflect: true }) value: string

  /**
   * Texto exibido como rótulo do input.
   */
  @Prop({ reflect: true }) readonly label: string

  /**
   * Texto exibido dentro do input quando está vazio, fornecendo uma dica ou sugestão ao usuário.
   */
  @Prop({ reflect: true }) readonly placeholder: string

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-textarea-${textareaId++}`

  /**
   * Indica se o textarea está desabilitado. Quando verdadeiro, o usuário não pode interagir com o campo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Número máximo de caracteres permitidos no textarea. Se definido como 0, não há limite.
   */
  @Prop({ reflect: true }) readonly maxlength: number = 0

  /**
   * Mostra o contador com a quantidade máxima de caracteres.
   */
  @Prop({ reflect: true }) readonly showCounter: boolean = false

  /**
   * Define o estado visual do componente, podendo ser 'danger', 'success' ou 'warning'.
   */
  @Prop({ reflect: true }) readonly state: 'danger' | 'success' | 'warning'
  /**
   * Ajusta a densidade do componente, podendo ser 'small', 'medium' ou 'large'.
   */
  @Prop({ reflect: true }) readonly density: 'small' | 'medium' | 'large' = 'medium'

  /**
   * Se verdadeiro, o rótulo e o input estarão na mesma linha (layout inline).
   */
  @Prop({ reflect: true }) readonly isInline: boolean = false

  /**
   * Comprimento atual do texto no textarea. É atualizado sempre que o valor do textarea muda.
   */
  @State() currentLength: number

  componentWillLoad() {
    this.currentLength = this.value?.length
  }

  /**
   * Valor atualizado do textarea
   */
  @Event() valueChange: EventEmitter<string>

  @Watch('value')
  valueChanged() {
    this.currentLength = this.value?.length
  }

  private readonly handleInput = (event: Event) => {
    this.value = (event.target as HTMLInputElement).value
    this.currentLength = this.value?.length
    this.valueChange.emit(this.value)
  }

  /**
   * Define um novo valor para o textarea.
   * @param newValue - O novo valor a ser definido.
   */
  @Method()
  async setValue(newValue: string) {
    this.value = newValue
    this.currentLength = this.value?.length
    this.valueChange.emit(this.value)
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-textarea': true,
      small: this.density === 'small',
      medium: this.density === 'medium',
      large: this.density === 'large',
      success: this.state === 'success',
      danger: this.state === 'danger',
      warning: this.state === 'warning',
    }
  }

  render() {
    const caracteresRestantes: number = this.maxlength - this.currentLength
    const mostrarLimite = this.currentLength > 0

    return (
      <Host>
        <div class={this.getCssClassMap()}>
          <div class={this.isInline ? 'row' : ''}>
            {this.isInline ? (
              <div class="col-auto pt-half">
                <label htmlFor={this.customId}>{this.label}</label>
              </div>
            ) : (
              <label htmlFor={this.customId}>{this.label}</label>
            )}
            <div class={this.isInline ? 'col' : ''}>
              <textarea
                id={this.customId}
                value={this.value}
                placeholder={this.placeholder}
                disabled={this.disabled}
                onInput={this.handleInput}
                {...(this.maxlength > 0 ? { maxLength: this.maxlength } : {})}
              ></textarea>
              <div class="text-base mt-1">
                {this.showCounter && (
                  <div>
                    {mostrarLimite ? (
                      <span class="restantes" aria-live="polite" role="status" id="limitmax">
                        Restam <strong>{caracteresRestantes}</strong> caracteres
                      </span>
                    ) : (
                      <span class="limit" aria-live="polite">
                        Limite máximo de <strong>{this.maxlength}</strong> caracteres
                      </span>
                    )}
                  </div>
                )}
              </div>
              <slot></slot>
            </div>
          </div>
        </div>
      </Host>
    )
  }
}

let textareaId = 0
