## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/textarea?tab=designer).

## Propriedades

| Propriedade   | Atributo       | Descrição                                                                                                | Tipo                                 | Valor padrão                      |
| ------------- | -------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------ | --------------------------------- |
| `customId`    | `custom-id`    | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                   | `string`                             | ```br-textarea-${textareaId++}``` |
| `density`     | `density`      | Ajusta a densidade do componente, podendo ser 'small', 'medium' ou 'large'.                              | `"large" \| "medium" \| "small"`     | `'medium'`                        |
| `disabled`    | `disabled`     | Indica se o textarea está desabilitado. Quando verdadeiro, o usuário não pode interagir com o campo.     | `boolean`                            | `false`                           |
| `isInline`    | `is-inline`    | Se verdadeiro, o rótulo e o input estarão na mesma linha (layout inline).                                | `boolean`                            | `false`                           |
| `label`       | `label`        | Texto exibido como rótulo do input.                                                                      | `string`                             | ---                               |
| `maxlength`   | `maxlength`    | Número máximo de caracteres permitidos no textarea. Se definido como 0, não há limite.                   | `number`                             | `0`                               |
| `placeholder` | `placeholder`  | Texto exibido dentro do input quando está vazio, fornecendo uma dica ou sugestão ao usuário.             | `string`                             | ---                               |
| `showCounter` | `show-counter` | Mostra o contador com a quantidade máxima de caracteres.                                                 | `boolean`                            | `false`                           |
| `state`       | `state`        | Define o estado visual do componente, podendo ser 'danger', 'success' ou 'warning'.                      | `"danger" \| "success" \| "warning"` | ---                               |
| `value`       | `value`        | Valor exibido no textarea. Pode ser alterado pelo usuário se a propriedade `readonly` não estiver ativa. | `string`                             | ---                               |

## Eventos

| Evento        | Descrição                    | Tipo                  |
| ------------- | ---------------------------- | --------------------- |
| `valueChange` | Valor atualizado do textarea | `CustomEvent<string>` |

## Métodos

### `setValue(newValue: string) => Promise<void>`

Define um novo valor para o textarea.

#### Parâmetros

| Nome       | Tipo     | Descrição                      |
| ---------- | -------- | ------------------------------ |
| `newValue` | `string` | - O novo valor a ser definido. |

#### Retorna

Tipo: `Promise<void>`

## Slots

| Slot        | Descrição                                                                                                   |
| ----------- | ----------------------------------------------------------------------------------------------------------- |
| `"default"` | Espaço de conteúdo do textarea. Geralmente usado para incluir texto adicional ou instruções para o usuário. |

## Exemplos

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-textarea
    class="mr-2"
    show-counter
    density="small"
    maxlength="100"
    label="Textarea com contador"
    defaultId="meu-textarea-contador"
    placeholder="Digite somente números"
  ></br-textarea>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-textarea label="Densidade baixa" density="small" class="mr-2" placeholder="Digite somente números"></br-textarea>
  <br-textarea label="Densidade alta" density="large" class="mr-2" placeholder="Digite somente números"> </br-textarea>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4"></div>
<br-textarea
  is-inline
  density="small"
  state="success"
  label="Label a esquerda"
  placeholder="Exemplo de textarea com sucesso"
>
  <p class="text-base mt-1">Texto auxiliar ao preenchimento.</p>
</br-textarea>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-textarea state="success" density="small" class="mr-2" placeholder="Exemplo de textarea com sucesso"></br-textarea>

  <br-textarea state="danger" density="large" class="mr-2" placeholder="Exemplo de textarea com erro"> </br-textarea>

  <br-textarea density="large" class="mr-2" disabled placeholder="Exemplo de textarea desabilitado"> </br-textarea>
</div>
```

---

## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)
