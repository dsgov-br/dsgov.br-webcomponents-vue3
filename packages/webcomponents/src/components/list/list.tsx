import { AttachInternals, Component, Element, h, Host, Prop } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/list?tab=designer).
 *
 * @slot default - Slot padrão para inclusão dos itens da lista.
 * @slot header - Slot nomeado para inclusão de ações relacionadas ao cabeçalho do list (uso de botões por exemplo).
 */
@Component({
  tag: 'br-list',
  styleUrl: 'list.scss',
  shadow: true,
  formAssociated: true,
})
export class List {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrListElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Define o cabeçalho para a lista.
   */
  @Prop({ reflect: true }) readonly header: string

  /**
   * Indica se a lista será horizontal. Por padrão, a lista é vertical.
   */
  @Prop({ reflect: true }) readonly isHorizontal: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-list-${listId++}`

  /**
   * Indica que o divider para o título da lista estará oculto.
   */
  @Prop({ reflect: true }) readonly hideHeaderDivider: boolean = false

  componentDidLoad() {
    if (this.isHorizontal) {
      const slotItems = Array.from(this.el.querySelectorAll('br-item'))

      slotItems.forEach((item) => {
        const nextElement = item.nextElementSibling
        if (nextElement?.classList.contains('br-divider')) {
          const divider = nextElement as HTMLElement
          divider.style.borderWidth = '0 1px 0 0'
          divider.style.width = '1px'
        }
      })
    }

    const items = Array.from(this.el.querySelectorAll('br-item'))
    items.forEach((item) => {
      item.setAttribute('role', 'listitem')
    })
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-list': true,
      horizontal: this.isHorizontal,
    }
  }

  private hasSlotContent(slotName: string): boolean {
    return this.el.querySelector(`[slot="${slotName}"]`) !== null
  }
  private readonly renderHeader = () => {
    if (this.header?.length > 0 || this.hasSlotContent('header')) {
      return (
        <div class="header">
          {this.header && <div class="title">{this.header}</div>}
          {this.hasSlotContent('header') && (
            <div class="d-flex">
              <slot name="header"></slot>
            </div>
          )}
        </div>
      )
    }
    return null
  }

  private renderDivider() {
    if ((this.header || this.hasSlotContent('header')) && !this.hideHeaderDivider) {
      return <div class="br-divider"></div>
    }
    return null
  }

  render() {
    return (
      <Host>
        <div class={this.getCssClassMap()} role="list" id={this.customId}>
          {this.renderHeader()}
          {this.renderDivider()}
          <slot></slot>
        </div>
      </Host>
    )
  }
}

let listId = 0
