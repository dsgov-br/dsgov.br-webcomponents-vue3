## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/list?tab=designer).

## Propriedades

| Propriedade         | Atributo              | Descrição                                                                              | Tipo      | Valor padrão              |
| ------------------- | --------------------- | -------------------------------------------------------------------------------------- | --------- | ------------------------- |
| `customId`          | `custom-id`           | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado. | `string`  | ```br-list-${listId++}``` |
| `header`            | `header`              | Define o cabeçalho para a lista.                                                       | `string`  | ---                       |
| `hideHeaderDivider` | `hide-header-divider` | Indica que o divider para o título da lista estará oculto.                             | `boolean` | `false`                   |
| `isHorizontal`      | `is-horizontal`       | Indica se a lista será horizontal. Por padrão, a lista é vertical.                     | `boolean` | `false`                   |



## Slots

| Slot        | Descrição                                                                                          |
| ----------- | -------------------------------------------------------------------------------------------------- |
| `"default"` | Slot padrão para inclusão dos itens da lista.                                                      |
| `"header"`  | Slot nomeado para inclusão de ações relacionadas ao cabeçalho do list (uso de botões por exemplo). |



## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-list role="list" header="Densidade alta (padrão)">
    <br-item role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
  </br-list>

  <br-list role="list" header="Densidade média">
    <br-item density="middle" role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item density="middle" role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item density="middle" role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
  </br-list>

  <br-list role="list" header="Densidade baixa">
    <br-item density="small" role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item density="small" role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item density="small" role="listitem">
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
  </br-list>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-list list-title="Por rótulo">
    <br-item>RÓTULO 01</br-item>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
    <br-item>RÓTULO 02</br-item>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
  </br-list>

  <br-list list-title="Por separador">
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Sub-item
      <span slot="end">META</span>
    </br-item>
  </br-list>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-list list-title="Título (opcional)" is-horizontal>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
    <span class="br-divider"></span>
    <br-item>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <span slot="end">META</span>
    </br-item>
  </br-list>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-list list-title="Itens selecionáveis">
    <br-item is-interactive>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <br-checkbox
        slot="end"
        class="mb-1"
        id="check-01"
        name="check-01"
        label="Rótulo do Checkbox 01"
        has-hidden-label
      ></br-checkbox>
    </br-item>
    <span class="br-divider"></span>
    <br-item is-interactive>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <br-checkbox
        slot="end"
        class="mb-1"
        id="check-02"
        name="check-02"
        label="Rótulo do Checkbox 02"
        has-hidden-label
      ></br-checkbox>
    </br-item>
    <span class="br-divider"></span>
    <br-item is-interactive>
      <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
      Texto principal
      <br-checkbox
        slot="end"
        class="mb-1"
        id="check-03"
        name="check-03"
        label="Rótulo do Checkbox 03"
        has-hidden-label
      ></br-checkbox>
    </br-item>
    <span class="br-divider"></span>
  </br-list>
</div>
```
## Dependências

### Usado por

 - [br-select](../select)

### Gráfico
```mermaid
graph TD;
  br-select --Depende---> br-list
  click br-list "src/components/list" "Link para a documentação do componente list"
  class br-list depComponent
  class br-list mainComponent
```

---
# List - Guia de Migração de Web Components de Vue para StencilJS

## Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue    | Propriedade Stencil | Descrição                                                                       | Tipo      | Padrão      |
| ------------------ | ------------------- | ------------------------------------------------------------------------------- | --------- | ----------- |
| `dataToggle`       | ---                 | Ativa a lista expansiva, onde os `br-item` filhos abrem/fecham ao clicá-los.    | `boolean` | `false`     |
| `dataUnique`       | ---                 | Ativa a abertura de um `br-item` da lista por vez quando a mesma é data-toggle. | `boolean` | `false`     |
| `density`          | ---                 | Níveis de densidade agora são definidos no componente `br-item`.                | `string`  | `undefined` |
| `hideTitleDivider` | `hideHeaderDivider` | Oculta o "divider" abaixo do header da lista (título e botões de ação).         | `boolean` | `false`     |
| `horizontal`       | `isHorizontal`      | Indica se a lista será horizontal. Por padrão, a lista é vertical.              | `boolean` | `false`     |
| `spaceBetween`     | ---                 | Indica que os itens da lista terão espaçamento uniforme entre eles.             | `boolean` | `false`     |
| `title`            | `listTitle`         | Define o título para a lista.                                                   | `string`  | `undefined` |

## Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

## Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).