import { AttachInternals, Component, Element, Event, EventEmitter, h, Host, Listen, Prop, State } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'
/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/item?tab=designer).
 *
 * @slot default - Área de conteúdo, podendo conter qualquer componente, exceto botões primários e componentes relacionados à navegação (como carrosséis, paginações, abas, menus, etc.).
 *
 * @slot start - Área de recursos visuais, podendo conter elementos como ícones, avatares e mídias.
 *
 * @slot end - Área de recursos complementares, podendo conter componentes interativos, metadados e informações adicionais.
 */
@Component({
  tag: 'br-item',
  styleUrl: 'item.scss',
  shadow: true,
  formAssociated: true,
})
export class Item {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrItemElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Desativa o item, tornando-o não interativo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-item-${itemId++}`

  /**
   * Indica se o item está no estado ativo.
   * Se definido como verdadeiro, o item será exibido como ativo.
   */
  @Prop({ reflect: true }) readonly isActive: boolean = false

  /**
   * Indica se o item está no estado selecionado.
   * Se definido como verdadeiro, o item será exibido como selecionado.
   */
  @Prop({ reflect: true }) readonly isSelected: boolean = false

  /**
   * Marca o item como interativo, permitindo que toda a superfície do item seja clicável.
   */
  @Prop({ reflect: true }) readonly isInteractive: boolean = false

  /**
   * URL ou caminho para o qual o usuário será direcionado ao clicar no item. Quando definido, o item será renderizado como um link.
   */
  @Prop({ reflect: true }) readonly href?: string

  /**
   * Define o alvo do link quando `href` está presente. Pode ser:
   * - `_blank` para abrir em uma nova aba,
   * - `_self` para abrir na mesma aba,
   * - `_parent` para abrir na aba pai,
   * - `_top` para abrir na aba superior.
   */
  @Prop({ reflect: true }) readonly target?: '_blank' | '_self' | '_parent' | '_top'

  /**
   * Quando definido como `true`, o item será tratado como um botão.
   */
  @Prop({ reflect: true }) readonly isButton: boolean = false

  /**
   * Tipo do botão, aplicável apenas se `isButton` for `true`. Pode ser:
   * - `'submit'` para enviar um formulário,
   * - `'reset'` para redefinir um formulário,
   * - `'button'` para um botão padrão.
   */
  @Prop({ reflect: true, mutable: true }) type: 'submit' | 'reset' | 'button'

  /**
   *  Define um valor associado ao br-item quando renderizado como um botão, utilizado em contextos de formulário.
   */
  @Prop({ reflect: true }) readonly value?: string

  /**
   * Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.
   */
  @Prop({ reflect: true }) readonly density: 'large' | 'medium' | 'small' = 'medium'

  // Propriedade interna para verificar a presença de conteúdo no slot default
  @State() hasSlotDefault: boolean = false

  @State() hasSlotStart: boolean = false

  @State() hasSlotEnd: boolean = false

  // Propriedade interna para armazenar o valor selecionado
  @State() selectedValue: boolean = false

  // Propriedade para rastrear a presença de rádios dentro do item
  private hasRadios: boolean = false

  @Listen('checkedChange', { target: 'document' })
  handleRadioChange() {
    requestAnimationFrame(() => {
      this.updateRadioSelectionState()
    })
  }

  @Listen('checkedChange', { target: 'document' })
  handleCheckboxChange(event: Event) {
    const target = event.target as HTMLElement

    if (this.el.contains(target)) {
      this.updateCheckboxSelectionState()
    }
  }

  /**
   * Evento customizado emitido quando o item é clicado, aplicável apenas se o item for um botão (`<button>`).
   * Pode ser utilizado para ações personalizadas, exceto quando o item está desativado.
   */
  @Event({ composed: true, cancelable: true, bubbles: true }) brDidClick: EventEmitter

  /**
   * Evento customizado aplicável para todos os tipos de elementos (`div`, `button`, `a`), emitido quando o item é selecionado e desde que a propriedade `isInteractive` esteja presente.
   */
  @Event({ bubbles: true, composed: true }) brDidSelect: EventEmitter<{ selected: boolean }>

  componentWillLoad() {
    this.hasSlotContentDefault()
    this.hasSlotStart = this.hasSlotContent('start')
    this.hasSlotEnd = this.hasSlotContent('end')
    this.selectedValue = this.isSelected
    if (this.isButton) {
      this.type = this.type || 'button'
    }
    const radios = Array.from(this.el.querySelectorAll('br-radio'))
    const checkboxes = Array.from(this.el.querySelectorAll('br-checkbox'))
    this.hasRadios = radios.length > 0

    if (this.isSelected) {
      if (checkboxes.length > 0) {
        checkboxes.forEach((checkbox) => (checkbox.checked = true))
      }
      if (radios.length > 0) {
        radios[0].checked = true
      }
    }

    this.updateRadioSelectionState()
    this.updateCheckboxSelectionState()
  }

  private hasSlotContentDefault(): void {
    this.hasSlotDefault = this.el.innerHTML !== ''
  }

  private hasSlotContent(slotName: string): boolean {
    return this.el.querySelector(`[slot="${slotName}"]`) !== null
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-item': true,
      disabled: this.disabled,
      active: this.isActive,
      selected: this.selectedValue,
      'py-3': this.density === 'medium',
      'py-4': this.density === 'small',
    }
  }

  private getTagType(): string {
    if (this.isButton) {
      return 'button'
    }
    if (this.href?.length > 0) {
      return 'a'
    }
    return 'div'
  }

  private getAttributes() {
    const TagType = this.getTagType()
    const baseAttributes =
      TagType === 'button' ? { type: this.type, value: this.value } : { href: this.href, target: this.target }

    return {
      ...baseAttributes,
      'aria-disabled': this.disabled ? 'true' : null,
      'aria-current': this.isActive ? 'true' : null,
      'data-toggle': this.isInteractive ? 'selection' : null,
      'is-selected': this.selectedValue ? true : null,
    }
  }

  private updateCheckboxSelectionState() {
    if (this.hasRadios) return

    const checkboxes = Array.from(this.el.querySelectorAll('br-checkbox'))

    this.selectedValue = checkboxes.some((checkbox) => checkbox.checked)
    this.brDidSelect.emit({ selected: this.selectedValue })
  }

  private updateRadioSelectionState() {
    if (!this.hasRadios) return

    const radios = Array.from(this.el.querySelectorAll('br-radio'))

    this.selectedValue = radios.some((radio) => radio.checked)
    this.brDidSelect.emit({ selected: this.selectedValue })
  }

  // Função para verificar se o clique foi dentro do slot do item.
  private isClickInsideInteractiveElement(target: HTMLElement): boolean {
    const interactiveSelectors = 'br-checkbox, br-radio, br-input, br-button, br-select, br-textarea'
    const interactiveElements = this.el.querySelectorAll(interactiveSelectors)
    return Array.from(interactiveElements).some((el) => el.contains(target) || target === el)
  }

  private updateCheckboxes() {
    const checkboxes = Array.from(this.el.querySelectorAll('br-checkbox'))

    checkboxes.forEach((checkbox) => {
      checkbox.checked = this.selectedValue
    })
  }

  private readonly handleClick = (event: MouseEvent) => {
    const clickedElement = event.target as HTMLElement

    if (this.disabled) {
      event.preventDefault()
      event.stopPropagation()
    }

    if (this.hasRadios) {
      return
    }

    if (this.isButton) {
      this.brDidClick.emit()
    } else if (this.isInteractive && !this.isClickInsideInteractiveElement(clickedElement)) {
      this.selectedValue = !this.selectedValue
      this.brDidSelect.emit({ selected: this.selectedValue })
      this.updateCheckboxes()
    }

    event.stopPropagation()
  }

  render() {
    const TagType = this.getTagType()
    const attributes = this.getAttributes()
    const classList = this.getCssClassMap()
    return (
      <Host>
        <TagType class={classList} {...attributes} onClick={this.handleClick} id={this.customId}>
          {this.hasSlotStart || this.hasSlotEnd ? (
            <div class="row align-items-center">
              {this.hasSlotStart && (
                <div class="col-auto">
                  <slot name="start"></slot>
                </div>
              )}

              <div class="col">
                <slot></slot>
              </div>

              {this.hasSlotEnd && (
                <div class="col-auto">
                  <slot name="end"></slot>
                </div>
              )}
            </div>
          ) : (
            <slot></slot>
          )}
        </TagType>
      </Host>
    )
  }
}

let itemId = 0
