## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/item?tab=designer).

## Propriedades

| Propriedade     | Atributo         | Descrição                                                                                                                                                                                                      | Tipo                                         | Valor padrão              |
| --------------- | ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | ------------------------- |
| `customId`      | `custom-id`      | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                         | `string`                                     | ```br-item-${itemId++}``` |
| `density`       | `density`        | Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.                                                                                                            | `"large" \| "medium" \| "small"`             | `'medium'`                |
| `disabled`      | `disabled`       | Desativa o item, tornando-o não interativo.                                                                                                                                                                    | `boolean`                                    | `false`                   |
| `href`          | `href`           | URL ou caminho para o qual o usuário será direcionado ao clicar no item. Quando definido, o item será renderizado como um link.                                                                                | `string`                                     | ---                       |
| `isActive`      | `is-active`      | Indica se o item está no estado ativo. Se definido como verdadeiro, o item será exibido como ativo.                                                                                                            | `boolean`                                    | `false`                   |
| `isButton`      | `is-button`      | Quando definido como `true`, o item será tratado como um botão.                                                                                                                                                | `boolean`                                    | `false`                   |
| `isInteractive` | `is-interactive` | Marca o item como interativo, permitindo que toda a superfície do item seja clicável.                                                                                                                          | `boolean`                                    | `false`                   |
| `isSelected`    | `is-selected`    | Indica se o item está no estado selecionado. Se definido como verdadeiro, o item será exibido como selecionado.                                                                                                | `boolean`                                    | `false`                   |
| `target`        | `target`         | Define o alvo do link quando `href` está presente. Pode ser: - `_blank` para abrir em uma nova aba, - `_self` para abrir na mesma aba, - `_parent` para abrir na aba pai, - `_top` para abrir na aba superior. | `"_blank" \| "_parent" \| "_self" \| "_top"` | ---                       |
| `type`          | `type`           | Tipo do botão, aplicável apenas se `isButton` for `true`. Pode ser: - `'submit'` para enviar um formulário, - `'reset'` para redefinir um formulário, - `'button'` para um botão padrão.                       | `"button" \| "reset" \| "submit"`            | ---                       |
| `value`         | `value`          | Define um valor associado ao br-item quando renderizado como um botão, utilizado em contextos de formulário.                                                                                                   | `string`                                     | ---                       |

## Eventos

| Evento        | Descrição                                                                                                                                                                                     | Tipo                                  |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------- |
| `brDidClick`  | Evento customizado emitido quando o item é clicado, aplicável apenas se o item for um botão (`<button>`). Pode ser utilizado para ações personalizadas, exceto quando o item está desativado. | `CustomEvent<any>`                    |
| `brDidSelect` | Evento customizado aplicável para todos os tipos de elementos (`div`, `button`, `a`), emitido quando o item é selecionado e desde que a propriedade `isInteractive` esteja presente.          | `CustomEvent<{ selected: boolean; }>` |


## Slots

| Slot        | Descrição                                                                                                                                                              |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `"default"` | Área de conteúdo, podendo conter qualquer componente, exceto botões primários e componentes relacionados à navegação (como carrosséis, paginações, abas, menus, etc.). |
| `"end"`     | Área de recursos complementares, podendo conter componentes interativos, metadados e informações adicionais.                                                           |
| `"start"`   | Área de recursos visuais, podendo conter elementos como ícones, avatares e mídias.                                                                                     |



## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-item>
    <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
    Densidade baixa
    <span slot="end">META</span>
  </br-item>
  <br-item density="medium">
    <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
    Densidade padrão
    <span slot="end">META</span>
  </br-item>
  <br-item density="small">
    <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
    Densidade alta
    <span slot="end">META</span>
  </br-item>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <div class="row">
    <div class="col-md-5 mx-3 mb-4">
      <br-item is-interactive>
        <br-checkbox id="check-01" name="check-01" label="Rótulo do Checkbox 01"></br-checkbox>
      </br-item>
      <br-item is-interactive>
        <br-checkbox id="check-02" name="check-02" label="Rótulo do Checkbox 02"></br-checkbox>
      </br-item>
    </div>

    <div class="col-md-5 mx-3 mb-4">
      <br-item is-interactive>
        <br-radio id="radio-01" name="radio" label="Rótulo do Radio 01"></br-radio>
      </br-item>
      <br-item is-interactive>
        <br-radio id="radio-02" name="radio" label="Rótulo do Radio 02"></br-radio>
      </br-item>
    </div>
  </div>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-item class="exemplo" disabled>
    <img class="rounded" src="https://picsum.photos/40/40" alt="imagem de exemplo 1" />
    <span>Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
  </br-item>
</div>
```
```html
<div class="d-flex flex-column flex-wrap justify-content-evenly mt-5 p-4">
  <br-item>
    <img src="https://picsum.photos/40/40" alt="imagem de exemplo 1" />
    <span>Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
  </br-item>
  <span class="br-divider"></span>
  <br-item>
    Lorem ipsum <a href="javascript:void(0);">dolor</a>, sit amet consectetur
    <a href="javascript:void(0);">adipisicing</a> elit.
    <br-button shape="circle" aria-label="Interagir com item">
      <br-icon icon-name="fa6-solid:city"></br-icon>
    </br-button>
  </br-item>
  <span class="br-divider"></span>
  <br-item href="javascript: void(0)">Item link </br-item>
  <span class="br-divider"></span>
  <br-item is-button> Item botão </br-item>
  <br-item id="myButton" is-button onclick="console.log(this)">Clique aqui</br-item>
</div>
```

---
# Item - Guia de Migração de Web Components de Vue para StencilJS

## Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil | Descrição                                                                | Tipo      | Padrão      |
| --------------- | ------------------- | ------------------------------------------------------------------------ | --------- | ----------- |
| ---             | `density`           | Define a densidade do item ('large', 'middle', 'small').                 | `string`  | `large`     |
| ---             | `isButton`          | Define se o item deve ser renderizado como um botão.                     | `boolean` | `false`     |
| ---             | `isInteractive`     | Indica se toda a superfície do item é interativa.                        | `boolean` | `false`     |
| ---             | `target`            | Especifica onde abrir o link (usado com href).                           | `string`  | `undefined` |
| ---             | `type`              | Tipo do botão, só utilizado se isButton for true.                        | `boolean` | `false`     |
| `active`        | `isActive`          | Coloca o item como ênfase, destacando-o dos demais (estado ativo).       | `boolean` | `false`     |
| `hover`         | ---                 | Efeito Hover, destaca o item ao passar o mouse por cima do mesmo.        | `boolean` | `false`     |
| `open`          | ---                 | Indica se o item está aberto/fechado quando o br-list pai é data-toggle. | `boolean` | `false`     |
| `selected`      | `isSelected`        | Indica se o item está ou não selecionado.                                | `boolean` | `false`     |
| `title`         | ---                 | Título visível para o item quando o br-list pai é data-toggle.           | `string`  | `undefined` |

## Como Utilizar Eventos

Entenda melhor como utilizar os eventos para o componente `<br-item></br-item>` no Vue e em Stencil:

### Vue

| Evento            | Descrição                                             |
| ----------------- | ----------------------------------------------------- |
| `toggle-open`     | Disparado quando a prop open é modificada.            |
| `toggle-selected` | Emitido quando quando a prop `selected` é modificada. |

### StencilJS

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidClick`, a propriedade será chamada `onBrDidClick`.

```html
<!-- StencilJS -->
<br-item onEventoPersonalizado="handleEvent"></br-item>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-item></br-item>
</template>

<script>
  const itemElement = document.querySelector('br-item')
  itemElement.addEventListener('brDidClick', (event) => {
    /* your listener */
  })
</script>
```

## Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

## Exemplos

Exemplos de uso do componente Item no Stencil:

```html
<br-item is-button is-selected onBrDidClick="handleClick"> Botão Item </br-item>
```

```html
<br-item href="https://example.com" target="_blank"> Link Item </br-item>
```

```html
<br-item is-interactive>
  <br-checkbox> Item Interativo </br-checkbox>
</br-item>
```

## Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).