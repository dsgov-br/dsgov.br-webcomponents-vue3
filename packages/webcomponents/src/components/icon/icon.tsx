import { Component, Element, h, Host, Prop } from '@stencil/core'
import 'iconify-icon'

/**
 * O componente `br-icon` fornece uma maneira flexível e dinâmica de incorporar ícones nas aplicações.
 * Utilizando a biblioteca Iconify, ele permite o uso de uma ampla variedade de ícones.
 *
 * Com opções para especificar a altura e largura, adicionar classes CSS personalizadas, e definir como o ícone
 * deve ser rotacionado ou espelhado, o `br-icon` oferece aos desenvolvedores as ferramentas necessárias para
 * integrar ícones de forma consistente com o estilo e design de suas aplicações, melhorando a experiência do usuário
 * e a clareza das interfaces.
 *
 * Para mais informações sobre quais ícones estão disponíveis, consulte a documentação do [Iconify](https://iconify.design/).
 */
@Component({
  tag: 'br-icon',
  styleUrl: 'icon.scss',
  shadow: true,
})
export class Icon {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrIconElement

  /**
   * Nome do ícone a ser exibido, utilizando a biblioteca Iconify.
   * Este nome deve corresponder ao nome do ícone definido na biblioteca para que ele seja exibido corretamente.
   */
  @Prop({ reflect: true }) readonly iconName: string

  /**
   * Define a altura do ícone. Pode ser especificada em qualquer unidade CSS válida, como pixels (px), ems (em), rems (rem), etc.
   * O valor padrão é '16'.
   */
  @Prop({ reflect: true }) readonly height: string = '16'

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-icon-${iconId++}`

  /**
   * Define a largura do ícone. Pode ser especificada em qualquer unidade CSS válida, como pixels (px), ems (em), rems (rem), etc.
   * O valor padrão é '24'.
   */
  @Prop({ reflect: true }) readonly width: string = '24'

  /**
   * Permite adicionar classes CSS adicionais ao ícone.
   * Use esta propriedade para aplicar estilos personalizados ao ícone, além dos estilos padrão.
   */
  @Prop({ reflect: true }) readonly cssClasses?: string

  /**
   * Se definido como verdadeiro, o ícone será alinhado verticalmente ao texto ao seu redor.
   * Útil quando o ícone é usado em linha com texto para garantir que esteja alinhado corretamente com o texto.
   * O valor padrão é `false`.
   */
  @Prop({ reflect: true }) readonly isInline?: boolean = false

  /**
   * Define o ângulo de rotação do ícone.
   * Aceita valores como "90deg", "180deg" e "270deg" para rotacionar o ícone em diferentes ângulos.
   */
  @Prop({ reflect: true }) readonly rotate?: '90deg' | '180deg' | '270deg'

  /**
   * Define o tipo de espelhamento do ícone.
   * Aceita valores como "horizontal" para espelhar o ícone horizontalmente, "vertical" para espelhar verticalmente, ou ambos.
   */
  @Prop({ reflect: true }) readonly flip?: 'horizontal' | 'vertical'

  render() {
    return (
      <Host>
        <iconify-icon
          id={this.customId}
          flip={this.flip}
          width={this.width}
          class={this.cssClasses}
          icon={this.iconName}
          inline={this.isInline}
          height={this.height}
          rotate={this.rotate}
        ></iconify-icon>
      </Host>
    )
  }
}

let iconId = 0
