## Visão Geral

O componente `br-icon` fornece uma maneira flexível e dinâmica de incorporar ícones nas aplicações.
Utilizando a biblioteca Iconify, ele permite o uso de uma ampla variedade de ícones.

Com opções para especificar a altura e largura, adicionar classes CSS personalizadas, e definir como o ícone
deve ser rotacionado ou espelhado, o `br-icon` oferece aos desenvolvedores as ferramentas necessárias para
integrar ícones de forma consistente com o estilo e design de suas aplicações, melhorando a experiência do usuário
e a clareza das interfaces.

Para mais informações sobre quais ícones estão disponíveis, consulte a documentação do [Iconify](https://iconify.design/).

## Propriedades

| Propriedade  | Atributo      | Descrição                                                                                                                                                                                                                    | Tipo                              | Valor padrão              |
| ------------ | ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------- | ------------------------- |
| `cssClasses` | `css-classes` | Permite adicionar classes CSS adicionais ao ícone. Use esta propriedade para aplicar estilos personalizados ao ícone, além dos estilos padrão.                                                                               | `string`                          | ---                       |
| `customId`   | `custom-id`   | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                       | `string`                          | ```br-icon-${iconId++}``` |
| `flip`       | `flip`        | Define o tipo de espelhamento do ícone. Aceita valores como "horizontal" para espelhar o ícone horizontalmente, "vertical" para espelhar verticalmente, ou ambos.                                                            | `"horizontal" \| "vertical"`      | ---                       |
| `height`     | `height`      | Define a altura do ícone. Pode ser especificada em qualquer unidade CSS válida, como pixels (px), ems (em), rems (rem), etc. O valor padrão é '16'.                                                                          | `string`                          | `'16'`                    |
| `iconName`   | `icon-name`   | Nome do ícone a ser exibido, utilizando a biblioteca Iconify. Este nome deve corresponder ao nome do ícone definido na biblioteca para que ele seja exibido corretamente.                                                    | `string`                          | ---                       |
| `isInline`   | `is-inline`   | Se definido como verdadeiro, o ícone será alinhado verticalmente ao texto ao seu redor. Útil quando o ícone é usado em linha com texto para garantir que esteja alinhado corretamente com o texto. O valor padrão é `false`. | `boolean`                         | `false`                   |
| `rotate`     | `rotate`      | Define o ângulo de rotação do ícone. Aceita valores como "90deg", "180deg" e "270deg" para rotacionar o ícone em diferentes ângulos.                                                                                         | `"180deg" \| "270deg" \| "90deg"` | ---                       |
| `width`      | `width`       | Define a largura do ícone. Pode ser especificada em qualquer unidade CSS válida, como pixels (px), ems (em), rems (rem), etc. O valor padrão é '24'.                                                                         | `string`                          | `'24'`                    |

## Exemplos

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-icon icon-name="fa6-solid:f"></br-icon>
  <br-icon icon-name="fa6-solid:f" flip="horizontal"></br-icon>
  <br-icon icon-name="fa6-solid:f" flip="vertical"></br-icon>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-icon icon-name="fa-brands:chromecast"></br-icon>
  <br-icon icon-name="fa-regular:envelope"></br-icon>
  <br-icon icon-name="fa-solid:camera"></br-icon>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <p>Ícone alinhado <br-icon icon-name="fa-solid:eye" is-inline></br-icon> verticalmente ao texto</p>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-icon icon-name="fa-solid:biking"></br-icon>
  <br-icon icon-name="fa-solid:biking" rotate="90deg"></br-icon>
  <br-icon icon-name="fa-solid:biking" rotate="180deg"></br-icon>
  <br-icon icon-name="fa-solid:biking" rotate="270deg"></br-icon>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-icon icon-name="fa-regular:comment-dots" width="72" height="48"></br-icon>
</div>
```

## Dependências

### Usado por

- [br-avatar](../avatar)
- [br-input](../input)
- [br-message](../message)
- [br-select](../select)
- [br-tag](../tag)
- [br-upload](../upload)

### Gráfico

```mermaid
graph TD;
  br-avatar --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-input --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-message --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-select --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-tag --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-upload --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  class br-icon mainComponent
```

---
