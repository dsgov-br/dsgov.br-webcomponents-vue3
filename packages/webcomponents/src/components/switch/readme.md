## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/switch?tab=designer).

## Propriedades

| Propriedade     | Atributo         | Descrição                                                                                                                                                                                                                                                                                                                            | Tipo                             | Valor padrão                  |
| --------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------- | ----------------------------- |
| `checked`       | `checked`        | Define o estado de seleção do checkbox. Se definido como verdadeiro, o checkbox estará marcado. Caso contrário, estará desmarcado.                                                                                                                                                                                                   | `boolean`                        | `false`                       |
| `customId`      | `custom-id`      | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                                                                                                                               | `string`                         | ```br-switch-${switchId++}``` |
| `density`       | `density`        | O ajuste da densidade consiste em aumentar ou reduzir a área de interação do switch. Quanto menor for a densidade, maior a área de interação.                                                                                                                                                                                        | `"large" \| "medium" \| "small"` | `'medium'`                    |
| `disabled`      | `disabled`       | Desativa o switch, tornando-o não interativo.                                                                                                                                                                                                                                                                                        | `boolean`                        | `false`                       |
| `hasIcon`       | `has-icon`       | Adiciona um ícone ao switch para indicar a mudança de estado.                                                                                                                                                                                                                                                                        | `boolean`                        | `false`                       |
| `label`         | `label`          | Texto descritivo. Caso um slot seja utilizado para fornecer um texto alternativo, o valor desta propriedade será ignorado.                                                                                                                                                                                                           | `string`                         | ---                           |
| `labelOff`      | `label-off`      | Texto exibido quando o switch está desativado.                                                                                                                                                                                                                                                                                       | `string`                         | ---                           |
| `labelOn`       | `label-on`       | Texto exibido quando o switch está ativado.                                                                                                                                                                                                                                                                                          | `string`                         | ---                           |
| `labelPosition` | `label-position` | Posição do rótulo em relação ao switch.                                                                                                                                                                                                                                                                                              | `"left" \| "right" \| "top"`     | `'left'`                      |
| `name`          | `name`           | Define o nome do switch, que é utilizado para agrupar switches em formulários e identificar o campo. O valor é obrigatório e deve ser fornecido para garantir o correto funcionamento em formulários.                                                                                                                                | `string`                         | ---                           |
| `value`         | `value`          | Define o valor associado ao switch quando ele faz parte de um formulário nativo (`<form>`). Esse valor é enviado com o formulário quando o switch está selecionado. **Nota:** Esta propriedade não deve ser utilizada para determinar se o switch está selecionado; para verificar o estado de seleção, use a propriedade `checked`. | `string`                         | ---                           |

## Eventos

| Evento          | Descrição                                              | Tipo                   |
| --------------- | ------------------------------------------------------ | ---------------------- |
| `checkedChange` | Disparado depois que o valor do `checked` foi alterado | `CustomEvent<boolean>` |

## Métodos

### `toggleChecked() => Promise<void>`

Inverte o valor da prop `checked`

#### Retorna

Tipo: `Promise<void>`




## Slots

| Slot        | Descrição                                                                                                                               |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| `"default"` | Descreve o que o switch faz quando a alternância estiver ativada. A label passada por slot tem precedência sobre a propriedade 'label'. |



## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-switch id="switch-small" density="small" checked>Densidade Alta</br-switch>
  <br-switch id="switch-medium" checked>Densidade Média (Padrão)</br-switch>
  <br-switch id="switch-large" checked density="large">Densidade Baixa</br-switch>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-switch id="switch-icon" has-icon checked>Label</br-switch>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <div class="col-md mt-5"><br-switch id="switch-default" label="Prop Label" checked></br-switch></div>
  <div class="col-md mt-5"><br-switch id="switch-left" checked>Label na esquerda</br-switch></div>
  <div class="col-md mt-5">
    <br-switch id="switch-right" checked label-position="right">Label na direita</br-switch>
  </div>
  <div class="col-md mt-5"><br-switch id="switch-top" checked label-position="top">Label no topo</br-switch></div>
  <div class="col-md mt-5">
    <br-switch id="switch-label-01" checked label-on="Ligado" label-off="Desligado">Label</br-switch>
  </div>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-switch id="switch-disabled" checked disabled>Disabled</br-switch>
</div>
```

---
## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| **Propriedade Antiga** | **Propriedade Nova** | **Descrição**                                                                                              | **Tipo**                   | **Padrão** |
| ---------------------- | -------------------- | ---------------------------------------------------------------------------------------------------------- | -------------------------- | ---------- |
| `ariaLabel`            | `ariaLabel`          | [OPCIONAL] Acessibilidade: define uma cadeia de caracteres para descrever o elemento.                      | `string`                   | `-`        |
| `checked`              | `checked`            | [OPCIONAL] Se presente, indica que o estado inicial do switch é `true`.                                    | `boolean`                  | `false`    |
| `disabled`             | `disabled`           | [OPCIONAL] Se presente, indica que o switch está desabilitado.                                             | `boolean`                  | `false`    |
| `icon`                 | `hasIcon`            | [OPCIONAL] Se presente, adiciona um ícone padrão à chave de alternância.                                   | `boolean`                  | `false`    |
| `id`                   | `id`                 | [OPCIONAL] ID do componente. Também usado para conectar o rótulo correspondente.                           | `string`                   | `-`        |
| `label`                | `label`              | [OPCIONAL] Descreve o que o switch faz quando ativado. Sobrescreve o rótulo passado por slot, se presente. | `string`                   | `-`        |
| `labelChecked`         | `labelOn`            | [OPCIONAL] Rótulo para o switch ativado, auxiliando no entendimento de sua posição.                        | `string`                   | `null`     |
| `labelNotChecked`      | `labelOff`           | [OPCIONAL] Rótulo para o switch desativado, auxiliando no entendimento de sua posição.                     | `string`                   | `null`     |
| `name`                 | `name`               | [OPCIONAL] Define o valor do atributo `name` do checkbox subjacente.                                       | `string`                   | `-`        |
| `right`                | `labelPosition`      | [OPCIONAL] Se presente, posiciona o rótulo a direita do switch.                                            | `right \| left \| top`     | `left`     |
| `size`                 | `density`            | [OPCIONAL] Ajusta a densidade da área interativa do switch.                                                | `large \| medium \| small` | `medium`   |
| `top`                  | `labelPosition`      | [OPCIONAL] Se presente, posiciona o rótulo acima do switch.                                                | `right \| left \| top`     | `left`     |
| `value`                | `value`              | Valor associado ao checkbox quando o switch está ativado.                                                  | `string`                   | `-`        |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-switch></br-switch>` no Vue e em Stencil:

#### Vue

| Evento           | Descrição                                                            |
| ---------------- | -------------------------------------------------------------------- |
| `onChange`       | Evento emitido quando o estado do checkbox é modificado.             |
| `update:checked` | Evento emitido para fazer o two-way data binding com a prop checked. |

#### StencilJS

| Evento                      | Descrição                                                                                                     | Tipo                |
| --------------------------- | ------------------------------------------------------------------------------------------------------------- | ------------------- |
| `brConnectedCallback`       | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback).     | `CustomEvent<this>` |
| `brDidLoad`                 | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentdidload).      | `CustomEvent<this>` |
| `brDidRender`               | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback).     | `CustomEvent<this>` |
| `brDidSwitchToggle`         | Evento customizado emitido para sinalizar a mudança do estado "checked".                                      | `CustomEvent<any>`  |
| `brDidUpdate`               | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentdidupdate).    | `CustomEvent<this>` |
| `brDisconnectedCallback`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#disconnectedcallback).  | `CustomEvent<this>` |
| `brShouldUpdate`            | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentshouldupdate). | `CustomEvent<this>` |
| `brWillLoad`                | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentwillload).     | `CustomEvent<this>` |
| `brWillRender`              | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentwillrender).   | `CustomEvent<this>` |
| `brWillUpdate`              | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentdidupdate).    | `CustomEvent<this>` |
| `formAssociatedCallback`    | Componente foi inserido em um `<form>`                                                                        | `CustomEvent<this>` |
| `formDisassociatedCallback` | Componente foi removido de um `<form>`                                                                        | `CustomEvent<this>` |

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidSwitchToggle`, a propriedade será chamada `brDidSwitchToggle`.

```html

<template>
  <br-switch id="switch-example" brDidSwitchToggle={(event) => {
    console.log('Estado do switch alterado para:', event.detail);
  }} />
</template>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-switch></br-switch>
</template>

<script>
  const switchElement = document.querySelector('br-switch')
  switchElement.addEventListener('brDidSwitchToggle', (event) => {
    /* your listener */
  })
</script>
```

### Slots

O componente `br-switch` possui o slot `default` no StencilJs, que permite a personalização do label do switch.

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplos

Exemplos de uso do componente Switch no Stencil:

```html
<br-switch id="switch-default" label="Switch de Alternância" checked ariaLabel="Exemplo de switch de alternância">
</br-switch>
```

```html
<br-switch id="switch-with-labels" label="Notificações" labelOn="Ativado" labelOff="Desativado" checked> </br-switch>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).