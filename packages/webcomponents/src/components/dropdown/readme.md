## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/dropdown?tab=designer).

## Propriedades

| Propriedade | Atributo    | Descrição                                                                                                                                            | Tipo      | Valor padrão                      |
| ----------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | --------------------------------- |
| `customId`  | `custom-id` | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                               | `string`  | ```br-dropdown-${dropdownId++}``` |
| `isOpen`    | `is-open`   | Indica se o dropdown está aberto ou fechado. Esta propriedade é refletida no DOM e pode ser alterada externamente. O valor padrão é falso (fechado). | `boolean` | `false`                           |

## Eventos

| Evento             | Descrição                                        | Tipo                                     |
| ------------------ | ------------------------------------------------ | ---------------------------------------- |
| `brDropdownChange` | Evento emitido quando o estado do dropdown muda. | `CustomEvent<{ 'is-opened': boolean; }>` |

## Métodos

### `hide() => Promise<{ isOpen: boolean; }>`

Esconde o dropdown.
Define a propriedade `isOpen` como falsa e retorna o novo estado.
Este método é marcado com

#### Retorna

Tipo: `Promise<{ isOpen: boolean; }>`

### `open() => Promise<{ isOpen: boolean; }>`

Abre o dropdown.
Define a propriedade `isOpen` como verdadeira e retorna o novo estado.
Este método é marcado com

#### Retorna

Tipo: `Promise<{ isOpen: boolean; }>`

## Slots

| Slot        | Descrição                            |
| ----------- | ------------------------------------ |
| `"target"`  | Slot que recebe o elemento alvo      |
| `"trigger"` | Slot que recebe o elemento acionador |

## Exemplos

```html
<div class="mt-5 p-4">
  <p>Clique no menu abaixo para abrir o dropdown.</p>
  <br-dropdown>
    <br-button slot="trigger" shape="circle">
      <br-icon icon-name="fa6-solid:ellipsis-vertical"></br-icon>
    </br-button>
    <br-list slot="target" list-title="Título (opcional)">
      <br-item>
        <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
        Texto principal
        <span slot="end">META</span>
      </br-item>
      <span class="br-divider"></span>
      <br-item>
        <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
        Texto principal
        <span slot="end">META</span>
      </br-item>
      <span class="br-divider"></span>
      <br-item>
        <br-icon slot="start" icon-name="fa6-solid:heart-pulse"></br-icon>
        Texto principal
        <span slot="end">META</span>
      </br-item>
      <span class="br-divider"></span>
    </br-list>
  </br-dropdown>
</div>
```

---
