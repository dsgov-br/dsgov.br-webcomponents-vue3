## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/avatar?tab=designer).

## Propriedades

| Propriedade     | Atributo          | Descrição                                                                                                                                                                                             | Tipo                             | Valor padrão                  |
| --------------- | ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- | ----------------------------- |
| `alt`           | `alt`             | Texto alternativo (alt) associado à imagem do avatar. Essencial para acessibilidade e SEO. Deve descrever de forma clara e concisa o conteúdo da imagem, por exemplo: "Foto de perfil de João Silva". | `"Imagem do avatar"`             | `'Imagem do avatar'`          |
| `customId`      | `custom-id`       | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                | `string`                         | ```br-avatar-${avatarId++}``` |
| `density`       | `density`         | Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.                                                                                                   | `"large" \| "medium" \| "small"` | `'medium'`                    |
| `disabled`      | `disabled`        | Define se o avatar está desabilitado                                                                                                                                                                  | `boolean`                        | `false`                       |
| `iconHeight`    | `icon-height`     | Altura do ícone no avatar do tipo icônico                                                                                                                                                             | `string`                         | ---                           |
| `iconMarginTop` | `icon-margin-top` | Ajuste para a margem de cima do ícone no avatar do tipo icônico                                                                                                                                       | `string`                         | ---                           |
| `iconWidth`     | `icon-width`      | Largura do ícone no avatar do tipo icônico                                                                                                                                                            | `string`                         | ---                           |
| `isIconic`      | `is-iconic`       | Indica se o avatar deve ser exibido como um ícone em vez de uma imagem fotográfica. Se definido como verdadeiro, o avatar será exibido como um ícone.                                                 | `boolean`                        | `false`                       |
| `src`           | `src`             | URL da imagem a ser exibida no avatar do tipo 'fotográfico'. Deve ser uma URL válida que aponta para a imagem desejada.                                                                               | `string`                         | ---                           |
| `text`          | `text`            | Texto exibido no avatar do tipo 'letra'. Esse texto será mostrado em vez de uma imagem e pode ser usado para representar iniciais ou outras informações textuais.                                     | `string`                         | ---                           |






## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-avatar is-iconic density="small"></br-avatar>
  <br-avatar is-iconic density="medium"></br-avatar>
  <br-avatar is-iconic density="large"></br-avatar>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-avatar text="Lorem ipsum" density="small"></br-avatar>
  <br-avatar text="Lorem ipsum" density="medium"></br-avatar>
  <br-avatar text="Lorem ipsum" density="large"></br-avatar>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-avatar alt="Lorem ipsum" src="https://picsum.photos/400" density="small"></br-avatar>
  <br-avatar alt="Lorem ipsum" src="https://picsum.photos/400" density="medium"></br-avatar>
  <br-avatar alt="Lorem ipsum" src="https://picsum.photos/400" density="large"></br-avatar>
</div>
```
## Dependências

### Depende de

- [br-icon](../icon)

### Gráfico
```mermaid
graph TD;
  br-avatar --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  class br-avatar mainComponent
```

---
## Guia de Migração de Web Components da 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil | Descrição                                                 | Tipo                             | Padrão       |
| --------------- | ------------------- | --------------------------------------------------------- | -------------------------------- | ------------ |
| `density`       | `density`           | Define os níveis de densidade para o elemento avatar      | `'small' \| 'medium' \| 'large'` | `'medium'`   |
| `iconic`        | `iconic`            | Define que o avatar apresentado será do tipo ícone        | `boolean`                        | `false`      |
| `image`         | `image`             | Define que o avatar apresentado será do tipo fotográfico  | `string`                         | `undefined`  |
| `name`          | `name`              | Nome do usuário que será apresentado no avatar pela letra | `string`                         | `'John Doe'` |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-avatar></br-avatar>` no Vue e em StencilJS:

#### Vue

- Evento `click`: Evento emitido quando o avatar é clicado.

Exemplo:

```html
<br-avatar id="avatar-exemplo" name="John Doe"></br-avatar>
```

Adicionando o listener de evento:

```javascript
document.getElementById('avatar-exemplo').addEventListener('click', (e) => {
  console.log('Avatar clicado')
})
```

#### StencilJS

- Usando eventos em JSX: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `avatarClick`, a propriedade será chamada `onAvatarClick`.

```html
<!-- StencilJS -->
<br-avatar onClick="handleAvatarClick"></br-avatar>
```

- Ouvindo eventos de um elemento não JSX:

```html
<template>
  <br-avatar></br-avatar>
</template>

<script>
  const avatarElement = document.querySelector('br-avatar')
  avatarElement.addEventListener('click', (event) => {
    /* seu listener */
  })
</script>
```

| Evento                    | Descrição                                                                                                 |
| ------------------------- | --------------------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidRender`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidUpdate`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentShouldUpdate` | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillLoad`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillRender`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillUpdate`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brConnectedCallback`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDisconnectedCallback`  | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |

## Métodos

- `renderImageOrIcon()`: Método auxiliar que decide se deve renderizar uma imagem, um ícone ou a inicial do nome.

```javascript
<script>const avatarElement = document.querySelector('br-avatar'); avatarElement.renderImageOrIcon();</script>
```

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Avatar em uma aplicação Web.

```html
<template>
  <br-avatar
    id="avatar-01"
    name="Jane Doe"
    image="path/to/image.jpg"
    density="large"
    onClick="handleAvatarClick"
  ></br-avatar>
</template>
<script>
  export default {
    methods: {
      handleAvatarClick(event) {
        console.log('Avatar clicado: ', event)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).