import { newE2EPage } from '@stencil/core/testing'

describe('br-avatar', () => {
  it('deve renderizar', async () => {
    const page = await newE2EPage()
    await page.setContent(/*html*/ `<br-avatar></br-avatar>`)

    const element = await page.find('br-avatar')

    expect(element).toHaveClass('hydrated')
  })

  it('deve ter shadow root', async () => {
    const page = await newE2EPage()
    await page.setContent(/*html*/ `<br-avatar></br-avatar>`)

    const element = await page.find('br-avatar')

    expect(element.shadowRoot).not.toBeNull()
  })
})
