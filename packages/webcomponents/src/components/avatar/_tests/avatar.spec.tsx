import { newSpecPage } from '@stencil/core/testing'
import { Avatar } from '../avatar'

describe('br-avatar', () => {
  it('deve inicializar com o estado padrão padrão', async () => {
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar></br-avatar>`,
    })

    const avatarElement = page.root.shadowRoot.querySelector('.br-avatar')
    const defaultIcon = page.root.shadowRoot.querySelector('br-icon')

    expect(avatarElement).toBeTruthy()
    expect(avatarElement.classList.contains('medium')).toBe(true)
    expect(avatarElement.getAttribute('id')).toBe('br-avatar-0')
    expect(defaultIcon).toBeTruthy()
    expect(defaultIcon.getAttribute('icon-name')).toBe('fa-solid:user')
    expect(defaultIcon.getAttribute('height')).toBe('80px')
    expect(defaultIcon.getAttribute('width')).toBe('88px')
    expect(defaultIcon.getAttribute('aria-hidden')).toBe('true')
  })

  it('deve usar ID customizado quando fornecido via prop custom-id', async () => {
    const expectedCustomId = 'avatar-id'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar custom-id="${expectedCustomId}"></br-avatar>`,
    })

    const avatarElement = page.root.shadowRoot.querySelector('.br-avatar')

    expect(avatarElement.getAttribute('id')).toBe(expectedCustomId)
  })

  it('deve exibir primeira letra maiúscula do texto fornecido', async () => {
    const inputText = 'lorem ipsum'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar text="${inputText}"></br-avatar>`,
    })

    const avatarContent = page.root.shadowRoot.querySelector('.content')

    expect(avatarContent.textContent).toBe(inputText[0].toUpperCase())
    expect(avatarContent.getAttribute('title')).toBe(inputText)
  })

  it('deve exibir primeira letra maiúscula com texto que contém caracteres especiais', async () => {
    const specialText = 'Ângelo'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar text="${specialText}"></br-avatar>`,
    })

    const avatarContent = page.root.shadowRoot.querySelector('.content')

    expect(avatarContent.textContent).toBe('Â')
    expect(avatarContent.getAttribute('title')).toBe(specialText)
  })

  it('deve exibir imagem com src e alt quando fornecidos', async () => {
    const imageUrl = 'https://picsum.photos/400'
    const imageAltText = 'Exemplo de texto alternativo'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar src="${imageUrl}" alt="${imageAltText}"></br-avatar>`,
    })

    const avatarImage = page.root.shadowRoot.querySelector('img')

    expect(avatarImage?.getAttribute('src')).toBe(imageUrl)
    expect(avatarImage?.getAttribute('alt')).toBe(imageAltText)
  })

  it('deve substituir imagem por ícone quando ocorrer erro no carregamento', async () => {
    const originalConsoleError = console.error
    console.error = jest.fn()

    const invalidImageUrl = 'https://invalid-url.com/image.jpg'
    const imageAltText = 'Exemplo de texto alternativo'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar src="${invalidImageUrl}" alt="${imageAltText}"></br-avatar>`,
    })

    const avatarImage = page.root.shadowRoot.querySelector('img')
    avatarImage.dispatchEvent(new Event('error'))

    await page.waitForChanges()

    const avatarElement = page.root.shadowRoot.querySelector('.br-avatar')
    expect(avatarElement.classList.contains('error')).toBe(true)
    expect(console.error).toHaveBeenCalledWith(`Erro ao carregar imagem do avatar: ${invalidImageUrl}`)

    console.error = originalConsoleError
  })

  it('deve aplicar classes de densidade (small, medium, large) conforme prop density', async () => {
    const availableDensities = ['small', 'medium', 'large']

    for (const densityValue of availableDensities) {
      const page = await newSpecPage({
        components: [Avatar],
        html: /*html*/ `<br-avatar density="${densityValue}"></br-avatar>`,
      })

      await page.waitForChanges()

      const avatarElement = page.root.shadowRoot.querySelector('.br-avatar')

      expect(page.root.density).toBe(densityValue)
      expect(avatarElement.classList.contains(densityValue)).toBe(true)
    }
  })

  it('deve atualizar classes quando a prop density é alterada dinamicamente', async () => {
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar density="small"></br-avatar>`,
    })

    let avatarElement = page.root.shadowRoot.querySelector('.br-avatar')
    expect(avatarElement.classList.contains('small')).toBe(true)

    page.root.density = 'large'
    await page.waitForChanges()

    avatarElement = page.root.shadowRoot.querySelector('.br-avatar')
    expect(avatarElement.classList.contains('large')).toBe(true)
    expect(avatarElement.classList.contains('small')).toBe(false)
  })

  it('deve ajustar dimensões do ícone automaticamente conforme densidade', async () => {
    const densityIconDimensions = {
      small: { height: '32px', width: '40px', marginTop: '0.7em' },
      medium: { height: '80px', width: '88px', marginTop: '1em' },
      large: { height: '128px', width: '136px', marginTop: '0.62em' },
    }

    for (const [densityValue, expectedDimensions] of Object.entries(densityIconDimensions)) {
      const page = await newSpecPage({
        components: [Avatar],
        html: /*html*/ `<br-avatar is-iconic density="${densityValue}"></br-avatar>`,
      })

      await page.waitForChanges()

      const avatarIcon = page.root.shadowRoot.querySelector('br-icon')

      expect(avatarIcon?.getAttribute('height')).toBe(expectedDimensions.height)
      expect(avatarIcon?.getAttribute('width')).toBe(expectedDimensions.width)
      expect(avatarIcon?.style.marginTop).toBe(expectedDimensions.marginTop)
    }
  })

  it('deve permitir dimensões customizadas para ícone via props específicas', async () => {
    const customIconWidth = '100px'
    const customIconHeight = '100px'
    const customIconMarginTop = '10px'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar is-iconic icon-width="${customIconWidth}" icon-height="${customIconHeight}" icon-margin-top="${customIconMarginTop}"></br-avatar>`,
    })

    const avatarIcon = page.root.shadowRoot.querySelector('br-icon')

    expect(avatarIcon?.getAttribute('height')).toBe(customIconHeight)
    expect(avatarIcon?.getAttribute('width')).toBe(customIconWidth)
    expect(avatarIcon?.style.marginTop).toBe(customIconMarginTop)
  })

  it('deve atualizar corretamente quando a prop text é alterada dinamicamente', async () => {
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar text="Inicial"></br-avatar>`,
    })

    page.root.text = 'Atualizado'

    await page.waitForChanges()

    const avatarContent = page.root.shadowRoot.querySelector('.content')
    expect(avatarContent.textContent).toBe('A')
    expect(avatarContent.getAttribute('title')).toBe('Atualizado')
  })

  it('deve adicionar classe disabled quando disabled=true', async () => {
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar disabled></br-avatar>`,
    })

    const avatarElement = page.root.shadowRoot.querySelector('.br-avatar')
    expect(avatarElement.classList.contains('disabled')).toBe(true)
  })

  it('deve manter estado inicial quando props inválidas são fornecidas', async () => {
    const originalConsoleWarn = console.warn
    console.warn = jest.fn()

    const invalidDensity = 'invalid-density'
    const page = await newSpecPage({
      components: [Avatar],
      html: /*html*/ `<br-avatar density="${invalidDensity}"></br-avatar>`,
    })

    const avatarElement = page.root.shadowRoot.querySelector('.br-avatar')
    const defaultIcon = page.root.shadowRoot.querySelector('br-icon')

    expect(avatarElement).toBeTruthy()
    expect(avatarElement.classList.contains('medium')).toBe(true)
    expect(defaultIcon).toBeTruthy()
    expect(defaultIcon.getAttribute('icon-name')).toBe('fa-solid:user')
    expect(defaultIcon.getAttribute('height')).toBe('80px')
    expect(defaultIcon.getAttribute('width')).toBe('88px')
    expect(defaultIcon.getAttribute('aria-hidden')).toBe('true')
    expect(console.warn).toHaveBeenCalledWith(`Valor inválido para density: ${invalidDensity}. Usando 'medium'.`)

    console.warn = originalConsoleWarn
  })
})
