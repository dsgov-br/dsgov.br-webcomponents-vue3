import { AttachInternals, Component, Element, Event, EventEmitter, h, Host, Prop, State } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 *
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/input?tab=designer).
 *
 * @slot action - Botão à direita do input.
 * @slot icon - Ícone à esquerda do input.
 * @slot help-text - Personalização do texto de ajuda.
 * @slot feedback - Mensagem de feedback como resposta específica a uma interação do usuário com o input. Pode ser feedback de erro, aviso, sucesso ou informação.
 */
@Component({
  tag: 'br-input',
  styleUrl: 'input.scss',
  shadow: true,
  formAssociated: true,
})
export class Input {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrInputElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Especifica o tipo de entrada do campo. Pode ser um dos seguintes:
   * - 'color': Seletor de cor
   * - 'email': Campo para e-mail
   * - 'hidden': Campo oculto
   * - 'password': Campo para senha
   * - 'range': Controle deslizante
   * - 'search': Campo de pesquisa
   * - 'number': Campo para números
   * - 'tel': Campo para números de telefone
   * - 'text': Campo de texto padrão
   * - 'url': Campo para URLs
   */
  @Prop({ reflect: true }) readonly type:
    | 'color'
    | 'email'
    | 'hidden'
    | 'password'
    | 'range'
    | 'search'
    | 'number'
    | 'tel'
    | 'text'
    | 'file'
    | 'url' = 'text'

  /**
   * Controla o comportamento de preenchimento automático do navegador para o input.
   */
  @Prop({ reflect: true }) readonly autocomplete: 'on' | 'off'

  /**
   * Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.
   */
  @Prop({ reflect: true }) readonly density: 'large' | 'medium' | 'small' = 'medium'

  /**
   * Desativa o input, tornando-o não interativo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Se verdadeiro, o rótulo e o input estarão na mesma linha (layout inline).
   */
  @Prop({ reflect: true }) readonly isInline: boolean = false

  /**
   * Se verdadeiro, o input terá destaque visual.
   */
  @Prop({ reflect: true }) readonly isHighlight: boolean = false

  /**
   * Define o estado do input
   * Os possíveis valores são:
   * - 'info': Mensagem informativa.
   * - 'warning': Mensagem de aviso.
   * - 'danger': Mensagem de erro ou alerta.
   * - 'success': Mensagem de sucesso.
   * O valor padrão é 'info'.
   */
  @Prop({ reflect: true }) readonly state: 'info' | 'warning' | 'danger' | 'success'

  /**
   * Texto exibido como rótulo do input.
   */
  @Prop({ reflect: true }) readonly label: string

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-input-${inputId++}`

  /**
   * Nome do input, utilizado para identificação em formulários.
   */
  @Prop({ reflect: true }) readonly name: string

  /**
   * Texto exibido dentro do input quando está vazio, fornecendo uma dica ou sugestão ao usuário.
   */
  @Prop({ reflect: true }) readonly placeholder: string

  /**
   * Se verdadeiro, o valor do input é exibido, mas não pode ser editado pelo usuário.
   */
  @Prop({ reflect: true }) readonly readonly: boolean = false

  /**
   * Se verdadeiro, o input é obrigatório e deve ser preenchido antes que o formulário possa ser enviado.
   */
  @Prop({ reflect: true }) readonly required: boolean = false

  /**
   * Valor exibido no input.
   * Pode ser alterado pelo usuário se a propriedade `readonly` não estiver ativa.
   */
  @Prop({ reflect: true, mutable: true }) value: string

  /**
   * Texto adicional que fornece ajuda ou informações sobre o input.
   */
  @Prop({ reflect: true }) readonly helpText: string

  /**
   * Controla a correção automática do texto.
   */
  @Prop({ reflect: true }) readonly autocorrect: 'off' | 'on' = 'off'

  /**
   * Define o valor mínimo para campos de entrada numéricos.
   */
  @Prop({ reflect: true }) readonly min: number

  /**
   * Define o valor máximo para campos de entrada numéricos.
   */
  @Prop({ reflect: true }) readonly max: number

  /**
   * Define o comprimento mínimo do valor do campo de entrada.
   */
  @Prop({ reflect: true }) readonly minlength: number

  /**
   * Define o comprimento máximo do valor do campo de entrada.
   */
  @Prop({ reflect: true }) readonly maxlength: number

  /**
   * Se verdadeiro, permite a seleção de múltiplos arquivos.
   */
  @Prop({ reflect: true }) readonly multiple: boolean = false

  /**
   * Define o padrão de entrada para validação.
   */
  @Prop({ reflect: true }) readonly pattern: string

  /**
   * Define o valor do passo para campos de entrada numéricos.
   */
  @Prop({ reflect: true }) readonly step: number

  @State() isPasswordVisible: boolean = false

  @State() internalState: 'info' | 'warning' | 'danger' | 'success' = 'info'

  @State() hasIconSlot: boolean = false

  @State() hasActionSlot: boolean = false

  @State() hasFeedbackSlot: boolean = false

  componentWillLoad() {
    this.hasIconSlot = this.hasSlotContent('icon')
    this.hasActionSlot = this.hasSlotContent('action')
    this.hasFeedbackSlot = this.hasSlotContent('feedback')
    this.internalState = this.state

    if (this.hasFeedbackSlot) {
      this.initializeStateWithFeedback()
    }
  }

  componentDidLoad() {
    if (this.hasFeedbackSlot) {
      this.setupSlotFeedbackListener()
    }
  }

  private initializeStateWithFeedback() {
    const initialMessageElement = this.el.querySelector('br-message')

    if (initialMessageElement) {
      const messageState = initialMessageElement.getAttribute('state')
      this.internalState = (messageState as 'info' | 'warning' | 'danger' | 'success') || this.state
    } else {
      this.internalState = this.state
    }
  }

  private setupSlotFeedbackListener() {
    const slot = this.el.shadowRoot?.querySelector('slot[name="feedback"]')
    if (slot) {
      slot.addEventListener('slotchange', () => {
        this.syncStateWithSlotFeedback()
      })
    }
  }

  private syncStateWithSlotFeedback() {
    const messageElement = this.el.querySelector('br-message')

    if (messageElement) {
      this.internalState = messageElement.getAttribute('state') as 'info' | 'warning' | 'danger' | 'success'
    } else {
      this.internalState = this.state
    }
  }

  private getInputCssClassMap(): CssClassMap {
    return {
      'br-input': true,
      'input-button': this.hasActionSlot,
      small: this.density === 'small',
      large: this.density === 'large',
      medium: this.density === 'medium',
      'input-inline': this.isInline,
      'input-highlight': this.isHighlight,
      success: this.internalState === 'success',
      danger: this.internalState === 'danger',
      info: this.internalState === 'info',
      warning: this.internalState === 'warning',
    }
  }

  /**
   * Valor atualizado do input
   */
  @Event() valueChange: EventEmitter<string>

  private readonly handleInput = (event: Event) => {
    this.value = (event.target as HTMLInputElement).value
    this.valueChange.emit(this.value)
  }

  private readonly renderLabel = () => {
    if (this.label?.length > 0) {
      if (this.isInline) {
        return (
          <div class="input-label">
            <label class="text-nowrap" htmlFor={this.customId}>
              {this.label}
            </label>
          </div>
        )
      } else {
        return <label htmlFor={this.customId}>{this.label}</label>
      }
    }
  }

  private hasSlotContent(slotName: string): boolean {
    return this.el.querySelector(`[slot="${slotName}"]`) !== null
  }

  private readonly renderInput = () => {
    return (
      <input
        class={this.isHighlight ? 'input-highlight' : null}
        id={this.customId}
        autocomplete={this.autocomplete}
        disabled={this.disabled}
        autoCorrect={this.autocorrect}
        min={this.min}
        max={this.max}
        minLength={this.minlength}
        maxLength={this.maxlength}
        multiple={this.multiple}
        name={this.name}
        pattern={this.pattern}
        step={this.step}
        onInput={this.handleInput}
        placeholder={this.placeholder}
        readonly={this.readonly}
        required={this.required}
        title={this.el.title}
        type={this.type}
        value={this.value}
      />
    )
  }

  private readonly renderHelpText = () => {
    return this.helpText?.length > 0 ? <p>{this.helpText}</p> : <slot name="help-text"></slot>
  }

  private readonly renderIconPassword = () => {
    const iconName = this.isPasswordVisible ? 'fa-solid:eye-slash' : 'fa-solid:eye'
    return <br-icon icon-name={iconName}></br-icon>
  }

  private readonly togglePasswordVisibility = () => {
    const input = this.el.shadowRoot.querySelector('input')
    this.isPasswordVisible = !this.isPasswordVisible
    input.type = this.isPasswordVisible ? 'text' : 'password'
  }

  private renderIconSlot() {
    if (this.hasSlotContent('icon')) {
      return (
        <div class="input-icon">
          <slot name="icon"></slot>
        </div>
      )
    }
  }

  private renderButton() {
    if (this.type === 'password') {
      return (
        <br-button
          class="button-transform"
          shape="circle"
          density="small"
          aria-label="botão de ação do ícone"
          onClick={this.togglePasswordVisibility}
        >
          {this.renderIconPassword()}
        </br-button>
      )
    } else if (this.hasSlotContent('action')) {
      return (
        <br-button class="button-transform" shape="circle" density="small" aria-label="botão de ação do ícone">
          <slot name="action"></slot>
        </br-button>
      )
    }
    return null
  }

  private readonly renderInlineInputContent = () => {
    if (this.hasIconSlot) {
      return <div class="input-content">{this.renderInputWithIcon()}</div>
    } else {
      return <div class="input-content">{this.renderInputWithoutIcon()}</div>
    }
  }

  private readonly renderInputContent = () => {
    return this.hasIconSlot ? this.renderInputWithIcon() : this.renderInputWithoutIcon()
  }

  private readonly renderInputWithIcon = () => {
    return (
      <div class="input-group">
        {this.renderIconSlot()}
        {this.renderInput()}
        {this.renderButton()}
        {this.renderHelpText()}
      </div>
    )
  }

  private readonly renderInputWithoutIcon = () => {
    return [this.renderInput(), this.renderButton(), this.renderHelpText()]
  }

  private renderFeedback() {
    if (this.hasFeedbackSlot) {
      return <slot name="feedback"></slot>
    }
  }

  render() {
    return (
      <Host>
        <div class={this.getInputCssClassMap()}>
          {this.renderLabel()}

          {this.isInline ? this.renderInlineInputContent() : this.renderInputContent()}

          {this.renderFeedback()}
        </div>
      </Host>
    )
  }
}

let inputId = 0
