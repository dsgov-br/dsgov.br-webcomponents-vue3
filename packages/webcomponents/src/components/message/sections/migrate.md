## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil | Descrição                                                                            | Tipo      | Padrão   |
| --------------- | ------------------- | ------------------------------------------------------------------------------------ | --------- | -------- |
| `text`          | `message`           | Texto da mensagem. **Obrigatório**                                                   | `string`  | `''`     |
| `title`         | `messageTitle`      | Título da mensagem. Opcional                                                         | `string`  | `''`     |
| `inline`        | `isInline`          | Exibir o título na mesma linha que a mensagem. Opcional                              | `boolean` | `false`  |
| `closable`      | `isClosable`        | Habilita o botão de fechar a mensagem. Opcional                                      | `boolean` | `false`  |
| `showIcon`      | `showIcon`          | Exibir ícone na mensagem. Opcional                                                   | `boolean` | `false`  |
| `feedback`      | `isFeedback`        | Mensagem contextual de feedback. Opcional                                            | `boolean` | `false`  |
| `type`          | `state`             | Estado da mensagem. **Obrigatório**. Valores: 'info', 'warning', 'danger', 'success' | `string`  | `'info'` |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-message></br-message>` no Vue e em StencilJS:

#### Vue

- Evento `close`: Evento emitido quando a mensagem é fechada.

Exemplo:

```html
<br-message id="message-exemplo" text="Mensagem de exemplo"></br-message>
```

Adicionando o listener de evento:

```javascript
document.getElementById('message-exemplo').addEventListener('close', (e) => {
  console.log('Mensagem fechada')
})
```

#### StencilJS

- Usando eventos em JSX: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidClose`, a propriedade será chamada `onBrDidClose`.

```html
<!-- StencilJS -->
<br-message onBrDidClose="handleMessageClose"></br-message>
```

- Ouvindo eventos de um elemento não JSX:

```html
<template>
  <br-message></br-message>
</template>

<script>
  const messageElement = document.querySelector('br-message')
  messageElement.addEventListener('onBrDidClose', (event) => {
    /* seu listener */
  })
</script>
```

| Evento                    | Descrição                                                                                                 |
| ------------------------- | --------------------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidRender`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidUpdate`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentShouldUpdate` | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillLoad`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillRender`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillUpdate`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brConnectedCallback`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDisconnectedCallback`  | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDidClose`              | Evento emitido quando o usuário fecha a mensagem, se closable for true.                                   |

## Métodos

Não se aplica.

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão.

Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Message em uma aplicação Web.

```html
<template>
  <br-message
    id="message-01"
    state="success"
    message-title="Título da Mensagem"
    message="Texto da mensagem de exemplo"
    is-inline
    is-closable
    show-icon
    onBrDidClose="handleMessageClose"
  ></br-message>
</template>
<script>
  export default {
    methods: {
      handleMessageClose(event) {
        console.log('Mensagem fechada: ', event)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).
