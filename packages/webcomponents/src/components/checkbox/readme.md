## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/checkbox?tab=designer).

## Propriedades

| Propriedade            | Atributo           | Descrição                                                                                                                                                                                                                                                                                                                                                                               | Tipo                   | Valor padrão                      |
| ---------------------- | ------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | --------------------------------- |
| `checked`              | `checked`          | Define o estado de seleção do checkbox. Se definido como verdadeiro, o checkbox estará marcado. Caso contrário, estará desmarcado.                                                                                                                                                                                                                                                      | `boolean`              | `false`                           |
| `customId`             | `custom-id`        | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                                                                                                                                                                                  | `string`               | ```br-checkbox-${checkboxId++}``` |
| `disabled`             | `disabled`         | Desativa o checkbox, tornando-o não interativo.                                                                                                                                                                                                                                                                                                                                         | `boolean`              | `false`                           |
| `hasHiddenLabel`       | `has-hidden-label` | Define se o label associado ao checkbox deve ser oculto. Se definido como verdadeiro, o texto do label será oculto, mas o checkbox ainda estará visível e funcional.                                                                                                                                                                                                                    | `boolean`              | `false`                           |
| `indeterminate`        | `indeterminate`    | Define o estado intermediário do checkbox. Se definido como verdadeiro, o checkbox exibirá um estado intermediário, que é um estado visual que indica que a opção está parcialmente selecionada. Este estado é útil quando o checkbox faz parte de um grupo com seleção parcial. Este estado será resetado automaticamente ao clicar no checkbox, alterando para marcado ou desmarcado. | `boolean`              | `false`                           |
| `label`                | `label`            | Texto descritivo exibido à direita do checkbox. Caso um slot seja utilizado para fornecer um texto alternativo, o valor desta propriedade será ignorado.                                                                                                                                                                                                                                | `string`               | ---                               |
| `name` *(obrigatório)* | `name`             | Define o nome do checkbox, que é utilizado para agrupar checkboxes em formulários e identificar o campo. O valor é obrigatório e deve ser fornecido para garantir o correto funcionamento em formulários.                                                                                                                                                                               | `string`               | ---                               |
| `state`                | `state`            | Indica a validade do checkbox. Valores possíveis: 'valid': O checkbox é considerado válido. 'invalid': O checkbox é considerado inválido. Se não for especificado, o valor padrão é `null`, indicando que a validade não foi definida.                                                                                                                                                  | `"invalid" \| "valid"` | ---                               |
| `value`                | `value`            | Define o valor associado ao checkbox quando ele faz parte de um formulário nativo (`<form>`). Esse valor é enviado com o formulário quando o checkbox está selecionado. **Nota:** Esta propriedade não deve ser utilizada para determinar se o checkbox está selecionado; para verificar o estado de seleção, use a propriedade `checked`.                                              | `string`               | ---                               |

## Eventos

| Evento                | Descrição                                                     | Tipo                   |
| --------------------- | ------------------------------------------------------------- | ---------------------- |
| `checkedChange`       | Disparado depois que o valor do `checked` foi alterado        | `CustomEvent<boolean>` |
| `indeterminateChange` | Disparado depois que o valor do `indeterminate` foi alterado. | `CustomEvent<boolean>` |

## Métodos

### `setIndeterminate(value: boolean) => Promise<void>`

Define o estado indeterminado do checkbox.

#### Parâmetros

| Nome    | Tipo      | Descrição                               |
| ------- | --------- | --------------------------------------- |
| `value` | `boolean` | Novo valor para o estado indeterminado. |

#### Retorna

Tipo: `Promise<void>`

### `toggleChecked() => Promise<void>`

Inverte o valor da prop `checked`

#### Retorna

Tipo: `Promise<void>`

## Slots

| Slot        | Descrição                                                                |
| ----------- | ------------------------------------------------------------------------ |
| `"default"` | O texto descritivo pode ser passado por propriedade (label) ou por slot. |

## Exemplos

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-checkbox id="checkbox-1" name="checkbox-1" has-hidden-label label="Hidden Label"></br-checkbox>
</div>
```

```html
<div class="d-flex justify-content-evenly mt-5">
  <br-checkbox id="ckb-1" name="h-checkbox-1" label="Horizontal 1"></br-checkbox>
  <br-checkbox id="ckb-2" name="h-checkbox-2" label="Horizontal 2"></br-checkbox>
  <br-checkbox id="ckb-3" name="h-checkbox-3" label="Horizontal 3"></br-checkbox>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-checkbox id="indeterminate01" name="indeterminate01" indeterminate label="Indeterminate"></br-checkbox>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-checkbox id="slotted" name="slotted">
    <span>Clique para aceitar os <a url="#">Termos de Aceite</a></span>
  </br-checkbox>
</div>
```

```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-checkbox class="mb-1" id="check-state-checked" name="check-state-checked" checked label="Marcado"></br-checkbox>
  <br-checkbox class="mb-1" id="check-state-unchecked" name="check-state-unchecked" label="Não marcado"></br-checkbox>
  <br-checkbox
    class="mb-1"
    id="check-state-disabled"
    name="check-state-disabled"
    disabled
    label="Desativado"
  ></br-checkbox>
  <br-checkbox
    class="mb-1"
    id="check-state-invalid"
    name="check-state-invalid"
    state="invalid"
    label="Inválido"
  ></br-checkbox>
  <br-checkbox id="check-state-valid" name="check-state-valid" state="valid" label="Válido"></br-checkbox>
</div>
```

```html
<div class="d-flex flex-column flex-wrap justify-content-evenly mt-5 p-4">
  <br-checkbox
    class="mb-1"
    id="checkbox-1"
    autofocus
    name="checkbox-1"
    label="Vertical 1"
    aria-label="Vertical 1"
  ></br-checkbox>
  <br-checkbox class="mb-1" id="checkbox-2" name="checkbox-2" label="Vertical 2" aria-label="Vertical 2"></br-checkbox>
  <br-checkbox id="checkbox-3" name="checkbox-3" label="Vertical 3" aria-label="Vertical 3"></br-checkbox>
</div>
```

## Dependências

### Usado por

- [br-checkgroup](../checkgroup)
- [br-select](../select)

### Gráfico

```mermaid
graph TD;
  br-checkgroup --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  br-select --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  class br-checkbox mainComponent
```

---

## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue     | Propriedade Stencil    | Descrição                                                                | Tipo      | Padrão      |
| ------------------- | ---------------------- | ------------------------------------------------------------------------ | --------- | ----------- |
| `dataChild`         | `checkgroupChild`      | Declara o comportamento checkgroup e coloca o checkbox como sendo filho. | `string`  | `undefined` |
| `dataParent`        | `checkgroupParent`     | Declara o comportamento checkgroup e coloca o checkbox como sendo pai.   | `string`  | `undefined` |
| `inline`            | --                     | Formata o componente para versão horizontal.                             | `boolean` | `false`     |
| `invalid`           | `format`               | Estado indicativo de que uma opção não é válida.                         | `boolean` | `false`     |
| `name` *(opcional)* | `name` *(obrigatório)* | Agora obrigatório, define o name que será atribuído ao checkbox.         | `string`  | `undefined` |
| `valid`             | `format`               | Estado indicativo de que a opção é válida.                               | `boolean` | `false`     |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-checkbox></br-checkbox>` no Vue e em Stencil:

#### Vue

- Evento *`onChange`*: Evento emitido quando o estado do checkbox é modificado.

- Evento *`update:checked`*: Evento emitido para fazer o two-way data binding com a prop checked.

Exemplo:

```html
<br-checkbox id="checkbox-exemplo" label="teste"> </br-checkbox>
```

Adicionando o listener de evento:

```javascript
document.getElementById('checkbox-exemplo').addEventListener('update:checked', (e) => {
  const checkedCheckbox = e.detail[0]
  console.log('O checkbox ' + checkedCheckbox.label + ' foi checado.')
})
```

#### StencilJS

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `checkedChange`, a propriedade será chamada `onCheckedChange`.

```html
<!-- StencilJS -->
<br-checkbox onEventoPersonalizado="handleEvent"></br-checkbox>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-checkbox></br-checkbox>
</template>

<script>
  const checkboxElement = document.querySelector('br-checkbox')
  checkboxElement.addEventListener('checkedChange', (event) => {
    /* your listener */
  })
</script>
```

| Evento                      | Descrição                                                                                                 |
| --------------------------- | --------------------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`        | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidRender`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidUpdate`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentShouldUpdate`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillLoad`       | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillRender`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillUpdate`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brConnectedCallback`       | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDisconnectedCallback`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `checkedChange`             | Evento emitido quando o checkbox é marcado ou desmarcado.                                                 |
| `formAssociatedCallback`    | Componente foi inserido em um `<form>`                                                                    |
| `formDisassociatedCallback` | Componente foi removido de um `<form>`                                                                    |

### Métodos

- `toggleChecked()`: é método público acessível que alterna o estado de uma variável boolean entre true e false. Veja como exemplo, o método é chamado após 2 segundos usando setTimeout():

```javascript
<script>const checkbox = document.querySelector('my-checkbox'); checkbox.toggleChecked(); }, 2000);</script>
```

### Slots

Foi criado o slot `label` no StencilJs, que permite a personalização do rótulo associado ao checkbox.

```html
<!-- StencilJS -->
<br-checkbox>
  <br-checkbox name="checkbox-1">
    <span slot="label">Clique para aceitar os <a url="#">Termos de Aceite</a></span> //Conteúdo do slot
  </br-checkbox>
</div>
```

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Checkbox em uma aplicação Web.

```html
<template>
  <br-checkbox
    id="cbx-01"
    name="cbx-01"
    label="Opção 1"
    aria-label="opção 1"
    onCheckedChange="handleEvent"
  ></br-checkbox>
</template>
<script>
  export default {
    methods: {
      handleEvent(event) {
        console.log(event.detail)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).
