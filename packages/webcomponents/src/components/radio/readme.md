## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/radio?tab=designer).

## Propriedades

| Propriedade            | Atributo           | Descrição                                                                                                                                                                                                                                                                                                                         | Tipo                   | Valor padrão                |
| ---------------------- | ------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | --------------------------- |
| `checked`              | `checked`          | Define o estado de seleção do radio. Se definido como verdadeiro, o radio estará marcado. Caso contrário, estará desmarcado.                                                                                                                                                                                                      | `boolean`              | `false`                     |
| `customId`             | `custom-id`        | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                                                                                                                            | `string`               | ```br-radio-${radioId++}``` |
| `disabled`             | `disabled`         | Desativa o radio, tornando-o não interativo.                                                                                                                                                                                                                                                                                      | `boolean`              | `false`                     |
| `hasHiddenLabel`       | `has-hidden-label` | Define se o label associado ao radio deve ser oculto. Se definido como verdadeiro, o texto do label será oculto, mas o radio ainda estará visível e funcional.                                                                                                                                                                    | `boolean`              | `false`                     |
| `label`                | `label`            | Texto descritivo exibido à direita do radio. Caso um slot seja utilizado para fornecer um texto alternativo, o valor desta propriedade será ignorado.                                                                                                                                                                             | `string`               | ---                         |
| `name` *(obrigatório)* | `name`             | Define o nome do radio, que é utilizado para agrupar radios em formulários e identificar o campo. O valor é obrigatório e deve ser fornecido para garantir o correto funcionamento em formulários.                                                                                                                                | `string`               | ---                         |
| `state`                | `state`            | Indica a validade do radio. Valores possíveis: - 'valid': O radio é considerado válido. - 'invalid': O radio é considerado inválido. Se não for especificado, o valor padrão é `null`, indicando que a validade não foi definida.                                                                                                 | `"invalid" \| "valid"` | ---                         |
| `value`                | `value`            | Define o valor associado ao radio quando ele faz parte de um formulário nativo (`<form>`). Esse valor é enviado com o formulário quando o radio está selecionado. **Nota:** Esta propriedade não deve ser utilizada para determinar se o radio está selecionado; para verificar o estado de seleção, use a propriedade `checked`. | `string`               | ---                         |

## Eventos

| Evento          | Descrição                                              | Tipo                   |
| --------------- | ------------------------------------------------------ | ---------------------- |
| `checkedChange` | Disparado depois que o valor do `checked` foi alterado | `CustomEvent<boolean>` |

## Métodos

### `toggleChecked() => Promise<void>`

Inverte o valor da prop `checked`

#### Retorna

Tipo: `Promise<void>`




## Slots

| Slot        | Descrição                                                                |
| ----------- | ------------------------------------------------------------------------ |
| `"default"` | O texto descritivo pode ser passado por propriedade (label) ou por slot. |

## Shadow Parts

| Part            | Description |
| --------------- | ----------- |
| `"radio-input"` |             |
| `"radio-label"` |             |


## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-radio class="mb-1" has-hidden-label></br-radio>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <p class="label mb-0">Rótulo</p>
  <p class="help-text">Informações adicionais</p>
  <br-radio class="mb-1" name="radio-group" value="option1">Option 1</br-radio>
  <br-radio class="mb-1" name="radio-group" value="option2">Option 2</br-radio>
  <br-radio class="mb-1" name="radio-group" value="option3">Option 3</br-radio>
  <br-radio class="mb-1" name="radio-group" value="option4">Option 4</br-radio>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-radio class="mb-1" id="radio-state-checked" name="radio-state-checked">Label Slotted</br-radio>
  <br-radio class="mb-1" id="radio-state-checked" name="radio-state-checked" label="Prop Label"></br-radio>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-radio class="mb-1" id="radio-state-checked" name="radio-state-checked" checked label="Checked"></br-radio>
  <br-radio class="mb-1" id="radio-state-checked" name="radio-state-checked" disabled label="Disabled"></br-radio>
  <br-radio class="mb-1" id="radio-state-invalid" name="radio-state-invalid" state="invalid" label="Invalid"></br-radio>
  <br-radio id="radio-state-valid" name="radio-state-valid" state="valid" label="Valid"></br-radio>
</div>
```
## Dependências

### Usado por

 - [br-select](../select)

### Gráfico
```mermaid
graph TD;
  br-select --Depende---> br-radio
  click br-radio "src/components/radio" "Link para a documentação do componente radio"
  class br-radio depComponent
  class br-radio mainComponent
```

---