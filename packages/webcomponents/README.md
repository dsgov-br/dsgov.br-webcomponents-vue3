# Web Components GovBR-DS

A biblioteca de Web Components do [GovBR-DS](https://gov.br/ds) está sendo desenvolvida utilizando o [Stencil](https://stenciljs.com/), um compilador que cria Web Components (Custom Elements). O Stencil incorpora os melhores conceitos dos frameworks mais populares em uma ferramenta simples de compilação. Para mais informações sobre o Stencil, visite o [site oficial](https://stenciljs.com/).

## Demonstração 🚀

Veja nossos componentes em funcionamento:

- [Versão 1.x](https://gov.br/ds/webcomponents/)
- [Versão 2.x (em desenvolvimento)](https://govbr-ds.gitlab.io/bibliotecas/wbc/govbr-ds-wbc/)

## Tecnologias 💻

Esse projeto é desenvolvido usando:

1. [GovBR-DS](https://gov.br/ds/ 'GovBR-DS')
1. [Stencil](https://stenciljs.com/ 'Stencil')
1. [Font Awesome](https://fontawesome.com/ 'Font Awesome')
1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')
1. [NX Monorepo](https://nx.dev/ 'NX Monorepo')

## O que são Web Components?

São um conjunto de tecnologias que permitem aos desenvolvedores criar novos elementos HTML personalizados, reutilizáveis ​​e encapsulados para uso em páginas e aplicativos da web. Eles se baseiam em padrões da web existentes e são suportados por todos os navegadores modernos.

O diagrama mostra os três componentes principais dos Web Components:

- Elementos HTML personalizados: tags HTML definidas usando JavaScript e podem ser usados ​​como qualquer outro elemento HTML.
- Shadow DOM: é um espaço de nome encapsulado para cada elemento HTML personalizado. Isso significa que os estilos e o JavaScript do elemento não interferem no resto da página.
- Templates: são fragmentos de HTML que podem ser reutilizados em vários elementos.
- Slots: são áreas em um elemento HTML personalizado onde templates podem ser inseridos.

### Benefícios dos Web Components

1. Reusabilidade
1. Modularidade
1. Encapsulamento
1. Compatibilidade
1. Manutenibilidade

### Ciclo de Vida

Para entender melhor o ciclo de vida dos componentes, acesse a página [https://stenciljs.com/docs/component-lifecycle](https://stenciljs.com/docs/component-lifecycle).

## Integração com frameworks

A integração entre Web Components e frameworks pode não ser fácil em determinadas situações. Por isso nós criamos as bibliotecas de integração (wrappers). Basicamente eles encapsulam os Web Components em uma biblioteca de componentes na tecnologia nativa de algum framework. Isso facilita a integração com funcionalidades nativas dos frameworks, como por exemplo o binding.

Para mais detalhes sobre como o Stencil faz a integração consulte [a documentação do Stencil sobre integrações](https://stenciljs.com/docs/overview).

Note que determinadas situações pode não ser possível fazer essa integração. Isso depende muito da evolução da especificação de Web Components e do suporte dos frameworks.

## Font Awesome e Fonte Rawline

Nossos componentes usam a [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline') juntamente com a [Font Awesome](https://fontawesome.com/ 'Font Awesome') padrão do DS.

Consulte a documentação no site do [GovBR-DS](https://www.gov.br/ds/como-comecar/instalacao 'GovBR-DS') para mais detalhes sobre como importá-los de seus respectivos CDNs.

## Instalação ⚙️

Instale o pacote abaixo:

```bash
npm install --save-dev @govbr-ds/webcomponents
```

## Scripts Disponíveis 📜

No arquivo `package.json` e `project.json`, você encontrará uma variedade de scripts úteis. Aqui está uma lista com a descrição de cada um:

```bash
nx start webcomponents
```

Executa o site no localhost para desenvolvimento.

```bash
nx build webcomponents
```

Compila o projeto para produção.

```bash
nx test:unit webcomponents
```

Executa os testes unitários.

```bash
nx lint:tsx webcomponents
```

Executa lint nos códigos TypeScript.

```bash
nx lint:md webcomponents
```

Executa lint nos arquivos markdown.

```bash
nx lint:styles webcomponents
```

Executa lint nos arquivos de estilos.

## Testes

Nossa estratégia de testes é dividida em duas abordagens principais: testes unitários e testes end-to-end (E2E).

### Testes Unitários (\*.spec.tsx)

Os testes unitários são ideais para validar a lógica interna dos componentes de forma isolada e rápida. Utilizamos o método `newSpecPage()` para testar:

- Estado inicial: comportamento do componente sem props
- Props: valores válidos, inválidos e mudanças em tempo de execução
- Classes CSS: presença e ausência de classes condicionais
- Slots: renderização correta do conteúdo
- Lógica interna: métodos, cálculos e transformações
- Validações básicas: required, minLength, maxValue etc.

```typescript
it('deve inicializar com o estado padrão padrão', async () => {
  const page = await newSpecPage({
    components: [BrComponent],
    html: `<br-component></br-component>`
  });
  expect(page.root).toEqualHtml(`<br-component>...</br-component>`);
});
```

### Testes E2E (\*.e2e.ts)

Os testes E2E são executados em um navegador real usando o método `newE2EPage()`. São mais lentos mas essenciais para testar:

- Eventos do componente: click, blur, change etc.
- Interações do usuário: drag-drop, digitação, atalhos
- Integrações DOM: forms, validação nativa, aria-*
- Estilos visuais: dimensões, cores, transições
- Shadow DOM: slots, parts, estilos encapsulados

```typescript
it('should emit click event', async () => {
  const page = await newE2EPage();
  await page.setContent('<br-component></br-component>');
  const brComponent = await page.find('br-component');
  const clickEvent = await brComponent.spyOnEvent('click');
  await brComponent.click();
  expect(clickEvent).toHaveReceivedEvent();
});
```

Para mais detalhes sobre testes no Stencil, consulte a [documentação oficial](https://stenciljs.com/docs/testing-overview).

## Configuração 🛠️

Os passos para usar os Web Components GovBR-DS em uma aplicação podem variar dependendo da tecnologia usada. Abaixo estão os passos gerais para grande parte dos casos:

### Adicione os tokens do `@govbr-ds/core`

```ts
import 'node_modules/@govbr-ds/core/dist/core-tokens.min.css'
```

### Adicione o `loader` do JS

Depois de instalada, importe a biblioteca de dentro da pasta *node_modules*:

```javascript
@import "node_modules/@govbr-ds/webcomponents/dist/loader"
```

Também é possível importar o JS da biblioteca usando um CDN (não mantido por nossa equipe):

```html
<script src="https://cdn.jsdelivr.net/npm/@govbr-ds/webcomponents@<VERSÃO>/dist/loader/index.js"></>
```

**Importante**: Dependendo do seu projeto pode ser necessário importar diretamente o arquivo `/dist/webcomponents/webcomponents.esm.js`. Observe as necessidades do seu projeto e importe os arquivos de acordo.

### Use as tags dos custom elements

```html
<br-button>Click aqui</br-button>
```

### Eventos

Você pode escutar eventos padrão, como clique e mouseover, da mesma forma que faria com elementos HTML normais. Muitos componentes também emitem eventos personalizados, que recomendamos fortemente utilizar. Esses eventos funcionam como os eventos padrão, mas possuem nomes específicos. Consulte a documentação de cada componente para obter detalhes sobre os nomes e dados dos eventos.

```html
<br-button>Label</br-button>

<script>
  const button = document.querySelector('br-button')
  button.addEventListener('click', (event) => {
    console.log('Botão foi clicado')
  })
</script>
```

## VSCODE

Ao criar nossos Web Components fazemos uso de custom elements. O VSCODE não conhece os componentes e por isso não consegue fazer sugestões inteligentes para usar no autocomplete. Para auxiliar com isso nós geramos um arquivo `vscode-data.json` com as definições dos componentes e disponibilizamos junto com o nosso pacote npm.

Para importar no seu VSCODE, adicione o campo abaixo alterando o local onde o seu projeto armazena o `node_modules`.

```json
{
  "html.customData": ["./node_modules/@govbr-ds/webcomponents/vscode-data.json"]
}
```

### Build

Nesse projeto stencil ao gerar um `build` do projeto também são gerados automaticamente:

- Arquivo `readme.md` dentro da pasta de cada componente.
- Documentações dos componentes na pasta `apps/site/docs/stencil-generated-docs` para serem usadas e versionadas pelo site.

### Exemplos de uso

Disponibilizamos alguns exemplos de como usar esse projeto com algumas tecnologias. Consulte o [nosso grupo aqui no gitlab](https://gitlab.com/govbr-ds/bibliotecas/wc 'GovBR-DS/WC') e procure pelos projetos de 'Quickstart' para mais detalhes.

## Arquitetura do Projeto Stencil

### Estrutura do Projeto

```markdown
webcomponents/
├── src/
│ ├── components/
│ │ └── br-component/
│ │ ├── sections/
│ │ │ └── migrate.md
│ │ ├── tests/
│ │ │ ├── br-component.e2e.ts
│ │ │ └── br-component.spec.tsx
│ │ ├── br-component.scss
│ │ ├── br-component.tsx
│ │ └── readme.md
│ ├── pages/
│ │ └── components/
│ │ └── br-component/
│ │ └── index.js
│ │ └── exemplo.html
│ │ └── scripts/
│ └── utils/
│ └── index.html
├── stencil.config.ts
├── package.json
└── tsconfig.json
```

#### Descrição dos Diretórios

- **src/**: Contém o código-fonte do projeto.

  - **components/**: Contém os componentes Web que serão criados com Stencil. Cada componente tem sua própria pasta.

    - **br-component/**: Uma pasta específica para o componente `br-component`.

      - **sections/**: Pasta com arquivos markdown para serem incluídos no site.

        - **migrate.md**: Arquivo explicando 'como migrar' o componente da versão anterior para a nova.

      - **test/**: Pasta com os arquivos de teste.

        - **br-component.e2e.ts**: Arquivo para testes end-to-end (E2E) do componente.

        - **br-component.spec.tsx**: Arquivo para testes unitários (Unit) do componente.

      - **br-component.tsx**: O arquivo principal do componente, que contém a lógica e a estrutura do componente.

      - **br-component.scss**: O arquivo de estilo do componente.

      - **readme.md**: Documentação gerada automaticamente do componente durante `build`.

  - **pages/**: Contém os exemplos dos componentes Web que serão criados com Stencil. Cada componente tem sua própria pasta.

    - **components/**: Pasta com os exemplos dos componentes.

    - **scripts/**: Pasta com os scripts usados no ambiente de desenvolvimento.

      - **br-component/**: Uma pasta específica para o componente `br-component`.

        - **index.js**: Arquivo com paga executar ações nos exemplos, como escutar por eventos.

        - **exemplo.html**: Arquivos com o código de cada exemplo.

  - **utils/**: Pasta para utilitários ou funções auxiliares que podem ser usadas em todo o projeto.
  -
  - **index.html**: Página inicial do servidor de desenvolvimento.

#### Arquivos de Configuração

- **stencil.config.ts**: O principal arquivo de configuração para um projeto Stencil. Nele, você define opções de configuração como saída do build, plugins, global scripts, e outras opções.

- **package.json**: Contém as dependências do projeto e scripts úteis para construir e desenvolver os componentes.

- **tsconfig.json**: Arquivo de configuração do TypeScript que especifica as opções do compilador TypeScript para o projeto.

## Documentações Complementares 📚

Consulte a seção sobre Web Componente na nossa [Wiki](https://gov.br/ds/wiki/desenvolvimento/web-components) para obter mais informações sobre esse projeto.

Para saber mais detalhes sobre a especificação Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Contribuindo 🤝

Antes de abrir um Merge Request, por favor, leve em consideração as seguintes informações:

- Este é um projeto open-source e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, use um título claro e explicativo para o MR, e siga os padrões estabelecidos em nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir? Consulte o nosso guia [como contribuir](https://gov.br/ds/wiki/comunidade/contribuindo-com-o-ds/ 'Como contribuir?').

## Reportar Bugs/Problemas ou Sugestões 🐛

Para reportar problemas ou sugerir melhorias, abra uma [issue](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/issues/new). Utilize o modelo adequado e forneça o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Commits 📝

Este projeto segue um padrão para branches e commits. Consulte a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender mais sobre esses padrões.

## Precisa de ajuda? 🆘

Por favor, **não** crie issues para perguntas gerais.

Use os canais abaixo para tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)
- Web Components [https://gov.br/ds/webcomponents](https://gov.br/ds/webcomponents)
- Canal no Discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Créditos 🎉

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') foram desenvolvidos pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') em colaboração com a comunidade.
