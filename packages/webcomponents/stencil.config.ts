import { resolve } from 'path'

import purgecss from '@fullhuman/postcss-purgecss'
import { angularOutputTarget } from '@stencil/angular-output-target'
import { Config } from '@stencil/core'
import { reactOutputTarget } from '@stencil/react-output-target'
import { sass } from '@stencil/sass'
import { vueOutputTarget } from '@stencil/vue-output-target'
import { postcss } from '@stencil-community/postcss'
import autoprefixer from 'autoprefixer'
import cssnano from 'cssnano'
import purgeReplace from 'postcss-replace'

import { generateMarkdown } from './custom-docs/output-docs'
import { angularValueAccessorBindings, vueComponentModels } from './src/value-accessors'

const namespace = 'webcomponents'
const componentCorePackage = `@govbr-ds/${namespace}`
const customElementsDir = 'dist/components'

const purge = purgecss({
  content: ['../../packages/webcomponents/src/**/*.tsx', '../../packages/webcomponents/src/**/*.scss'],
  safelist: {
    standard: [':host', /^br-/, /^bg-/],
    deep: [/^deep-/],
    greedy: [/^greedy-/],
  },
  defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || [],
})

export const config: Config = {
  namespace,
  taskQueue: 'async',
  preamble: 'Construído com @govbr-ds/webcomponents',
  buildEs5: 'prod',
  buildDist: true,
  enableCache: true,
  extras: {
    enableImportInjection: true,
    cloneNodeFix: true,
    appendChildSlotFix: true,
  },
  outputTargets: [
    {
      type: 'dist',
      copy: [{ src: '../README.md' }, { src: '../LICENSE' }],
    },
    {
      type: 'dist-custom-elements',
      customElementsExportBehavior: 'single-export-module',
      externalRuntime: false,
      minify: true,
      generateTypeDeclarations: true,
    },
    {
      type: 'docs-custom',
      generator: generateMarkdown,
    },
    {
      type: 'docs-vscode',
      file: 'vscode-data.json',
    },
    {
      // https://stenciljs.com/docs/hydrate-app
      type: 'dist-hydrate-script',
      dir: 'dist/hydrate',
    },
    {
      type: 'www',
      serviceWorker: null,
      copy: [
        {
          src: '../../../node_modules/@govbr-ds/core/dist/core.min.css',
          dest: 'assets/core.min.css',
          warn: true,
        },
        {
          src: '../../../apps/site/docs/stencil-generated-docs',
          dest: 'assets/stencil-generated-docs',
          warn: false,
        },
        {
          src: 'pages',
          warn: true,
          keepDirStructure: true,
        },
        {
          src: 'assets',
          keepDirStructure: true,
          warn: true,
        },
      ],
    },
    angularOutputTarget({
      componentCorePackage,
      outputType: 'component',
      directivesProxyFile: resolve(__dirname, '../angular/src/stencil-generated/components.ts').replace(/\\/g, '/'),
      directivesArrayFile: resolve(__dirname, '../angular/src/stencil-generated/index.ts').replace(/\\/g, '/'),
      valueAccessorConfigs: angularValueAccessorBindings,
      customElementsDir,
    }),
    angularOutputTarget({
      componentCorePackage,
      outputType: 'standalone',
      directivesProxyFile: resolve(__dirname, '../angular/standalone/src/stencil-generated/components.ts').replace(
        /\\/g,
        '/'
      ),
      directivesArrayFile: resolve(__dirname, '../angular/standalone/src/stencil-generated/index.ts').replace(
        /\\/g,
        '/'
      ),
      valueAccessorConfigs: angularValueAccessorBindings,
      customElementsDir,
    }),
    reactOutputTarget({
      outDir: resolve(__dirname, '../react/src/stencil-generated').replace(/\\/g, '/'),
      customElementsDir,
    }),
    reactOutputTarget({
      outDir: resolve(__dirname, '../react/ssr/stencil-generated').replace(/\\/g, '/'),
      hydrateModule: '@govbr-ds/webcomponents/dist/hydrate',
      customElementsDir,
    }),
    vueOutputTarget({
      componentCorePackage,
      proxiesFile: resolve(__dirname, '../vue/src/stencil-generated/components.ts').replace(/\\/g, '/'),
      includeImportCustomElements: true,
      includePolyfills: false,
      includeDefineCustomElements: false,
      componentModels: vueComponentModels,
      hydrateModule: '@govbr-ds/webcomponents/dist/hydrate',
      customElementsDir,
    }),
  ],
  devServer: {
    port: 3333,
    reloadStrategy: 'hmr',
    openBrowser: false,
  },
  plugins: [
    sass({
      includePaths: [resolve(__dirname, '../../node_modules').replace(/\\/g, '/')],
      injectGlobalPaths: [
        `../../node_modules/@govbr-ds/core/src/partial/scss/base`.replace(/\\/g, '/'),
        `../../node_modules/@govbr-ds/core/src/partial/scss/utilities`.replace(/\\/g, '/'),
      ],
      outputStyle: 'compressed',
      sourceMap: true,
      sourceMapEmbed: true,
      sourceMapContents: true,
    }),
    postcss({
      plugins: [
        autoprefixer(),
        // O Shadow DOM não respeita o estilo de 'html' e 'body', então substituímos por ':host'
        purgeReplace({ pattern: 'html', data: { replaceAll: ':host' } }),
        // Purge e cssnano se for build de produção
        ...(!process.argv.includes('--dev') ? [purge, cssnano()] : []),
      ],
    }),
  ],
  testing: {
    /**
     * O GitLab CI não permite sandbox, portanto estes parâmetros devem ser passados para o Chrome Headless
     * antes que ele possa executar seus testes
     */
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
}
