import { resolve } from 'path'

import purgecss from '@fullhuman/postcss-purgecss'
import { angularOutputTarget } from '@stencil/angular-output-target'
import { Config } from '@stencil/core'
import { reactOutputTarget } from '@stencil/react-output-target'
import { sass } from '@stencil/sass'
import { vueOutputTarget } from '@stencil/vue-output-target'
import { postcss } from '@stencil-community/postcss'
import autoprefixer from 'autoprefixer'
import cssnano from 'cssnano'
import purgeReplace from 'postcss-replace'

import { generateMarkdown } from './custom-docs/output-docs'
import { angularValueAccessorBindings, vueComponentModels } from './src/value-accessors'

const namespace = 'webcomponents'
const componentCorePackage = `@govbr-ds/webcomponents`
const customElementsDir = 'dist/components'
const resolvePath = (path: string) => resolve(__dirname, path).replace(/\\/g, '/')

const purge = purgecss({
  content: ['../../packages/webcomponents/src/**/*.tsx', '../../packages/webcomponents/src/**/*.scss'],
  safelist: {
    standard: [':host', /^br-/, /^bg-/],
    deep: [/^deep-/],
    greedy: [/^greedy-/],
  },
  defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || [],
})

export const config: Config = {
  namespace,
  taskQueue: 'async',
  preamble: 'Construído com @govbr-ds/webcomponents',
  buildEs5: 'prod',
  buildDist: true,
  enableCache: true,
  extras: {
    enableImportInjection: true,
    cloneNodeFix: true,
    appendChildSlotFix: true,
  },
  outputTargets: [
    {
      type: 'dist',
      copy: [{ src: '../README.md', dest: '../../README.md', warn: true }],
    },
    {
      type: 'dist-custom-elements',
      customElementsExportBehavior: 'single-export-module',
      externalRuntime: false,
      minify: true,
    },
    {
      type: 'docs-custom',
      generator: generateMarkdown,
    },
    {
      type: 'docs-vscode',
      file: 'vscode-data.json',
    },
    {
      // https://stenciljs.com/docs/hydrate-app
      type: 'dist-hydrate-script',
      dir: 'dist/hydrate',
    },
    {
      type: 'www',
      serviceWorker: null,
      copy: [
        {
          src: '../../../node_modules/@govbr-ds/core/dist/core.min.css',
          dest: 'assets/core.min.css',
          warn: true,
        },
        {
          src: '../../../apps/site/docs/stencil-generated-docs',
          dest: 'assets/stencil-generated-docs',
          warn: false,
        },
        {
          src: 'pages',
          warn: true,
          keepDirStructure: true,
        },
        {
          src: 'assets',
          keepDirStructure: true,
          warn: true,
        },
      ],
    },
    angularOutputTarget({
      /* --------------------------- Angular output target -------------------------- */
      componentCorePackage,
      // Gera muitos wrappers de componentes vinculados a um único módulo Angular (abordagem lazy/hydrated)
      directivesProxyFile: resolvePath('../angular/src/stencil-generated/components.ts'),
      directivesArrayFile: resolvePath('../angular/src/stencil-generated/index.ts'),
      valueAccessorConfigs: angularValueAccessorBindings,
      customElementsDir,
    }),
    angularOutputTarget({
      /* -------------------- Angular Standalone output target -------------------- */
      componentCorePackage,
      // Gere um componente com a flag standalone definida como true.
      directivesProxyFile: resolvePath('../angular/standalone/src/stencil-generated/components.ts'),
      directivesArrayFile: resolvePath('../angular/standalone/src/stencil-generated/index.ts'),
      valueAccessorConfigs: angularValueAccessorBindings,
      customElementsDir,
    }),
    reactOutputTarget({
      /* --------------------------- React output target -------------------------- */
      outDir: resolvePath('../react/src/stencil-generated'),
      customElementsDir,
    }),
    reactOutputTarget({
      /* ------------------------- React SSR output target ------------------------ */
      outDir: resolvePath('../react/ssr/stencil-generated'),
      hydrateModule: '@govbr-ds/webcomponents/dist/hydrate',
      customElementsDir,
    }),
    vueOutputTarget({
      /* ---------------------------- Vue output target --------------------------- */
      componentCorePackage,
      proxiesFile: resolvePath('../vue/src/stencil-generated/components.ts'),
      includeImportCustomElements: true,
      includePolyfills: false,
      includeDefineCustomElements: false,
      componentModels: vueComponentModels,
      hydrateModule: '@govbr-ds/webcomponents/dist/hydrate',
      customElementsDir,
    }),
  ],
  devServer: {
    port: 3333,
    reloadStrategy: 'pageReload',
    openBrowser: false,
  },
  plugins: [
    sass({
      includePaths: [resolvePath('../../node_modules')],
      injectGlobalPaths: [
        resolvePath(`../../node_modules/@govbr-ds/core/src/partial/scss/base`),
        resolvePath(`../../node_modules/@govbr-ds/core/src/partial/scss/utilities`),
      ],
      outputStyle: 'compressed',
      sourceMap: true,
      sourceMapEmbed: true,
      sourceMapContents: true,
    }),
    postcss({
      plugins: [
        autoprefixer(),
        // O Shadow DOM não respeita o estilo de 'html' e 'body', então substituímos por ':host'
        purgeReplace({ pattern: 'html', data: { replaceAll: ':host' } }),
        // Purge e cssnano se for build de produção
        ...(!process.argv.includes('--dev') ? [purge, cssnano()] : []),
      ],
    }),
  ],
  testing: {
    /**
     * O GitLab CI não permite sandbox, portanto estes parâmetros devem ser passados para o Chrome Headless
     * antes que ele possa executar seus testes
     */
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
}
