# Wrapper Angular para @govbr-ds/webcomponents

Um wrapper Angular que encapsula os [Web Components GovBR-DS](https://gov.br/ds/webcomponents), habilita `NG_VALUE_ACCESSORS` e permite vincular eventos de entrada diretamente a um `value accessor` para integração perfeita no fluxo de dados bidirecional do Angular.

## Por que usar este wrapper? 🤔

Usar o wrapper Angular para os Web Components GovBR-DS oferece várias vantagens:

- **Desacoplamento da Detecção de Mudanças**: Os wrappers de componentes Angular serão desacoplados da detecção de mudanças, prevenindo repinturas desnecessárias do seu componente web.

- **Conversão de Eventos**: Os eventos dos componentes web serão convertidos para observáveis RxJS para alinhar com o @Output() do Angular e não serão emitidos através dos limites dos componentes.

- **Control Value Accessors**: Opcionalmente, os componentes web de controle de formulário podem ser usados como control value accessors com os formulários reativos do Angular ou `ngModel`.

- **Sem Necessidade de CUSTOM_ELEMENTS_SCHEMA**: Não é necessário incluir o CUSTOM_ELEMENTS_SCHEMA do Angular em todos os módulos que consomem seus componentes Stencil.

Para mais detalhes, consulte a [documentação oficial do Stencil](https://stenciljs.com/docs/angular).

## Instalação ⚙️

Instale os pacotes abaixo:

```bash
npm install --save-dev @govbr-ds/{core,webcomponents,webcomponents-angular}
```

## Configuração 🛠️

Para usar o wrapper Angular dos Web Components GovBR-DS em uma aplicação Angular, você precisa seguir os seguintes passos:

### Adicione o módulo Angular ao módulo do seu aplicativo

Adicione o `WebcomponentsAngularModule` exportado por `@govbr-ds/webcomponents-angular`:

```ts
/** app.module.ts */
import { WebcomponentsAngularModule } from '@govbr-ds/webcomponents-angular';
...

@NgModule({
  imports: [WebcomponentsAngularModule.forRoot(), ...],
  ...
})
export class AppModule {}
```

### Adicione os tokens do `@govbr-ds/core`

```json
/** angular.json */
{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "Angular-Project": {
      ...
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            ...
            "styles": [
              ...
              "node_modules/@govbr-ds/core/dist/core-tokens.min.css"
            ],
            ...
          }
      }
    }
  }
}
```

Os estilos do `govbr-ds/core` também podem ser importados para o arquivo de estilo principal do seu aplicativo caso não deseje incluí-los no `angular.json`:

```css
@import '~@govbr-ds/core/dist/core-tokens.min.css';
```

## Uso com NgModel e binding 🔄

Os Web Components podem ser usados como qualquer outra tag HTML e funcionam no mecanismo de reatividade do Angular, como `one-way-binding`, `two-way-binding` e `events`.

```html
<br-button [active]="isActive" [disabled]="isDisabled" emphasis="primary" (onClick)="onClick()">
  {{ label }}
</br-button>
```

Para habilitar a vinculação bidirecional e o uso de `ngModel` dentro dos componentes de formulário, você precisará adicionar o `@angular/forms`. Também é necessário colocar o `ngDefaultControl` na tag dos componentes para ativar o `ngModel`.

Por favor, note que pode ser necessário desativar `aot` para habilitar a vinculação bidirecional de dados. Mais informações: [https://github.com/ionic-team/stencil-ds-output-targets/issues/317](https://github.com/ionic-team/stencil-ds-output-targets/issues/317).

```ts
import { FormsModule } from '@angular/forms';
import { WebcomponentsAngularModule } from '@govbr-ds/webcomponents-angular';
...

@NgModule({
  imports: [WebcomponentsAngularModule.forRoot(), FormsModule, ...],
  ...
})
export class AppModule {}
```

```html
<!-- Template do componente Angular -->

<br-checkbox
  name="userTermsConditions"
  [(ngModel)]="termsConditions"
  (checkedChange)="onTermsConditionsChange()"
  ngDefaultControl
>
  Concordo com os Termos e Condições
</br-checkbox>
```

```ts
/** Typescript do componente Angular */
import { Component } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  termsConditions = true

  onTermsConditionsChange() {
    console.log('O valor dos termos e condições mudou!', this.termsConditions)
  }
}
```

## Angular standalone 1️⃣

Você também pode usar nosso wrapper Angular no Angular standalone. Para isso, você precisará importar os componentes de `@govbr-ds/webcomponents-angular/standalone` e usá-los como usaria qualquer outro componente Angular.

```ts
/** Typescript do componente Angular */
import { FormsModule } from '@angular/forms';
import { BrButton } from '@govbr-ds/webcomponents-angular/standalone';
...

@Component({
  selector: 'app-component',
  templateUrl: './app.component.html',
  standalone: true,
  imports: [BrButton],
})
export class AppComponent2 {}
```

```html
<!-- Template do componente Angular -->
<br-button emphasis="primary">Lorem ipsum</br-button>
```

## Estrutura de pastas e arquivos 📁

```plaintext
- src/
  - stencil-generated/
  - angular-webcomponents.module.ts
  - index.ts
- standalone/
  - src/
    - stencil-generated/
    - index.ts
- scripts
```

A pasta `src` contém os arquivos da biblioteca Angular padrão, enquanto a `standalone` contém os arquivos para o Angular standalone.

Ao gerar um build da biblioteca de Web Components, os arquivos da pasta `src/stencil-generated` e `standalone/stencil-generated` são recriados. **Todas as alterações nesses arquivos serão perdidas ao gerar um novo build dos Web Components.**

O arquivo `angular-webcomponents.module.ts` exporta os componentes, os registra como custom elements e exporta os Control Value Accessors.

Os arquivos `index.ts` são os entry points da biblioteca.

A pasta `scripts` contém o script para corrigir um bug que ocorre algumas vezes ao gerar o build dos Web Components.

## Build 📦

Antes de compilar a biblioteca Angular, é necessário gerar o build dos webcomponents:

```bash
nx build webcomponents
```

Em seguida, para gerar o build da biblioteca Angular, execute:

```bash
nx build angular
```

## Documentações Complementares 📚

Consulte a seção sobre Web Componente na nossa [Wiki](https://gov.br/ds/wiki/desenvolvimento/web-components) para obter mais informações sobre esse projeto.

Para saber mais detalhes sobre a especificação Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Contribuindo 🤝

Antes de abrir um Merge Request, por favor, leve em consideração as seguintes informações:

- Este é um projeto open-source e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, use um título claro e explicativo para o MR, e siga os padrões estabelecidos em nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir? Consulte o nosso guia [como contribuir](https://gov.br/ds/wiki/comunidade/contribuindo-com-o-ds/ 'Como contribuir?').

## Reportar Bugs/Problemas ou Sugestões 🐛

Para reportar problemas ou sugerir melhorias, abra uma [issue](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/issues/new). Utilize o modelo adequado e forneça o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Commits 📝

Este projeto segue um padrão para branches e commits. Consulte a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender mais sobre esses padrões.

## Precisa de ajuda? 🆘

Por favor, **não** crie issues para perguntas gerais.

Use os canais abaixo para tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)
- Web Components [https://gov.br/ds/webcomponents](https://gov.br/ds/webcomponents)
- Canal no Discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Créditos 🎉

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') foram desenvolvidos pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') em colaboração com a comunidade.
