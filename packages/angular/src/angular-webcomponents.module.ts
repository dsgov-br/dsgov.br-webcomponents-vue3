import { CommonModule, DOCUMENT } from '@angular/common'
import { APP_INITIALIZER, ModuleWithProviders, NgModule, NgZone } from '@angular/core'
import { defineCustomElements } from '@govbr-ds/webcomponents/dist/loader'
import { DIRECTIVES } from './stencil-generated'
import { BooleanValueAccessor } from './stencil-generated/boolean-value-accessor'
import { NumericValueAccessor } from './stencil-generated/number-value-accessor'
import { RadioValueAccessor } from './stencil-generated/radio-value-accessor'
// import { SelectValueAccessor } from './stencil-generated/select-value-accessor'
import { TextValueAccessor } from './stencil-generated/text-value-accessor'

const DECLARATIONS = [
  ...DIRECTIVES,
  BooleanValueAccessor,
  TextValueAccessor,
  // SelectValueAccessor,
  NumericValueAccessor,
  RadioValueAccessor,
]

@NgModule({
  imports: [CommonModule],
  declarations: DECLARATIONS,
  exports: DECLARATIONS,
})
export class WebcomponentsAngularModule {
  static forRoot(): ModuleWithProviders<WebcomponentsAngularModule> {
    return {
      ngModule: WebcomponentsAngularModule,
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: () => defineCustomElements,
          multi: true,
          deps: [DOCUMENT, NgZone],
        },
      ],
    }
  }
}
