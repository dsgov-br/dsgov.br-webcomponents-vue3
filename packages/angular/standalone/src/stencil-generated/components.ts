/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';

import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@govbr-ds/webcomponents/dist/components';

import { defineCustomElement as defineBrAvatar } from '@govbr-ds/webcomponents/dist/components/br-avatar.js';
import { defineCustomElement as defineBrButton } from '@govbr-ds/webcomponents/dist/components/br-button.js';
import { defineCustomElement as defineBrCheckbox } from '@govbr-ds/webcomponents/dist/components/br-checkbox.js';
import { defineCustomElement as defineBrCheckgroup } from '@govbr-ds/webcomponents/dist/components/br-checkgroup.js';
import { defineCustomElement as defineBrDropdown } from '@govbr-ds/webcomponents/dist/components/br-dropdown.js';
import { defineCustomElement as defineBrIcon } from '@govbr-ds/webcomponents/dist/components/br-icon.js';
import { defineCustomElement as defineBrInput } from '@govbr-ds/webcomponents/dist/components/br-input.js';
import { defineCustomElement as defineBrItem } from '@govbr-ds/webcomponents/dist/components/br-item.js';
import { defineCustomElement as defineBrList } from '@govbr-ds/webcomponents/dist/components/br-list.js';
import { defineCustomElement as defineBrLoading } from '@govbr-ds/webcomponents/dist/components/br-loading.js';
import { defineCustomElement as defineBrMessage } from '@govbr-ds/webcomponents/dist/components/br-message.js';
import { defineCustomElement as defineBrRadio } from '@govbr-ds/webcomponents/dist/components/br-radio.js';
import { defineCustomElement as defineBrSelect } from '@govbr-ds/webcomponents/dist/components/br-select.js';
import { defineCustomElement as defineBrSwitch } from '@govbr-ds/webcomponents/dist/components/br-switch.js';
import { defineCustomElement as defineBrTag } from '@govbr-ds/webcomponents/dist/components/br-tag.js';
import { defineCustomElement as defineBrTextarea } from '@govbr-ds/webcomponents/dist/components/br-textarea.js';
import { defineCustomElement as defineBrUpload } from '@govbr-ds/webcomponents/dist/components/br-upload.js';
@ProxyCmp({
  defineCustomElementFn: defineBrAvatar,
  inputs: ['alt', 'customId', 'density', 'disabled', 'iconHeight', 'iconMarginTop', 'iconWidth', 'isIconic', 'src', 'text']
})
@Component({
  selector: 'br-avatar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['alt', 'customId', 'density', 'disabled', 'iconHeight', 'iconMarginTop', 'iconWidth', 'isIconic', 'src', 'text'],
  standalone: true
})
export class BrAvatar {
  protected el: HTMLBrAvatarElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrAvatar extends Components.BrAvatar {}


@ProxyCmp({
  defineCustomElementFn: defineBrButton,
  inputs: ['colorMode', 'customId', 'density', 'disabled', 'emphasis', 'isActive', 'isLoading', 'shape', 'type', 'value']
})
@Component({
  selector: 'br-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['colorMode', 'customId', 'density', 'disabled', 'emphasis', 'isActive', 'isLoading', 'shape', 'type', 'value'],
  standalone: true
})
export class BrButton {
  protected el: HTMLBrButtonElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrButton extends Components.BrButton {}


@ProxyCmp({
  defineCustomElementFn: defineBrCheckbox,
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'indeterminate', 'label', 'name', 'state', 'value'],
  methods: ['setIndeterminate', 'toggleChecked']
})
@Component({
  selector: 'br-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'indeterminate', 'label', 'name', 'state', 'value'],
  standalone: true
})
export class BrCheckbox {
  protected el: HTMLBrCheckboxElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['checkedChange', 'indeterminateChange']);
  }
}


export declare interface BrCheckbox extends Components.BrCheckbox {
  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  checkedChange: EventEmitter<CustomEvent<boolean>>;
  /**
   * Disparado depois que o valor do `indeterminate` foi alterado.
   */
  indeterminateChange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrCheckgroup,
  inputs: ['customId', 'indeterminate', 'label', 'labelDesselecionado', 'labelSelecionado']
})
@Component({
  selector: 'br-checkgroup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'indeterminate', 'label', 'labelDesselecionado', 'labelSelecionado'],
  standalone: true
})
export class BrCheckgroup {
  protected el: HTMLBrCheckgroupElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrCheckgroup extends Components.BrCheckgroup {}


@ProxyCmp({
  defineCustomElementFn: defineBrDropdown,
  inputs: ['customId', 'isOpen'],
  methods: ['open', 'hide']
})
@Component({
  selector: 'br-dropdown',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'isOpen'],
  standalone: true
})
export class BrDropdown {
  protected el: HTMLBrDropdownElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDropdownChange']);
  }
}


export declare interface BrDropdown extends Components.BrDropdown {
  /**
   * Evento emitido quando o estado do dropdown muda.
   */
  brDropdownChange: EventEmitter<CustomEvent<{ 'is-opened': boolean }>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrIcon,
  inputs: ['cssClasses', 'customId', 'flip', 'height', 'iconName', 'isInline', 'rotate', 'width']
})
@Component({
  selector: 'br-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['cssClasses', 'customId', 'flip', 'height', 'iconName', 'isInline', 'rotate', 'width'],
  standalone: true
})
export class BrIcon {
  protected el: HTMLBrIconElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrIcon extends Components.BrIcon {}


@ProxyCmp({
  defineCustomElementFn: defineBrInput,
  inputs: ['autocomplete', 'autocorrect', 'customId', 'density', 'disabled', 'helpText', 'isHighlight', 'isInline', 'label', 'max', 'maxlength', 'min', 'minlength', 'multiple', 'name', 'pattern', 'placeholder', 'readonly', 'required', 'state', 'step', 'type', 'value']
})
@Component({
  selector: 'br-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['autocomplete', 'autocorrect', 'customId', 'density', 'disabled', 'helpText', 'isHighlight', 'isInline', 'label', 'max', 'maxlength', 'min', 'minlength', 'multiple', 'name', 'pattern', 'placeholder', 'readonly', 'required', 'state', 'step', 'type', 'value'],
  standalone: true
})
export class BrInput {
  protected el: HTMLBrInputElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['valueChange']);
  }
}


export declare interface BrInput extends Components.BrInput {
  /**
   * Valor atualizado do input
   */
  valueChange: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrItem,
  inputs: ['customId', 'density', 'disabled', 'href', 'isActive', 'isButton', 'isInteractive', 'isSelected', 'target', 'type', 'value']
})
@Component({
  selector: 'br-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'density', 'disabled', 'href', 'isActive', 'isButton', 'isInteractive', 'isSelected', 'target', 'type', 'value'],
  standalone: true
})
export class BrItem {
  protected el: HTMLBrItemElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDidClick', 'brDidSelect']);
  }
}


export declare interface BrItem extends Components.BrItem {
  /**
   * Evento customizado emitido quando o item é clicado, aplicável apenas se o item for um botão (`<button>`).
Pode ser utilizado para ações personalizadas, exceto quando o item está desativado.
   */
  brDidClick: EventEmitter<CustomEvent<any>>;
  /**
   * Evento customizado aplicável para todos os tipos de elementos (`div`, `button`, `a`), emitido quando o item é selecionado e desde que a propriedade `isInteractive` esteja presente.
   */
  brDidSelect: EventEmitter<CustomEvent<{ selected: boolean }>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrList,
  inputs: ['customId', 'header', 'hideHeaderDivider', 'isHorizontal']
})
@Component({
  selector: 'br-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'header', 'hideHeaderDivider', 'isHorizontal'],
  standalone: true
})
export class BrList {
  protected el: HTMLBrListElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrList extends Components.BrList {}


@ProxyCmp({
  defineCustomElementFn: defineBrLoading,
  inputs: ['customId', 'isMedium', 'isProgress', 'label', 'progressPercent']
})
@Component({
  selector: 'br-loading',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'isMedium', 'isProgress', 'label', 'progressPercent'],
  standalone: true
})
export class BrLoading {
  protected el: HTMLBrLoadingElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrLoading extends Components.BrLoading {}


@ProxyCmp({
  defineCustomElementFn: defineBrMessage,
  inputs: ['autoRemove', 'customId', 'isClosable', 'isFeedback', 'isInline', 'message', 'messageTitle', 'showIcon', 'state']
})
@Component({
  selector: 'br-message',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['autoRemove', 'customId', 'isClosable', 'isFeedback', 'isInline', 'message', 'messageTitle', 'showIcon', 'state'],
  standalone: true
})
export class BrMessage {
  protected el: HTMLBrMessageElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDidClose']);
  }
}


export declare interface BrMessage extends Components.BrMessage {
  /**
   * Evento emitido quando o usuário fecha a mensagem, se closable for true.
   */
  brDidClose: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrRadio,
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'label', 'name', 'state', 'value'],
  methods: ['toggleChecked']
})
@Component({
  selector: 'br-radio',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'label', 'name', 'state', 'value'],
  standalone: true
})
export class BrRadio {
  protected el: HTMLBrRadioElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['checkedChange']);
  }
}


export declare interface BrRadio extends Components.BrRadio {
  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  checkedChange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrSelect,
  inputs: ['customId', 'isMultiple', 'isOpen', 'label', 'options', 'placeholder', 'selectAllLabel', 'showSearchIcon', 'unselectAllLabel'],
  methods: ['toggleOpen']
})
@Component({
  selector: 'br-select',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'isMultiple', 'isOpen', 'label', 'options', 'placeholder', 'selectAllLabel', 'showSearchIcon', 'unselectAllLabel'],
  standalone: true
})
export class BrSelect {
  protected el: HTMLBrSelectElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDidSelectChange']);
  }
}


export declare interface BrSelect extends Components.BrSelect {
  /**
   * Evento emitido sempre que houver atualização nos itens selecionados.
   */
  brDidSelectChange: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrSwitch,
  inputs: ['checked', 'customId', 'density', 'disabled', 'hasIcon', 'label', 'labelOff', 'labelOn', 'labelPosition', 'name', 'value'],
  methods: ['toggleChecked']
})
@Component({
  selector: 'br-switch',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'customId', 'density', 'disabled', 'hasIcon', 'label', 'labelOff', 'labelOn', 'labelPosition', 'name', 'value'],
  standalone: true
})
export class BrSwitch {
  protected el: HTMLBrSwitchElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['checkedChange']);
  }
}


export declare interface BrSwitch extends Components.BrSwitch {
  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  checkedChange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrTag,
  inputs: ['color', 'customId', 'density', 'disabled', 'iconName', 'interaction', 'interactionSelect', 'label', 'multiple', 'name', 'selected', 'shape', 'status']
})
@Component({
  selector: 'br-tag',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['color', 'customId', 'density', 'disabled', 'iconName', 'interaction', 'interactionSelect', 'label', 'multiple', 'name', 'selected', 'shape', 'status'],
  standalone: true
})
export class BrTag {
  protected el: HTMLBrTagElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['radioSelected']);
  }
}


export declare interface BrTag extends Components.BrTag {
  /**
   * Evento emitido quando a tag é selecionada.
   */
  radioSelected: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrTextarea,
  inputs: ['customId', 'density', 'disabled', 'isInline', 'label', 'maxlength', 'placeholder', 'showCounter', 'state', 'value'],
  methods: ['setValue']
})
@Component({
  selector: 'br-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'density', 'disabled', 'isInline', 'label', 'maxlength', 'placeholder', 'showCounter', 'state', 'value'],
  standalone: true
})
export class BrTextarea {
  protected el: HTMLBrTextareaElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['valueChange']);
  }
}


export declare interface BrTextarea extends Components.BrTextarea {
  /**
   * Valor atualizado do textarea
   */
  valueChange: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  defineCustomElementFn: defineBrUpload,
  inputs: ['accept', 'disabled', 'label', 'multiple', 'state']
})
@Component({
  selector: 'br-upload',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['accept', 'disabled', 'label', 'multiple', 'state'],
  standalone: true
})
export class BrUpload {
  protected el: HTMLBrUploadElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrUpload extends Components.BrUpload {}


