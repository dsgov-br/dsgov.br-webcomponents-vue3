#!/bin/bash

# Script para deletar epics do GitLab
# Uso: ./delete_all_epics.sh [epic_iid1 epic_iid2 ...]
# Se nenhum número de epic for fornecido, todos os epics serão deletados

# Valores padrão
GITLAB_API_URL="https://gitlab.com/api/v4"
DRY_RUN=true # Sempre começa em dry-run
EPIC_STATE="opened"
declare -a EPIC_IIDS=()

if [ -z "$GITLAB_TOKEN" ] || [ -z "$GROUP_ID" ]; then
  echo "Erro: GITLAB_TOKEN e GROUP_ID precisam ser configurados no script"
  exit 1
fi

delete_epic() {
  local epic_iid=$1
  delete_response=$(curl --silent --request DELETE --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/groups/$GROUP_ID/epics/$epic_iid")
  echo "Epic $epic_iid deletado: $delete_response"
}

# Se argumentos foram fornecidos, delete apenas os epics específicos
if [ $# -gt 0 ]; then
  echo "Deletando epics específicos: $@"
  for epic_iid in "$@"; do
    delete_epic "$epic_iid"
  done
else
  # Caso contrário, delete todos os epics
  echo "Obtendo todos os epics do grupo..."
  response=$(curl --silent --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/groups/$GROUP_ID/epics?per_page=100")

  if [ -z "$response" ] || [ "$response" = "null" ]; then
    echo "Nenhum epic encontrado ou erro na requisição"
    exit 1
  fi

  echo "Deletando todos os epics..."
  echo "$response" | jq -r '.[].iid' | while read epic_iid; do
    delete_epic "$epic_iid"
  done
fi
