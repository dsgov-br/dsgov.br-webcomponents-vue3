import './styles.scss'
import router from './router'
import { createApp, Plugin } from 'vue'
import App from './app/App.vue'
import * as WebComponentsVue from '@govbr-ds/webcomponents-vue'

const app = createApp(App)
app.use(router)
app.use(WebComponentsVue as unknown as Plugin)
app.mount('#root')
