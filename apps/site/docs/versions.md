---
description: Versionamento no @govbr-ds/webcomponents
sidebar_position: 96
slug: 'versionamento'
title: 'Versionamento'
---

Na nossa biblioteca, não apenas adotamos o versionamento semântico para gerenciar as versões dos nossos softwares, mas também utilizamos o padrão de commits conhecido como Conventional Commits para registrar as alterações no código-fonte de forma clara e padronizada.

## Versionamento Semântico {#versionamento-semântico}

Seguindo o padrão do versionamento semântico, as nossas versões são numeradas no formato **X.Y.Z**, onde:

**X** representa a versão principal. Incrementamos o número da versão principal quando fazemos alterações que podem não ser compatíveis com versões anteriores, como mudanças significativas na arquitetura ou na API.

**Y** representa a versão secundária. Incrementamos o número da versão secundária quando adicionamos novos recursos de uma forma compatível com as versões anteriores, ou seja, quando mantemos a retrocompatibilidade.

**Z** representa a versão de correção. Incrementamos o número da versão de correção quando fazemos correções de bugs ou pequenas melhorias que não afetam a compatibilidade.

Além disso, periodicamente disponibilizamos versões "next" e "alpha" para permitir que os usuários experimentem novos recursos ou funcionalidades que ainda estão em desenvolvimento ou fase de teste.

**Versões Next**: São versões que contêm recursos ou correções que serão incluídos na próxima versão principal ou secundária. Elas oferecem aos usuários a oportunidade de testar esses recursos antes do lançamento oficial.

**Versões Alpha**: São versões ainda mais experimentais, geralmente disponibilizadas apenas para testadores internos ou grupos selecionados. Elas podem conter funcionalidades em estágio inicial de desenvolvimento e podem não ser totalmente funcionais ou estáveis.

Essas versões "next" e "alpha" permitem que nossa comunidade de usuários acompanhe de perto o progresso do desenvolvimento, forneça feedback precoce e nos ajude a identificar e corrigir problemas antes do lançamento oficial.

Em resumo, ao adotar o versionamento semântico e disponibilizar versões "next" e "alpha", buscamos garantir transparência, estabilidade e qualidade em nossos produtos, ao mesmo tempo em que permitimos a inovação e a colaboração contínua com nossa comunidade de usuários.

## Conventional Commits {#conventional-commits}

O Conventional Commits é uma convenção simples para estruturar mensagens de commit. Ele define um conjunto de regras e convenções para nomear commits de forma semântica, facilitando a compreensão das mudanças introduzidas em cada commit. Cada mensagem de commit segue um formato específico que inclui um tipo, um escopo opcional e uma descrição.

Ao utilizar o Conventional Commits, garantimos que cada alteração seja descrita de forma consistente e significativa, o que facilita a rastreabilidade das mudanças ao longo do tempo. Isso é especialmente útil quando combinado com o versionamento semântico, pois permite que os desenvolvedores entendam facilmente quais mudanças foram introduzidas em cada versão do software.

Além disso, a combinação do versionamento semântico com o Conventional Commits ajuda na automação de tarefas de lançamento, geração de notas de lançamento e outras atividades relacionadas ao ciclo de vida do software.

:::tip

Para o projeto de Web Components, commits com **os escopos** "site" ou "no-release" não são levados em consideração na hora de definir a versão das bibliotecas.

:::

:::info

Para mais informações sobre como fazemos o versionamento da nossa biblioteca, consulte a [documentação sobre commits na nossa Wiki](https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/git-gitlab/guias/commit/).

:::
