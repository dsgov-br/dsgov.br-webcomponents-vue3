## Métodos

### `setIndeterminate(value: boolean) => Promise<void>`

Define o estado indeterminado do checkbox.

#### Parâmetros

| Nome    | Tipo      | Descrição                               |
| ------- | --------- | --------------------------------------- |
| `value` | `boolean` | Novo valor para o estado indeterminado. |

#### Retorna

Tipo: `Promise<void>`

### `toggleChecked() => Promise<void>`

Inverte o valor da prop `checked`

#### Retorna

Tipo: `Promise<void>`
