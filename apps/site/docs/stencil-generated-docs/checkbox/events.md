## Eventos

| Evento                | Descrição                                                     | Tipo                   |
| --------------------- | ------------------------------------------------------------- | ---------------------- |
| `checkedChange`       | Disparado depois que o valor do `checked` foi alterado        | `CustomEvent<boolean>` |
| `indeterminateChange` | Disparado depois que o valor do `indeterminate` foi alterado. | `CustomEvent<boolean>` |
