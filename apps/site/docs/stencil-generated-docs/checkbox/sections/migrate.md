## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue     | Propriedade Stencil    | Descrição                                                                | Tipo      | Padrão      |
| ------------------- | ---------------------- | ------------------------------------------------------------------------ | --------- | ----------- |
| `dataChild`         | `checkgroupChild`      | Declara o comportamento checkgroup e coloca o checkbox como sendo filho. | `string`  | `undefined` |
| `dataParent`        | `checkgroupParent`     | Declara o comportamento checkgroup e coloca o checkbox como sendo pai.   | `string`  | `undefined` |
| `inline`            | --                     | Formata o componente para versão horizontal.                             | `boolean` | `false`     |
| `invalid`           | `format`               | Estado indicativo de que uma opção não é válida.                         | `boolean` | `false`     |
| `name` *(opcional)* | `name` *(obrigatório)* | Agora obrigatório, define o name que será atribuído ao checkbox.         | `string`  | `undefined` |
| `valid`             | `format`               | Estado indicativo de que a opção é válida.                               | `boolean` | `false`     |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-checkbox></br-checkbox>` no Vue e em Stencil:

#### Vue

- Evento *`onChange`*: Evento emitido quando o estado do checkbox é modificado.

- Evento *`update:checked`*: Evento emitido para fazer o two-way data binding com a prop checked.

Exemplo:

```html
<br-checkbox id="checkbox-exemplo" label="teste"> </br-checkbox>
```

Adicionando o listener de evento:

```javascript
document.getElementById('checkbox-exemplo').addEventListener('update:checked', (e) => {
  const checkedCheckbox = e.detail[0]
  console.log('O checkbox ' + checkedCheckbox.label + ' foi checado.')
})
```

#### StencilJS

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `checkedChange`, a propriedade será chamada `onCheckedChange`.

```html
<!-- StencilJS -->
<br-checkbox onEventoPersonalizado="handleEvent"></br-checkbox>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-checkbox></br-checkbox>
</template>

<script>
  const checkboxElement = document.querySelector('br-checkbox')
  checkboxElement.addEventListener('checkedChange', (event) => {
    /* your listener */
  })
</script>
```

| Evento                      | Descrição                                                                                                 |
| --------------------------- | --------------------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`        | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidRender`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidUpdate`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentShouldUpdate`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillLoad`       | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillRender`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillUpdate`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brConnectedCallback`       | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDisconnectedCallback`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `checkedChange`             | Evento emitido quando o checkbox é marcado ou desmarcado.                                                 |
| `formAssociatedCallback`    | Componente foi inserido em um `<form>`                                                                    |
| `formDisassociatedCallback` | Componente foi removido de um `<form>`                                                                    |

### Métodos

- `toggleChecked()`: é método público acessível que alterna o estado de uma variável boolean entre true e false. Veja como exemplo, o método é chamado após 2 segundos usando setTimeout():

```javascript
<script>const checkbox = document.querySelector('my-checkbox'); checkbox.toggleChecked(); }, 2000);</script>
```

### Slots

Foi criado o slot `label` no StencilJs, que permite a personalização do rótulo associado ao checkbox.

```html
<!-- StencilJS -->
<br-checkbox>
  <br-checkbox name="checkbox-1">
    <span slot="label">Clique para aceitar os <a url="#">Termos de Aceite</a></span> //Conteúdo do slot
  </br-checkbox>
</div>
```

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Checkbox em uma aplicação Web.

```html
<template>
  <br-checkbox
    id="cbx-01"
    name="cbx-01"
    label="Opção 1"
    aria-label="opção 1"
    onCheckedChange="handleEvent"
  ></br-checkbox>
</template>
<script>
  export default {
    methods: {
      handleEvent(event) {
        console.log(event.detail)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).
