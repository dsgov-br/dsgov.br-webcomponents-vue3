## Eventos

| Evento       | Descrição                                                               | Tipo               |
| ------------ | ----------------------------------------------------------------------- | ------------------ |
| `brDidClose` | Evento emitido quando o usuário fecha a mensagem, se closable for true. | `CustomEvent<any>` |
