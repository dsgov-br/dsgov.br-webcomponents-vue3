## Slots

| Slot        | Descrição                                                                       |
| ----------- | ------------------------------------------------------------------------------- |
| `"default"` | Utilizado para inserir qualquer conteúdo que será exibido no corpo da mensagem. |
