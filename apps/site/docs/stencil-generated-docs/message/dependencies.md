## Dependências

### Depende de

- [br-icon](../icon)
- [br-button](../button)

### Gráfico

```mermaid
graph TD;
  br-message --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-message --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-message mainComponent
```
