if (!window.elementsToListen && !window.eventsToListen) {
  const elementsToListen = document.querySelectorAll('br-message')
  const eventsToListen = ['brDidClose']
  window.elementsToListen = true

  elementsToListen.forEach((domElement) => {
    eventsToListen.forEach((event) => {
      domElement.addEventListener(event, function (event) {
        console.log({
          Evento: event.type,
          Elemento: domElement.outerHTML,
          ...(event.detail && { Detail: event.detail }),
          ...(event.data && { Data: event.data }),
        })
      })
    })
  })
}