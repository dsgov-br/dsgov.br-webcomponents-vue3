## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### br-loading - Guia de Migração de Web Components de Vue para StencilJS

Este guia detalha as mudanças necessárias para migrar o componente `br-loading` de Vue para StencilJS, incluindo propriedades, eventos e estrutura.

#### Propriedades

| Propriedade Vue | Propriedade Stencil | Descrição                                          | Tipo      | Padrão  |
| :-------------- | :------------------ | :------------------------------------------------- | :-------- | :------ |
| -               | -                   | Atributos ARIA herdados do elemento host.          | `object`  | `{}`    |
| -               | `label`             | Texto exibido abaixo do indicador de carregamento. | `string`  | `''`    |
| -               | `medium`            | Define o tamanho médio para o componente.          | `boolean` | `false` |
| -               | `percent`           | Porcentagem de progresso (se `progress` for true). | `number`  | `null`  |
| -               | `progress`          | Indica se a barra de progresso está visível.       | `boolean` | `false` |

#### Eventos

| Evento                | Descrição                                                                                       |
| :-------------------- | :---------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`  | Emitido após o componente ser carregado.                                                        |
| `brComponentWillLoad` | Emitido antes do componente ser carregado. É aqui que os atributos ARIA herdados são coletados. |
| `brConnectedCallback` | Emitido quando o componente é conectado ao DOM.                                                 |

#### Estrutura

- O componente renderiza um elemento `div` com a classe `br-loading` e atributos ARIA para acessibilidade.
- Se `progress` for `true`, uma barra de progresso com duas máscaras é exibida.
- Se `progress` for `false`, um indicador de carregamento indeterminado e um rótulo (`label`) são exibidos.
- A classe `medium` pode ser adicionada para alterar o tamanho do componente.

#### Exemplo de Uso

```html
<br-loading progress percent="50" label="Carregando..."></br-loading>
```

#### Considerações Finais

- Para eventos do ciclo de vida do StencilJS (`brComponentWillLoad`, etc.), consulte a documentação oficial em [https://stenciljs.com/docs/component-lifecycle](https://stenciljs.com/docs/component-lifecycle).
- Certifique-se de que os estilos do componente estejam corretamente definidos no arquivo `loading.scss`.
- Para suporte ou dúvidas adicionais, consulte a equipe do Padrão Digital de Governo em [https://discord.gg/NkaVZERAT7](https://discord.gg/NkaVZERAT7).
