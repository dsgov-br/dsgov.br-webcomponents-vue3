## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### 1. Nomes de Propriedades

Alguns nomes de propriedades foram alterados na versão 2.x. Verifique as mudanças abaixo para garantir uma migração suave.

#### Propriedades removidas na versão 2.x

| Propriedade         | Descrição                                                                                                                                  | Tipo     | Padrão |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ------ |
| `checkgroup-child`  | Declara o comportamento checkgroup e coloca o checkbox como sendo pai. O filho possui o mesmo valor para a propriedade 'checkgroup-child'  | `string` | `null` |
| `checkgroup-parent` | Declara o comportamento checkgroup e coloca o checkbox como sendo filho. O pai possui o mesmo valor para a propriedade 'checkgroup-parent' | `string` | `null` |

#### Propriedades incluídas na versão 2.x

| Propriedade           | Descrição                                                                                             | Tipo      | Padrão              |
| --------------------- | ----------------------------------------------------------------------------------------------------- | --------- | ------------------- |
| `checked`             | Define o estado de seleção do checkbox.                                                               | `boolean` | `false`             |
| `indeterminate`       | Indica se o grupo de checkboxes está em estado indeterminado (alguns, mas não todos, estão marcados). | `boolean` | `false`             |
| `label`               | Define o rótulo do grupo de caixas de seleção.                                                        | `string`  | `'false'`           |
| `labelDesselecionado` | Define o texto do rótulo quando o checkbox está desmarcado.                                           | `string`  | `'Selecionar tudo'` |
| `labelSelecionado`    | Define o texto do rótulo quando o checkbox está marcado.                                              | `string`  | `'Desmarcar tudo'`  |

### 2. Eventos

O componente `<br-checkgroup></br-checkgroup>` emite o evento `brDidAllCheckboxesSelected`. Esse evento segue o formato padrão do navegador e é disparado após o valor do `checked` ser alterado, retornando um feedback para o componente.

### 3. Slots

Na versão 2.x (StencilJS), foi criado um **slot padrão** que permite a personalização do conteúdo interno do `checkgroup`. O exemplo abaixo demonstra como usá-lo.

```html
<br-checkgroup label="Lista de opções" label-selecionado="Selecionar tudo" label-desselecionado="Desmarcar tudo">
  <br-checkbox id="ckb-2" name="h-checkbox-2" label="Opção 2"></br-checkbox>
</br-checkgroup>
```

### 4. Exemplos de Uso do Componente

#### 4.1. Vue - 1.x (antes da migração para Stencil)

```html
<!-- Vue 1.x -->
<div class="mb-1">
  <br-checkbox label="Parent - Grupo 1" checkgroup-parent="grupo-1"></br-checkbox>
</div>
<div class="mb-1 ml-2">
  <br-checkbox label="Child - Grupo 1" checkgroup-child="grupo-1"></br-checkbox>
</div>
<div class="mb-1 ml-2">
  <br-checkbox label="Child - Grupo 1" checkgroup-child="grupo-1"></br-checkbox>
</div>
<div class="mb-1 ml-2">
  <br-checkbox
    label="Child - Grupo 1 / Parent - Grupo 2"
    checkgroup-child="grupo-1"
    checkgroup-parent="grupo-2"
  ></br-checkbox>
</div>
<div class="mb-1 ml-4">
  <br-checkbox label="Child - Grupo 2" checkgroup-child="grupo-2"></br-checkbox>
</div>
<div class="mb-1 ml-4">
  <br-checkbox label="Child - Grupo 2" checkgroup-child="grupo-2"></br-checkbox>
</div>
<div class="mb-1 ml-4">
  <br-checkbox label="Child - Grupo 2" checkgroup-child="grupo-2"></br-checkbox>
</div>
```

#### 4.2. Vue - 2.x (após a migração para Stencil)

```html
<!-- StencilJS -->
<br-checkgroup label="Lista de opções" label-selecionado="Selecionar tudo" label-desselecionado="Desmarcar tudo">
  <br-checkbox id="ckb-1" name="h-checkbox-1" label="Opção 1" class="mb-1"></br-checkbox>
  <br-checkbox id="ckb-2" name="h-checkbox-2" label="Opção 2"></br-checkbox>
</br-checkgroup>
```

### 5. Considerações Finais

Se você encontrar problemas durante o processo de migração ou tiver dúvidas sobre qualquer etapa, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7) para obter assistência.
