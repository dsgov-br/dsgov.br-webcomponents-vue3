## Slots

| Slot        | Descrição                            |
| ----------- | ------------------------------------ |
| `"target"`  | Slot que recebe o elemento alvo      |
| `"trigger"` | Slot que recebe o elemento acionador |