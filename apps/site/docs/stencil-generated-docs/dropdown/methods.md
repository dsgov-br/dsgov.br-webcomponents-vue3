## Métodos

### `hide() => Promise<{ isOpen: boolean; }>`

Esconde o dropdown.
Define a propriedade `isOpen` como falsa e retorna o novo estado.
Este método é marcado com

#### Retorna

Tipo: `Promise<{ isOpen: boolean; }>`

### `open() => Promise<{ isOpen: boolean; }>`

Abre o dropdown.
Define a propriedade `isOpen` como verdadeira e retorna o novo estado.
Este método é marcado com

#### Retorna

Tipo: `Promise<{ isOpen: boolean; }>`
