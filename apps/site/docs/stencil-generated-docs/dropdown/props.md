## Propriedades

| Propriedade | Atributo    | Descrição                                                                                                                                            | Tipo      | Valor padrão                      |
| ----------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | --------------------------------- |
| `customId`  | `custom-id` | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                               | `string`  | ```br-dropdown-${dropdownId++}``` |
| `isOpen`    | `is-open`   | Indica se o dropdown está aberto ou fechado. Esta propriedade é refletida no DOM e pode ser alterada externamente. O valor padrão é falso (fechado). | `boolean` | `false`                           |
