## Slots

| Slot        | Descrição                                                                                                   |
| ----------- | ----------------------------------------------------------------------------------------------------------- |
| `"default"` | Espaço de conteúdo do textarea. Geralmente usado para incluir texto adicional ou instruções para o usuário. |
