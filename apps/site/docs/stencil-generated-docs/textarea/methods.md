## Métodos

### `setValue(newValue: string) => Promise<void>`

Define um novo valor para o textarea.

#### Parâmetros

| Nome       | Tipo     | Descrição                      |
| ---------- | -------- | ------------------------------ |
| `newValue` | `string` | - O novo valor a ser definido. |

#### Retorna

Tipo: `Promise<void>`
