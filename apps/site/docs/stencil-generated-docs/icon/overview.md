## Visão Geral

O componente `br-icon` fornece uma maneira flexível e dinâmica de incorporar ícones nas aplicações.
Utilizando a biblioteca Iconify, ele permite o uso de uma ampla variedade de ícones.

Com opções para especificar a altura e largura, adicionar classes CSS personalizadas, e definir como o ícone
deve ser rotacionado ou espelhado, o `br-icon` oferece aos desenvolvedores as ferramentas necessárias para
integrar ícones de forma consistente com o estilo e design de suas aplicações, melhorando a experiência do usuário
e a clareza das interfaces.

Para mais informações sobre quais ícones estão disponíveis, consulte a documentação do [Iconify](https://iconify.design/).
