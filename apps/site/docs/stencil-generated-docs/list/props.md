## Propriedades

| Propriedade         | Atributo              | Descrição                                                                              | Tipo      | Valor padrão              |
| ------------------- | --------------------- | -------------------------------------------------------------------------------------- | --------- | ------------------------- |
| `customId`          | `custom-id`           | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado. | `string`  | ```br-list-${listId++}``` |
| `header`            | `header`              | Define o cabeçalho para a lista.                                                       | `string`  | ---                       |
| `hideHeaderDivider` | `hide-header-divider` | Indica que o divider para o título da lista estará oculto.                             | `boolean` | `false`                   |
| `isHorizontal`      | `is-horizontal`       | Indica se a lista será horizontal. Por padrão, a lista é vertical.                     | `boolean` | `false`                   |