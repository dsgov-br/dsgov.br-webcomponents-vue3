## Slots

| Slot        | Descrição                                                                                          |
| ----------- | -------------------------------------------------------------------------------------------------- |
| `"default"` | Slot padrão para inclusão dos itens da lista.                                                      |
| `"header"`  | Slot nomeado para inclusão de ações relacionadas ao cabeçalho do list (uso de botões por exemplo). |
