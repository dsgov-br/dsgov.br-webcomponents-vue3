## Eventos

| Evento          | Descrição                                  | Tipo                  |
| --------------- | ------------------------------------------ | --------------------- |
| `radioSelected` | Evento emitido quando a tag é selecionada. | `CustomEvent<string>` |
