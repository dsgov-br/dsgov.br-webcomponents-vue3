## Dependências

### Depende de

- [br-input](../input)
- [br-icon](../icon)
- [br-checkbox](../checkbox)
- [br-radio](../radio)
- [br-list](../list)

### Gráfico
```mermaid
graph TD;
  br-select --Depende---> br-input
  click br-input "src/components/input" "Link para a documentação do componente input"
  class br-input depComponent
  br-select --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-select --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  br-select --Depende---> br-radio
  click br-radio "src/components/radio" "Link para a documentação do componente radio"
  class br-radio depComponent
  br-select --Depende---> br-list
  click br-list "src/components/list" "Link para a documentação do componente list"
  class br-list depComponent
  br-input --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-input --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-select mainComponent
```