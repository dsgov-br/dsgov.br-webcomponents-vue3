## Métodos

### `toggleOpen() => Promise<void>`

Inverte o valor da prop `isOpen`

#### Retorna

Tipo: `Promise<void>`
