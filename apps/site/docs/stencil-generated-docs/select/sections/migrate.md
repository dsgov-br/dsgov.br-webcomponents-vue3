## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue    | Propriedade Stencil | Descrição                                                                                                  | Tipo      | Padrão                                                               |
| ------------------ | ------------------- | ---------------------------------------------------------------------------------------------------------- | --------- | -------------------------------------------------------------------- |
| --                 | `id`                | Especifica o ID do componente. Se não for fornecido, o ID será gerado automaticamente e de forma aleatória | `string`  | `undefined`                                                          |
| --                 | `isOpen`            | Indica se a listagem de itens do select está expandida.                                                    | `boolean` | `false`                                                              |
| `label`            | `label`             | Rótulo que indica o tipo de informação que deve ser selecionada.                                           | `string`  | `undefined`                                                          |
| `multiple`         | `isMultiple`        | Habilita o modo múltiplo para selecionar várias opções.                                                    | `boolean` | `false`                                                              |
| `options`          | `options`           | JSON contendo as opções disponíveis no select.                                                             | `object`  | `[{ "label": <string>, "value": <string>,  "selected": <boolean> }]` |
| `placeholder`      | `placeholder`       | Texto auxiliar exibido antes de uma seleção.                                                               | `string`  | `Selecione uma opção`                                                |
| `selectAllLabel`   | `selectAllLabel`    | Rótulo para selecionar todas as opções.                                                                    | `string`  | `"Selecionar todos"`                                                 |
| `showSearchIcon`   | `showSearchIcon`    | Exibe o ícone de busca no campo de entrada.                                                                | `boolean` | `false`                                                              |
| `unselectAllLabel` | `unselectAllLabel`  | Rótulo para desmarcar todas as opções.                                                                     | `string`  | `"Desselecionar todos"`                                              |

## Como Utilizar Eventos

Entenda melhor como utilizar os eventos para o componente `<br-select></br-select>` no Vue e em Stencil:

### Vue

| Evento   | Descrição                                   |
| -------- | ------------------------------------------- |
| `change` | Emitido ao alterar ou selecionar uma opção. |

### StencilJS

| Evento              | Descrição                                   |
| ------------------- | ------------------------------------------- |
| `brDidSelectChange` | Emitido ao alterar ou selecionar uma opção. |

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidSelectChange`, a propriedade será chamada `onbrDidSelectChange`.

```html
<!-- StencilJS -->
<br-select onEventoPersonalizado="handleEvent"></br-select>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-select></br-select>
</template>

<script>
  const selectElement = document.querySelector('br-select')
  selectElement.addEventListener('brDidSelectChange', (event) => {
    /* your listener */
  })
</script>
```

## Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

## Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).