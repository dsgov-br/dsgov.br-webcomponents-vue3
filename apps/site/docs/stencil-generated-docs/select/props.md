## Propriedades

| Propriedade        | Atributo             | Descrição                                                                              | Tipo      | Valor padrão                  |
| ------------------ | -------------------- | -------------------------------------------------------------------------------------- | --------- | ----------------------------- |
| `customId`         | `custom-id`          | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado. | `string`  | ```br-select-${selectId++}``` |
| `isMultiple`       | `is-multiple`        | Habilita o modo múltiplo para selecionar várias opções.                                | `boolean` | `false`                       |
| `isOpen`           | `is-open`            | Indica se a listagem de itens do select está expandida                                 | `boolean` | `false`                       |
| `label`            | `label`              | Rótulo que indica o tipo de informação que deve ser selecionada.                       | `string`  | ---                           |
| `options`          | `options`            | JSON contendo as opções do select.                                                     | `string`  | `'[]'`                        |
| `placeholder`      | `placeholder`        | Texto auxiliar exibido antes de uma seleção.                                           | `string`  | `''`                          |
| `selectAllLabel`   | `select-all-label`   | Rótulo para selecionar todas as opções.                                                | `string`  | `'Selecionar todos'`          |
| `showSearchIcon`   | `show-search-icon`   | Exibe o ícone de busca no campo de entrada.                                            | `boolean` | `false`                       |
| `unselectAllLabel` | `unselect-all-label` | Rótulo para desmarcar todas as opções.                                                 | `string`  | `'Desselecionar todos'`       |