## Slots

| Slot        | Descrição                                                                                                                               |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| `"default"` | Descreve o que o switch faz quando a alternância estiver ativada. A label passada por slot tem precedência sobre a propriedade 'label'. |
