## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| **Propriedade Antiga** | **Propriedade Nova** | **Descrição**                                                                                              | **Tipo**                   | **Padrão** |
| ---------------------- | -------------------- | ---------------------------------------------------------------------------------------------------------- | -------------------------- | ---------- |
| `ariaLabel`            | `ariaLabel`          | [OPCIONAL] Acessibilidade: define uma cadeia de caracteres para descrever o elemento.                      | `string`                   | `-`        |
| `checked`              | `checked`            | [OPCIONAL] Se presente, indica que o estado inicial do switch é `true`.                                    | `boolean`                  | `false`    |
| `disabled`             | `disabled`           | [OPCIONAL] Se presente, indica que o switch está desabilitado.                                             | `boolean`                  | `false`    |
| `icon`                 | `hasIcon`            | [OPCIONAL] Se presente, adiciona um ícone padrão à chave de alternância.                                   | `boolean`                  | `false`    |
| `id`                   | `id`                 | [OPCIONAL] ID do componente. Também usado para conectar o rótulo correspondente.                           | `string`                   | `-`        |
| `label`                | `label`              | [OPCIONAL] Descreve o que o switch faz quando ativado. Sobrescreve o rótulo passado por slot, se presente. | `string`                   | `-`        |
| `labelChecked`         | `labelOn`            | [OPCIONAL] Rótulo para o switch ativado, auxiliando no entendimento de sua posição.                        | `string`                   | `null`     |
| `labelNotChecked`      | `labelOff`           | [OPCIONAL] Rótulo para o switch desativado, auxiliando no entendimento de sua posição.                     | `string`                   | `null`     |
| `name`                 | `name`               | [OPCIONAL] Define o valor do atributo `name` do checkbox subjacente.                                       | `string`                   | `-`        |
| `right`                | `labelPosition`      | [OPCIONAL] Se presente, posiciona o rótulo a direita do switch.                                            | `right \| left \| top`     | `left`     |
| `size`                 | `density`            | [OPCIONAL] Ajusta a densidade da área interativa do switch.                                                | `large \| medium \| small` | `medium`   |
| `top`                  | `labelPosition`      | [OPCIONAL] Se presente, posiciona o rótulo acima do switch.                                                | `right \| left \| top`     | `left`     |
| `value`                | `value`              | Valor associado ao checkbox quando o switch está ativado.                                                  | `string`                   | `-`        |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-switch></br-switch>` no Vue e em Stencil:

#### Vue

| Evento           | Descrição                                                            |
| ---------------- | -------------------------------------------------------------------- |
| `onChange`       | Evento emitido quando o estado do checkbox é modificado.             |
| `update:checked` | Evento emitido para fazer o two-way data binding com a prop checked. |

#### StencilJS

| Evento                      | Descrição                                                                                                     | Tipo                |
| --------------------------- | ------------------------------------------------------------------------------------------------------------- | ------------------- |
| `brConnectedCallback`       | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback).     | `CustomEvent<this>` |
| `brDidLoad`                 | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentdidload).      | `CustomEvent<this>` |
| `brDidRender`               | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback).     | `CustomEvent<this>` |
| `brDidSwitchToggle`         | Evento customizado emitido para sinalizar a mudança do estado "checked".                                      | `CustomEvent<any>`  |
| `brDidUpdate`               | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentdidupdate).    | `CustomEvent<this>` |
| `brDisconnectedCallback`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#disconnectedcallback).  | `CustomEvent<this>` |
| `brShouldUpdate`            | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentshouldupdate). | `CustomEvent<this>` |
| `brWillLoad`                | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentwillload).     | `CustomEvent<this>` |
| `brWillRender`              | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentwillrender).   | `CustomEvent<this>` |
| `brWillUpdate`              | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#componentdidupdate).    | `CustomEvent<this>` |
| `formAssociatedCallback`    | Componente foi inserido em um `<form>`                                                                        | `CustomEvent<this>` |
| `formDisassociatedCallback` | Componente foi removido de um `<form>`                                                                        | `CustomEvent<this>` |

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidSwitchToggle`, a propriedade será chamada `brDidSwitchToggle`.

```html

<template>
  <br-switch id="switch-example" brDidSwitchToggle={(event) => {
    console.log('Estado do switch alterado para:', event.detail);
  }} />
</template>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-switch></br-switch>
</template>

<script>
  const switchElement = document.querySelector('br-switch')
  switchElement.addEventListener('brDidSwitchToggle', (event) => {
    /* your listener */
  })
</script>
```

### Slots

O componente `br-switch` possui o slot `default` no StencilJs, que permite a personalização do label do switch.

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplos

Exemplos de uso do componente Switch no Stencil:

```html
<br-switch id="switch-default" label="Switch de Alternância" checked ariaLabel="Exemplo de switch de alternância">
</br-switch>
```

```html
<br-switch id="switch-with-labels" label="Notificações" labelOn="Ativado" labelOff="Desativado" checked> </br-switch>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).
