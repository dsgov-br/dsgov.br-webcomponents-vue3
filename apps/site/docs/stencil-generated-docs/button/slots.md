## Slots

| Slot        | Descrição                                                             |
| ----------- | --------------------------------------------------------------------- |
| `"default"` | Espaço de conteúdo do button. Geralmente usado para incluir o rótulo. |