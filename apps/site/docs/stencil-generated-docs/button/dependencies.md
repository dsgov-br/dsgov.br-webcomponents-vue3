## Dependências

### Usado por

 - [br-input](../input)
 - [br-message](../message)
 - [br-tag](../tag)
 - [br-upload](../upload)

### Gráfico
```mermaid
graph TD;
  br-input --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-message --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-tag --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-upload --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-button mainComponent
```