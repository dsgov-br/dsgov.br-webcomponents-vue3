# Item - Guia de Migração de Web Components de Vue para StencilJS

## Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil | Descrição                                                                | Tipo      | Padrão      |
| --------------- | ------------------- | ------------------------------------------------------------------------ | --------- | ----------- |
| ---             | `density`           | Define a densidade do item ('large', 'middle', 'small').                 | `string`  | `large`     |
| ---             | `isButton`          | Define se o item deve ser renderizado como um botão.                     | `boolean` | `false`     |
| ---             | `isInteractive`     | Indica se toda a superfície do item é interativa.                        | `boolean` | `false`     |
| ---             | `target`            | Especifica onde abrir o link (usado com href).                           | `string`  | `undefined` |
| ---             | `type`              | Tipo do botão, só utilizado se isButton for true.                        | `boolean` | `false`     |
| `active`        | `isActive`          | Coloca o item como ênfase, destacando-o dos demais (estado ativo).       | `boolean` | `false`     |
| `hover`         | ---                 | Efeito Hover, destaca o item ao passar o mouse por cima do mesmo.        | `boolean` | `false`     |
| `open`          | ---                 | Indica se o item está aberto/fechado quando o br-list pai é data-toggle. | `boolean` | `false`     |
| `selected`      | `isSelected`        | Indica se o item está ou não selecionado.                                | `boolean` | `false`     |
| `title`         | ---                 | Título visível para o item quando o br-list pai é data-toggle.           | `string`  | `undefined` |

## Como Utilizar Eventos

Entenda melhor como utilizar os eventos para o componente `<br-item></br-item>` no Vue e em Stencil:

### Vue

| Evento            | Descrição                                             |
| ----------------- | ----------------------------------------------------- |
| `toggle-open`     | Disparado quando a prop open é modificada.            |
| `toggle-selected` | Emitido quando quando a prop `selected` é modificada. |

### StencilJS

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidClick`, a propriedade será chamada `onBrDidClick`.

```html
<!-- StencilJS -->
<br-item onEventoPersonalizado="handleEvent"></br-item>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-item></br-item>
</template>

<script>
  const itemElement = document.querySelector('br-item')
  itemElement.addEventListener('brDidClick', (event) => {
    /* your listener */
  })
</script>
```

## Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

## Exemplos

Exemplos de uso do componente Item no Stencil:

```html
<br-item is-button is-selected onBrDidClick="handleClick"> Botão Item </br-item>
```

```html
<br-item href="https://example.com" target="_blank"> Link Item </br-item>
```

```html
<br-item is-interactive>
  <br-checkbox> Item Interativo </br-checkbox>
</br-item>
```

## Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).
