## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil                 | Descrição                                                                                                      | Tipo      | Padrão      |
| --------------- | ----------------------------------- | -------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `autocomplete`  | `autocomplete`                      | Define o comportamento do autocomplete no formulário.                                                          | `string`  | `undefined` |
| `autofocus`     | `autofocus`                         | Define se o campo de entrada recebe foco automaticamente ao carregar a página.                                 | `boolean` | `false`     |
| `button`        | `<br-icon slot="action"></br-icon>` | Define o botão à direita para o campo de entrada. Se `type="password"`, não há necessidade de definir o ícone. | `boolean` | `false`     |
| `danger`        | `danger`                            | Define se o campo de entrada está em estado de perigo.                                                         | `boolean` | `false`     |
| `density`       | `density`                           | Define a densidade do campo de entrada.                                                                        | `string`  | `''`        |
| `disabled`      | `disabled`                          | Define se o campo de entrada é desabilitado.                                                                   | `boolean` | `false`     |
| `highlight`     | `is-highlight`                      | Define se o campo de entrada está destacado.                                                                   | `boolean` | `false`     |
| `icon`          | `<br-icon slot="icon"></br-icon>`   | Define o ícone para o campo de entrada.                                                                        | `boolean` | `false`     |
| `info`          | `info`                              | Define se o campo de entrada está em estado de informação.                                                     | `boolean` | `false`     |
| `inline`        | `is-inline`                         | Define o layout do input e label como inline.                                                                  | `boolean` | `false`     |
| `label`         | `label`                             | Define o rótulo do campo de entrada.                                                                           | `string`  | `undefined` |
| `name`          | `name`                              | Define o nome do campo de entrada.                                                                             | `string`  | `undefined` |
| `placeholder`   | `placeholder`                       | Define o placeholder do campo de entrada.                                                                      | `string`  | `undefined` |
| `readonly`      | `readonly`                          | Define se o campo de entrada é somente leitura.                                                                | `boolean` | `false`     |
| `required`      | `required`                          | Define se o campo de entrada é obrigatório.                                                                    | `boolean` | `false`     |
| `success`       | `success`                           | Define se o campo de entrada está em estado de sucesso.                                                        | `boolean` | `false`     |
| `type`          | `type`                              | Define o tipo de campo de entrada.                                                                             | `string`  | `text`      |
| `value`         | `value`                             | Define o valor inicial do campo de entrada.                                                                    | `string`  | `undefined` |
| `warning`       | `warning`                           | Define se o campo de entrada está em estado de alerta.                                                         | `boolean` | `false`     |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-input></br-input>` no Vue e em StencilJS:

#### Vue

- Evento *`input`*: Evento emitido quando o valor do campo de entrada é modificado.

Exemplo:

```html
<br-input id="input-exemplo" label="Teste"> </br-input>
```

Adicionando o listener de evento:

```javascript
document.getElementById('input-exemplo').addEventListener('input', (e) => {
  const inputValue = e.target.value
  console.log('O valor do input é: ' + inputValue)
})
```

#### StencilJS

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `valueChange`, a propriedade será chamada `onValueChange`.

```html
<!-- StencilJS -->
<br-input onValueChange="handleEvent"></br-input>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-input></br-input>
</template>

<script>
  const inputElement = document.querySelector('br-input')
  inputElement.addEventListener('valueChange', (event) => {
    /* your listener */
  })
</script>
```

### Métodos

- `handleInput(event)`: é o método público acessível que atualiza o valor do campo de entrada e emite o evento de alteração. Veja como exemplo, o método é chamado dentro do listener de evento `onInput`:

```javascript
<script>const inputElement = document.querySelector('br-input'); inputElement.handleInput(event);</script>
```

### Slots

Foi criado o slot `icon` no StencilJS, que permite a personalização do ícone associado ao input.

```html
<!-- StencilJS -->
<br-input>
  <span slot="icon">🔍</span>
  <!-- Conteúdo do slot -->
</br-input>
```

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Input em uma aplicação Web.

```html
<template>
  <br-input id="input-01" name="input-01" label="Nome" aria-label="nome" onValueChange="handleEvent"></br-input>
</template>
<script>
  export default {
    methods: {
      handleEvent(event) {
        console.log(event.detail)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).