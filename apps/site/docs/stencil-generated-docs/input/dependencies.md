## Dependências

### Usado por

- [br-select](../select)

### Depende de

- [br-icon](../icon)
- [br-button](../button)

### Gráfico

```mermaid
graph TD;
  br-input --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-input --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-select --Depende---> br-input
  click br-input "src/components/input" "Link para a documentação do componente input"
  class br-input depComponent
  class br-input mainComponent
```
