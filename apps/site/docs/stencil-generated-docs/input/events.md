## Eventos

| Evento        | Descrição                 | Tipo                  |
| ------------- | ------------------------- | --------------------- |
| `valueChange` | Valor atualizado do input | `CustomEvent<string>` |
