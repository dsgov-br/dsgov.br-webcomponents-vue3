## Eventos

| Evento          | Descrição                                              | Tipo                   |
| --------------- | ------------------------------------------------------ | ---------------------- |
| `checkedChange` | Disparado depois que o valor do `checked` foi alterado | `CustomEvent<boolean>` |
