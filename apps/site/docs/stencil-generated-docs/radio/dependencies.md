## Dependências

### Usado por

 - [br-select](../select)

### Gráfico
```mermaid
graph TD;
  br-select --Depende---> br-radio
  click br-radio "src/components/radio" "Link para a documentação do componente radio"
  class br-radio depComponent
  class br-radio mainComponent
```