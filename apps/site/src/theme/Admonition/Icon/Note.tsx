export default function AdmonitionIconNote(): JSX.Element {
  return (
    <div className="icon">
      <i className="far fa-sticky-note" aria-hidden="true"></i>
    </div>
  )
}
