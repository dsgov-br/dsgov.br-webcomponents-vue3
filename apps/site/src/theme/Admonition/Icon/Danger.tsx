export default function AdmonitionIconDanger(): JSX.Element {
  return (
    <div className="icon">
      <i className="fas fa-times-circle fa-lg" aria-hidden="true"></i>
    </div>
  )
}
