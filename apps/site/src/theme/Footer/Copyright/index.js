export default function FooterCopyright({ copyright }) {
  return (
    <div className="info">
      <div className="text-down-01 text-medium pb-3" dangerouslySetInnerHTML={{ __html: copyright }}></div>
    </div>
  )
}
