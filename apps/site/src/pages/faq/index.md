---
description: FAQ para auxiliar os usuários
slug: 'faq'
title: 'Perguntas Frequentes'
hide_table_of_contents: true
---

import Stencil from './o-que-e-o-stencil.md';
import WC from './o-que-sao-web-components.md';
import Lib from './por-que-criar-uma-biblioteca-de-componentes.md';

<details>
 <summary>O que são Web Components?</summary>
 <div><WC/></div>
</details>

<details>
  <summary>O que é o Stencil?</summary>
   <div><Stencil/></div>
</details>

<details>
  <summary>Por que criar uma biblioteca de Componentes?</summary>
  <div><Lib/></div>
</details>
