import Link from '@docusaurus/Link'
import { useLatestVersion, useVersions } from '@docusaurus/plugin-content-docs/client'
import useDocusaurusContext from '@docusaurus/useDocusaurusContext'
import VersionsArchived from '@site/versionsArchived.json'
import Heading from '@theme/Heading'
import Layout from '@theme/Layout'

const docsPluginId = undefined
const VersionsArchivedList = Object.entries(VersionsArchived)

function DocumentationLabel() {
  return 'Documentação'
}

function ReleaseNotesLabel() {
  return 'Release Notes'
}

export default function Version(): JSX.Element {
  const {
    siteConfig: { customFields },
  } = useDocusaurusContext()
  const versions = useVersions(docsPluginId)
  const latestVersion = useLatestVersion(docsPluginId)
  const currentVersion = versions.find((version) => version.name === 'current')!
  const pastVersions = versions.filter((version) => version !== latestVersion && version.name !== 'current')
  const repoUrl = `${customFields.repo!}/-/releases`

  return (
    <Layout title="Versions" description="Docusaurus 2 Versions page listing all documented site versions">
      <main className="container margin-vert--lg">
        <Heading as="h1">Versões da documentação de Web Components</Heading>

        <div className="margin-bottom--lg">
          <Heading as="h3" id="next">
            {currentVersion !== latestVersion && 'Versão mais nova (estável)'}
            {/* Como inicialmente estamos disponibilizando inicialmente apenas a next, precisamos desse 'if' inicial */}
            {currentVersion === latestVersion && 'Próxima versão (ainda em desenvolvimento)'}
          </Heading>
          <table>
            <tbody>
              <tr>
                <th>{latestVersion.label}</th>
                <td>
                  <Link to={`${latestVersion.path}`}>
                    <DocumentationLabel />
                  </Link>
                </td>
                <td>
                  <Link to={`${repoUrl}/v${latestVersion.name}`}>
                    <ReleaseNotesLabel />
                  </Link>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        {currentVersion !== latestVersion && (
          <div className="margin-bottom--lg">
            <Heading as="h3" id="latest">
              Próxima versão (ainda em desenvolvimento)
            </Heading>
            <table>
              <tbody>
                <tr>
                  <th>{currentVersion.label}</th>
                  <td>
                    <Link to={`${currentVersion.path}`}>
                      <DocumentationLabel />
                    </Link>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        )}

        {pastVersions.length > 0 && (
          <div className="margin-bottom--lg">
            <Heading as="h3" id="archive">
              Versões anteriores
            </Heading>
            <table>
              <tbody>
                {pastVersions.map((version) => (
                  <tr key={version.name}>
                    <th>{version.label}</th>
                    <td>
                      <Link to={`${version.path}`}>
                        <DocumentationLabel />
                      </Link>
                    </td>
                    <td>
                      <Link href={`${repoUrl}/v${version.name}`}>
                        <ReleaseNotesLabel />
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}

        {VersionsArchivedList.length > 0 && (
          <div className="margin-bottom--lg">
            <Heading as="h3" id="archive">
              Versões arquivadas (sem manutenção)
            </Heading>
            <table>
              <tbody>
                {VersionsArchivedList.map(([versionName, versionUrl]) => (
                  <tr key={versionName}>
                    <th>{versionName}</th>
                    <td>
                      <Link to={versionUrl}>
                        <DocumentationLabel />
                      </Link>
                    </td>
                    <td>
                      <Link href={`${repoUrl}/v${versionName}`}>
                        <ReleaseNotesLabel />
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </main>
    </Layout>
  )
}
