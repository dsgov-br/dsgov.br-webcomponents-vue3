import { useActiveDocContext } from '@docusaurus/plugin-content-docs/client'
import useDocusaurusContext from '@docusaurus/useDocusaurusContext'
import Heading from '@theme/Heading'
import clsx from 'clsx'
import styles from './styles.module.css'

type IntegrationItem = {
  title: string
  Svg: React.ComponentType<React.ComponentProps<'svg'>>
  text?: JSX.Element
  href?: any
}

function Feature({ title, Svg, text, href }: Readonly<IntegrationItem>) {
  return (
    <div className={clsx(`col br-card ${href ? 'hover' : ''}`, styles.featureBlock)}>
      {href ? (
        <a href={href}>
          <div className="card-content text--center">
            <Heading as="h3">{title}</Heading>
            <p>{text}</p>
          </div>
        </a>
      ) : (
        <div>
          <div className="text--center">
            <Svg className={styles.featureSvg} role="img" />
          </div>
          <div className="card-content text--center">
            <Heading as="h3">{title}</Heading>
            <p>{text}</p>
          </div>
        </div>
      )}
    </div>
  )
}

export default function HomepageFeatures(): JSX.Element {
  const {
    siteConfig: { customFields },
  } = useDocusaurusContext()
  const docsPluginId = undefined
  const activeVersionObj = useActiveDocContext(docsPluginId).activeVersion!
  console.log(activeVersionObj)
  const Integration: IntegrationItem[] = [
    {
      title: 'Angular',
      Svg: require('@site/static/img/frameworks/angular.svg').default,
    },
    {
      title: 'React',
      Svg: require('@site/static/img/frameworks/react.svg').default,
    },
    {
      title: 'Vue',
      Svg: require('@site/static/img/frameworks/vue.svg').default,
    },
  ]

  const Community: IntegrationItem[] = [
    {
      title: 'Discord',
      Svg: require('@site/static/img/community/discord.svg').default,
      text: <>Para compartilhar suas experiências, tirar suas dúvidas e ajudar outros membros da comunidade.</>,
      href: customFields.discordInvite,
    },
    {
      title: 'Gitlab',
      Svg: require('@site/static/img/community/gitlab.svg').default,
      text: <>Pedir novas features, abrir issues, contribuir com o projeto.</>,
      href: customFields.repo,
    },
  ]

  return (
    <>
      <section className={styles.features}>
        <div className="container">
          <Heading as="h2">Integração com frameworks</Heading>
          <div className="row">
            {Integration.map((props, idx) => (
              <Feature key={idx} {...props} />
            ))}
          </div>
        </div>
      </section>
      <section className={styles.features}>
        <div className="container">
          <Heading as="h2">Comunidade</Heading>
          <p>
            Nosso projeto é open source. Participe da nossa comunidade e contribua com o projeto! Todos são bem-vindos.
          </p>
          <div className="row">
            {Community.map((props, idx) => (
              <Feature key={idx} {...props} />
            ))}
          </div>
        </div>
      </section>
    </>
  )
}
